package amoy

import (
	"fmt"
	"os"
)

// Eprintf likes fmt.Printf but use stderr as the output.
func Eprintf(format string, a ...interface{}) (n int, err error) {
	return fmt.Fprintf(os.Stderr, format, a...)
}

// Eprint likes fmt.Print but use stderr as the output.
func Eprint(a ...interface{}) (n int, err error) {
	return fmt.Fprint(os.Stderr, a...)
}

// Eprintln likes fmt.Println but use stderr as the output.
func Eprintln(a ...interface{}) (n int, err error) {
	return fmt.Fprintln(os.Stderr, a...)
}

package amoy

import (
	"hash/fnv"
)

// FNV32a returns the 32-bit FNV-1a hash of the given string.
func FNV32a(text string) uint32 {
	algorithm := fnv.New32a()
	_, _ = algorithm.Write([]byte(text))
	return algorithm.Sum32()
}

// FNV64a returns the 64-bit FNV-1a hash of the given string.
func FNV64a(text string) uint64 {
	algorithm := fnv.New64a()
	_, _ = algorithm.Write([]byte(text))
	return algorithm.Sum64()
}

// FNV128a returns the 128-bit FNV-1a hash of the given string.
func FNV128a(text string) []byte {
	algorithm := fnv.New128a()
	_, _ = algorithm.Write([]byte(text))
	return algorithm.Sum(nil)
}

package amoy

import (
	"os"
	"testing"

	"github.com/1set/gut/yos"
)

func TestGetHomeDir(t *testing.T) {
	osHome := os.Getenv("HOME")
	tests := []struct {
		name    string
		dirs    []string
		want    string
		wantErr bool
	}{
		{"nil args", nil, osHome, false},
		{"no args", []string{}, osHome, false},
		{"empty arg", []string{""}, osHome, false},
		{"one arg", []string{"foo"}, yos.JoinPath(osHome, "foo"), false},
		{"two args", []string{"foo", "bar"}, yos.JoinPath(osHome, "foo", "bar"), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetHomeDir(tt.dirs...)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetHomeDir(%v) error = %v, wantErr %v", tt.dirs, err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetHomeDir(%v) got = %v, want %v", tt.dirs, got, tt.want)
			}
		})
	}
}

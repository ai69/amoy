package amoy

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func TestParallelTaskRun(t *testing.T) {
	tests := []struct {
		name    string
		timeOut time.Duration
		num     int
		tasks   []string
		eta     time.Duration
		wantErr bool
	}{
		{"1 Worker for 1 Task (Min)", ZeroDuration, 0, []string{"a"}, ZeroDuration, true},
		{"1 Worker for 1 Task", ZeroDuration, 1, []string{"a"}, Seconds(1), false},
		{"1 Worker for 2 Task", ZeroDuration, 1, []string{"a", "b"}, Seconds(2), false},
		{"2 Workers for 2 Tasks", ZeroDuration, 2, []string{"a", "b"}, Seconds(1), false},
		{"2 Workers for 3 Tasks", ZeroDuration, 2, []string{"a", "b", "c"}, Seconds(2), false},
		{"2 Workers for 7 Tasks", ZeroDuration, 2, []string{"a", "b", "c", "d", "e", "f", "g"}, Seconds(4), false},
		{"2 Workers for 7 Tasks With 0 Sec Timeout", Milliseconds(1), 2, []string{"a", "b", "c", "d", "e", "f", "g"}, ZeroDuration, true},
		{"2 Workers for 7 Tasks With 0.5 Sec Timeout", Seconds(0.5), 2, []string{"a", "b", "c", "d", "e", "f", "g"}, Seconds(0.5), true},
		{"2 Workers for 7 Tasks With 3.5 Sec Timeout", Seconds(3.5), 2, []string{"a", "b", "c", "d", "e", "f", "g"}, Seconds(3.5), true},
		{"2 Workers for 7 Tasks With 5 Sec Timeout", Seconds(5), 2, []string{"a", "b", "c", "d", "e", "f", "g"}, Seconds(4), false},
		{"7 Workers for 7 Tasks", ZeroDuration, 7, []string{"a", "b", "c", "d", "e", "f", "g"}, Seconds(1), false},
		{"7 Workers for 7 Tasks (Max)", ZeroDuration, 20, []string{"a", "b", "c", "d", "e", "f", "g"}, Seconds(1), false},
	}
	tolerance := Milliseconds(100)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// create context
			ctx := context.Background()
			if tt.timeOut > 0 {
				t.Logf("set context timeout: %v", tt.timeOut)
				ctx, _ = context.WithTimeout(ctx, tt.timeOut)
			}
			// run
			start := time.Now()
			callTestParallelTaskRunTest(ctx, t, tt.name, tt.num, tt.tasks, tt.wantErr)
			elapsed := time.Since(start)
			// check elapsed time
			if elapsed > tt.eta+tolerance {
				t.Errorf("ParallelTaskRun() took too long (time cost: %v, expected: %v)", elapsed, tt.eta)
			} else if elapsed < tt.eta-tolerance {
				t.Errorf("ParallelTaskRun() took too short (time cost: %v, expected: %v)", elapsed, tt.eta)
			}
		})
	}
}

func callTestParallelTaskRunTest(ctx context.Context, t *testing.T, name string, num int, tasks []string, wantErr bool) {
	timeStart := time.Now()
	timeLapsedStr := func() string {
		return fmt.Sprintf("%02.3f", float64(time.Since(timeStart).Microseconds())/1e6)
	}
	err := ParallelTaskRun(ctx, num, len(tasks), func(ctx context.Context, id int) (interface{}, error) {
		task := tasks[id]
		t.Logf("[%s][%s] got task%d to run: %s", name, timeLapsedStr(), id, task)
		if err := SleepWithContext(ctx, Seconds(1)); err != nil {
			return nil, err
		}
		if FeelLucky(0.3) {
			return nil, fmt.Errorf("feel unlucky")
		}
		return "[" + task + "]", nil
	}, func(ctx context.Context, id int, result interface{}, err error) {
		task := tasks[id]
		t.Logf("[%s][%s] got task%d %q result: %v, error: %v", name, timeLapsedStr(), id, task, result, err)
	})
	if wantErr && err == nil {
		t.Errorf("[%s] should return error (time cost: %v)", name, time.Since(timeStart))
	} else if !wantErr && err != nil {
		t.Errorf("[%s] failed (time cost: %v), error: %v", name, time.Since(timeStart), err)
	} else {
		t.Logf("[%s] all tasks are done (time_cost: %v), error: %v", name, time.Since(timeStart), err)
	}
}

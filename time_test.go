package amoy

import (
	"reflect"
	"testing"
	"time"
)

func TestAfterNow(t *testing.T) {
	tests := []struct {
		name string
		t    time.Time
		want bool
	}{
		{"Before Now", time.Date(2021, time.April, 15, 23, 59, 59, 0, time.UTC), false},
		{"After Now", time.Date(2999, time.April, 15, 23, 59, 59, 0, time.UTC), true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AfterNow(tt.t); got != tt.want {
				t.Errorf("AfterNow() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBeforeNow(t *testing.T) {
	tests := []struct {
		name string
		t    time.Time
		want bool
	}{
		{"Before Now", time.Date(2021, time.April, 15, 23, 59, 59, 0, time.UTC), true},
		{"After Now", time.Date(2999, time.April, 15, 23, 59, 59, 0, time.UTC), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := BeforeNow(tt.t); got != tt.want {
				t.Errorf("BeforeNow() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetUnixTime(t *testing.T) {
	tests := []struct {
		name string
		want int64
	}{
		{"one test", 1618722646},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetUnixTime(); got < tt.want {
				t.Errorf("GetUnixTime() = %v, want > %v", got, tt.want)
			}
		})
	}
}

func TestParseUnixTime(t *testing.T) {
	tests := []struct {
		name  string
		stamp int64
		want  time.Time
	}{
		{"zero test", 0, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC).Local()},
		{"one test", 1627528253, time.Date(2021, 7, 29, 3, 10, 53, 0, time.UTC).Local()},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseUnixTime(tt.stamp); got != tt.want {
				t.Errorf("ParseUnixTime(%d) = %v, want = %v", tt.stamp, got, tt.want)
			}
		})
	}

	now := time.Now()
	gotS := GetUnixTime()
	gotT := ParseUnixTime(gotS)
	if now.Sub(gotT) > 1*Second {
		t.Errorf("ParseUnixTime(%d) = %v, want = %v", gotS, gotT, now)
	}
}

func TestNowStr(t *testing.T) {
	t.Logf("NowStr -> %v", NowStr())
	t.Logf("ShortNowStr -> %v", ShortNowStr())
	t.Logf("DateNowStr -> %v", DateNowStr())
	t.Logf("UTCNow -> %v", UTCNow())
	t.Logf("UTCNowStr -> %v", UTCNowStr())
	t.Logf("ShortUTCNowStr -> %v", ShortUTCNowStr())
	t.Logf("DateUTCNowStr -> %v", DateUTCNowStr())
}

func TestSleepForSeconds(_ *testing.T) {
	SleepForSeconds(1)
}

func TestMilliseconds(t *testing.T) {
	tests := []struct {
		name string
		n    float64
		want time.Duration
	}{
		{"zero", 0, 0},
		{"one", 1, 1 * time.Millisecond},
		{"one and half", 1.5, 1*time.Millisecond + 500*time.Microsecond},
		{"one msec", 0.001, time.Microsecond},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Milliseconds(tt.n); got != tt.want {
				t.Errorf("Milliseconds(%f) = %v, want %v", tt.n, got, tt.want)
			}
		})
	}
}

func TestSeconds(t *testing.T) {
	tests := []struct {
		name string
		n    float64
		want time.Duration
	}{
		{"zero", 0, 0},
		{"one", 1, 1 * time.Second},
		{"one and half", 1.5, 1*time.Second + 500*time.Millisecond},
		{"one msec", 0.001, time.Millisecond},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Seconds(tt.n); got != tt.want {
				t.Errorf("Seconds(%f) = %v, want %v", tt.n, got, tt.want)
			}
		})
	}
}

func TestMinutes(t *testing.T) {
	tests := []struct {
		name string
		n    float64
		want time.Duration
	}{
		{"zero", 0, 0},
		{"one", 1, 1 * time.Minute},
		{"one and half", 1.5, 1*time.Minute + 30*time.Second},
		{"six sec", 0.1, 6 * time.Second},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Minutes(tt.n); got != tt.want {
				t.Errorf("Minutes(%f) = %v, want %v", tt.n, got, tt.want)
			}
		})
	}
}

func TestHours(t *testing.T) {
	tests := []struct {
		name string
		n    float64
		want time.Duration
	}{
		{"zero", 0, 0},
		{"one", 1, 1 * time.Hour},
		{"one and half", 1.5, 1*time.Hour + 30*time.Minute},
		{"twelve min", 0.2, 12 * time.Minute},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Hours(tt.n); got != tt.want {
				t.Errorf("Hours(%f) = %v, want %v", tt.n, got, tt.want)
			}
		})
	}
}

func TestDays(t *testing.T) {
	tests := []struct {
		name string
		n    float64
		want time.Duration
	}{
		{"zero", 0, 0},
		{"one", 1, 24 * time.Hour},
		{"one and half", 1.5, 36 * time.Hour},
		{"ten", 10.0, 240 * time.Hour},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Days(tt.n); got != tt.want {
				t.Errorf("Days(%f) = %v, want %v", tt.n, got, tt.want)
			}
		})
	}
}

func TestWeeks(t *testing.T) {
	tests := []struct {
		name string
		n    float64
		want time.Duration
	}{
		{"zero", 0, 0},
		{"one", 1, 7 * 24 * time.Hour},
		{"one and half", 1.5, 10.5 * 24 * time.Hour},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Weeks(tt.n); got != tt.want {
				t.Errorf("Weeks(%f) = %v, want %v", tt.n, got, tt.want)
			}
		})
	}
}

func TestYears(t *testing.T) {
	tests := []struct {
		name string
		n    float64
		want time.Duration
	}{
		{"zero", 0, 0},
		{"one", 1, 365 * 24 * time.Hour},
		{"one and one fifth", 1.2, 438 * 24 * time.Hour},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Years(tt.n); got != tt.want {
				t.Errorf("Years(%f) = %v, want %v", tt.n, got, tt.want)
			}
		})
	}
}

func TestDayBeginEnd(t *testing.T) {
	local := time.FixedZone("Shanghai", 8*60*60)
	tests := []struct {
		name      string
		now       time.Time
		zone      *time.Location
		wantBegin time.Time
		wantEnd   time.Time
	}{
		{
			"Same UTC Day (Local->Local)",
			time.Date(2021, 9, 1, 12, 0, 0, 0, local),
			local,
			time.Date(2021, 9, 1, 0, 0, 0, 0, local),
			time.Date(2021, 9, 1, 23, 59, 59, 999999999, local),
		},
		{
			"Diff UTC Day (Local->Local)",
			time.Date(2021, 9, 1, 6, 0, 0, 0, local),
			local,
			time.Date(2021, 9, 1, 0, 0, 0, 0, local),
			time.Date(2021, 9, 1, 23, 59, 59, 999999999, local),
		},
		{
			"Same UTC Day (Local->UTC)",
			time.Date(2021, 9, 1, 12, 0, 0, 0, local),
			time.UTC,
			time.Date(2021, 9, 1, 0, 0, 0, 0, time.UTC),
			time.Date(2021, 9, 1, 23, 59, 59, 999999999, time.UTC),
		},
		{
			"Diff UTC Day (Local->UTC)",
			time.Date(2021, 9, 1, 6, 0, 0, 0, local),
			time.UTC,
			time.Date(2021, 9, 1, 0, 0, 0, 0, time.UTC),
			time.Date(2021, 9, 1, 23, 59, 59, 999999999, time.UTC),
		},
		{
			"Same UTC Day (UTC->Local)",
			time.Date(2021, 9, 1, 12, 0, 0, 0, local).UTC(),
			local,
			time.Date(2021, 9, 1, 0, 0, 0, 0, local),
			time.Date(2021, 9, 1, 23, 59, 59, 999999999, local),
		},
		{
			"Diff UTC Day (UTC->Local)",
			time.Date(2021, 9, 1, 6, 0, 0, 0, local).UTC(),
			local,
			time.Date(2021, 8, 31, 0, 0, 0, 0, local),
			time.Date(2021, 8, 31, 23, 59, 59, 999999999, local),
		},
		{
			"Same UTC Day (UTC->UTC)",
			time.Date(2021, 9, 1, 12, 0, 0, 0, local).UTC(),
			time.UTC,
			time.Date(2021, 9, 1, 0, 0, 0, 0, time.UTC),
			time.Date(2021, 9, 1, 23, 59, 59, 999999999, time.UTC),
		},
		{
			"Diff UTC Day (UTC->UTC)",
			time.Date(2021, 9, 1, 6, 0, 0, 0, local).UTC(),
			time.UTC,
			time.Date(2021, 8, 31, 0, 0, 0, 0, time.UTC),
			time.Date(2021, 8, 31, 23, 59, 59, 999999999, time.UTC),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DayBegin(tt.now, tt.zone); got != tt.wantBegin {
				t.Errorf("DayBegin(%v) = %v, want %v", tt.now, got, tt.wantBegin)
			}
			if got := DayEnd(tt.now, tt.zone); got != tt.wantEnd {
				t.Errorf("DayEnd(%v) = %v, want %v", tt.now, got, tt.wantEnd)
			}
		})
	}
}

func BenchmarkDurationToString(b *testing.B) {
	d := Days(365) + Hours(23) + Minutes(34) + Seconds(45.678932)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = DurationToString(d)
	}
}

func TestDurationToString(t *testing.T) {
	tests := []struct {
		name string
		d    time.Duration
		want string
	}{
		{"Zero", 0, "00:00:00.000"},
		{"~1 Sec", Seconds(0.9999), "00:00:00.999"},
		{"1 Sec", Seconds(1), "00:00:01.000"},
		{"12.3456 Sec", Seconds(12.3456), "00:00:12.345"},
		{"1D 2H 3M 4.5S", Days(1) + Hours(2) + Minutes(3) + Seconds(4.5), "01:02:03:04.500"},
		{"12D 23H 34M 45.678S", Days(12) + Hours(23) + Minutes(34) + Seconds(45.678), "12:23:34:45.678"},
		{"365D 23H 34M 45.678S", Days(365) + Hours(23) + Minutes(34) + Seconds(45.678), "365:23:34:45.678"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DurationToString(tt.d); got != tt.want {
				t.Errorf("DurationToString(%v) = %v, want %v", tt.d, got, tt.want)
			}
		})
	}
}

func TestMustParseDateString(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want time.Time
	}{
		{"First Day of 2022", "2022-01-01", NewUTCDay(2022, 01, 01)},
		{"Second Day of 2022", "2022-01-02", NewUTCDay(2022, 01, 02)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MustParseDateString(tt.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MustParseDateString(%s) = %v, want %v", tt.s, got, tt.want)
			}
		})
	}
}

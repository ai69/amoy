package amoy

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
)

// LineFunc stands for a handler for each line string.
type LineFunc func(line string) (err error)

//revive:disable:error-naming It's not a real error
var (
	// QuitRead indicates the arbitrary error means to quit from reading.
	QuitRead = errors.New("amoy: quit read by line")
)

// ReadFileByLine iterates the given file by lines (the line ending chars are not included).
func ReadFileByLine(path string, callback LineFunc) (err error) {
	var file *os.File
	if file, err = os.Open(path); err != nil {
		return
	}
	defer file.Close()
	return ReadByLine(file, callback)
}

// ReadByLine iterates the given Reader by lines (the line ending chars are not included).
func ReadByLine(rd io.Reader, callback LineFunc) (err error) {
	readLine := func(r *bufio.Reader) (string, error) {
		var (
			err      error
			line, ln []byte
			isPrefix = true
		)
		for isPrefix && err == nil {
			line, isPrefix, err = r.ReadLine()
			ln = append(ln, line...)
		}
		return string(ln), err
	}
	r := bufio.NewReader(rd)
	s, e := readLine(r)
	for e == nil {
		if err = callback(s); err != nil {
			break
		}
		s, e = readLine(r)
	}

	if err == QuitRead {
		err = nil
	}
	return
}

// ReadFileLines reads all lines from the given file (the line ending chars are not included).
func ReadFileLines(path string) (lines []string, err error) {
	err = ReadFileByLine(path, func(l string) error {
		lines = append(lines, l)
		return nil
	})
	return
}

// ReadLines reads all lines from the given reader (the line ending chars are not included).
func ReadLines(rd io.Reader) (lines []string, err error) {
	err = ReadByLine(rd, func(l string) error {
		lines = append(lines, l)
		return nil
	})
	return
}

// CountFileLines counts all lines from the given file (the line ending chars are not included).
func CountFileLines(path string) (count int, err error) {
	err = ReadFileByLine(path, func(l string) error {
		count++
		return nil
	})
	return
}

// CountLines counts all lines from the given reader (the line ending chars are not included).
func CountLines(rd io.Reader) (count int, err error) {
	err = ReadByLine(rd, func(l string) error {
		count++
		return nil
	})
	return
}

// WriteLines writes the given lines to a Writer.
func WriteLines(wr io.Writer, lines []string) error {
	w := bufio.NewWriter(wr)
	defer w.Flush()
	for _, line := range lines {
		if _, err := fmt.Fprintln(w, line); err != nil {
			return err
		}
	}
	return nil
}

// WriteFileLines writes the given lines as a text file.
func WriteFileLines(path string, lines []string) error {
	return openFileWriteLines(path, createFileFlag, lines)
}

// AppendFileLines appends the given lines to the end of a text file.
func AppendFileLines(path string, lines []string) error {
	return openFileWriteLines(path, appendFileFlag, lines)
}

func openFileWriteLines(path string, flag int, lines []string) error {
	file, err := os.OpenFile(path, flag, filePerm)
	if err != nil {
		return err
	}
	defer file.Close()
	return WriteLines(file, lines)
}

// ExtractTopLines extracts the top n lines from the given stream (the line ending chars are not included), or lesser lines if the given stream doesn't contain enough line ending chars.
func ExtractTopLines(rd io.Reader, n int) ([]string, error) {
	if n <= 0 {
		return nil, errors.New("amoy: n should be greater than 0")
	}
	result := make([]string, 0)
	if err := ReadByLine(rd, func(line string) error {
		result = append(result, line)
		n--
		if n <= 0 {
			return QuitRead
		}
		return nil
	}); err != nil {
		return nil, err
	}
	return result, nil
}

// ExtractFirstLine extracts the first line from the given stream (the line ending chars are not included).
func ExtractFirstLine(rd io.Reader) (string, error) {
	lines, err := ExtractTopLines(rd, 1)
	if err != nil {
		return EmptyStr, err
	}
	if len(lines) < 1 {
		return EmptyStr, nil
	}
	return lines[0], nil
}

// ExtractBottomLines extracts the bottom n lines from the given stream (the line ending chars are not included), or lesser lines if the given stream doesn't contain enough line ending chars.
func ExtractBottomLines(rd io.Reader, n int) ([]string, error) {
	if n <= 0 {
		return nil, errors.New("amoy: n should be greater than 0")
	}
	var (
		result = make([]string, n, n)
		cnt    int
	)
	if err := ReadByLine(rd, func(line string) error {
		result[cnt%n] = line
		cnt++
		return nil
	}); err != nil {
		return nil, err
	}
	if cnt <= n {
		return result[0:cnt], nil
	}
	pos := cnt % n
	return append(result[pos:], result[0:pos]...), nil
}

// ExtractLastLine extracts the last line from the given stream (the line ending chars are not included).
func ExtractLastLine(rd io.Reader) (string, error) {
	lines, err := ExtractBottomLines(rd, 1)
	if err != nil {
		return EmptyStr, err
	}
	if len(lines) < 1 {
		return EmptyStr, nil
	}
	return lines[len(lines)-1], nil
}

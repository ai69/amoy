package amoy

import (
	"crypto/sha1"
	"encoding/base32"
	"encoding/hex"
	"strings"

	"github.com/1set/gut/yrand"
	guuid "github.com/google/uuid"
)

// GetUUID returns a new random UUID in hex string.
// e.g. 7897af8b-9188-4fc5-9e0b-8bf63a2d7bab
func GetUUID() string {
	return guuid.New().String()
}

var b32Enc = base32.StdEncoding.WithPadding(base32.NoPadding)

// GetCDKey returns a new random CD key with 25 characters + 4 dashes in custom Base32 encoding (125 random bits in total), which contains no I, O, 0, 1.
func GetCDKey() string {
	const keys = `ABCDEFGHJKLMNPQRSTUVWXYZ23456789`
	raw, _ := yrand.String(keys, 25)
	var p []string
	for i := 0; i < len(raw); i += 5 {
		p = append(p, raw[i:i+5])
	}
	return strings.Join(p, "-")
}

// GetUB32 returns a new random UUID in standard base32 string without padding, i.e. 26-byte long.
// e.g. AMZIPGLFRVEQLKNI477QBST5V4
func GetUB32() string {
	id := guuid.New()
	if b, err := id.MarshalBinary(); err == nil {
		return b32Enc.EncodeToString(b)
	}
	return EmptyStr
}

// GetSID returns a new random UUID in base36 string.
// e.g. 3wqn6cl0uvfsbchw87izr8scm
func GetSID() string {
	id := guuid.New()
	if b, err := id.MarshalBinary(); err == nil {
		return EncodeBytesAsBase36(b)
	}
	return EmptyStr
}

// GetFingerprint returns SHA1 hash of a string in fingerprint format.
// e.g. 0a:4d:55:a8:d7:78:e5:02:2f:ab:70:19:77:c5:d8:40:bb:c4:86:d0
func GetFingerprint(s string) string {
	sum := sha1.Sum([]byte(s)) // nolint: gosec
	hexs := make([]string, len(sum))
	for i, c := range sum {
		hexs[i] = hex.EncodeToString([]byte{c})
	}
	return strings.Join(hexs, ":")
}

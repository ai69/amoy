package amoy

import (
	"reflect"
	"strings"
	"testing"
)

func TestExtractTopLines(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		n       int
		want    []string
		wantErr bool
	}{
		{"Non-positive N", "Hello", 0, nil, true},
		{"Empty string", "", 1, []string{}, false},
		{"One line", "Hello", 1, []string{"Hello"}, false},
		{"One line and newline", "Hello\n", 2, []string{"Hello"}, false},
		{"Two lines", "Hello\nWorld", 2, []string{"Hello", "World"}, false},
		{"Two lines and newline", "Hello\nWorld\n", 2, []string{"Hello", "World"}, false},
		{"Three lines with newline", "Hello\n\nWorld\n", 3, []string{"Hello", "", "World"}, false},
		{"All newlines", "\n\n\n", 1, []string{""}, false},
		{"21 lines 1", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 1, []string{"a"}, false},
		{"21 lines 2", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 2, []string{"a", "b"}, false},
		{"21 lines 3", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 3, []string{"a", "b", "c"}, false},
		{"21 lines 4", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 4, []string{"a", "b", "c", "d"}, false},
		{"21 lines 10", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 10, []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"}, false},
		{"21 lines 20", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 20, []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t"}, false},
		{"21 lines 21", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 21, []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u"}, false},
		{"21 lines 22", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 22, []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ExtractTopLines(strings.NewReader(tt.s), tt.n)
			if (err != nil) != tt.wantErr {
				t.Errorf("ExtractTopLines() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExtractTopLines() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExtractFirstLine(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    string
		wantErr bool
	}{
		{"Empty string", "", "", false},
		{"One line", "Hello", "Hello", false},
		{"One line and newline", "Hello\n", "Hello", false},
		{"Two lines", "Hello\nWorld", "Hello", false},
		{"Two lines and newline", "Hello\nWorld\n", "Hello", false},
		{"Three lines with newline", "Hello\n\nWorld\n", "Hello", false},
		{"All newlines", "\n\n\n", "", false},
		{"21 lines", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", "a", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ExtractFirstLine(strings.NewReader(tt.s))
			if (err != nil) != tt.wantErr {
				t.Errorf("ExtractFirstLine() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExtractFirstLine() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExtractBottomLines(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		n       int
		want    []string
		wantErr bool
	}{
		{"Non-positive N", "Hello", 0, nil, true},
		{"Empty string", "", 1, []string{}, false},
		{"One line", "Hello", 1, []string{"Hello"}, false},
		{"One line and newline", "Hello\n", 2, []string{"Hello"}, false},
		{"Two lines 2", "Hello\nWorld", 2, []string{"Hello", "World"}, false},
		{"Two lines 1", "Hello\nWorld", 1, []string{"World"}, false},
		{"Two lines and newline", "Hello\nWorld\n", 2, []string{"Hello", "World"}, false},
		{"Three lines with newline", "Hello\n\nWorld\n", 2, []string{"", "World"}, false},
		{"All newlines", "\n\n\n", 1, []string{""}, false},
		{"21 lines 1", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 1, []string{"u"}, false},
		{"21 lines 2", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 2, []string{"t", "u"}, false},
		{"21 lines 3", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 3, []string{"s", "t", "u"}, false},
		{"21 lines 4", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 4, []string{"r", "s", "t", "u"}, false},
		{"21 lines 10", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 10, []string{"l", "m", "n", "o", "p", "q", "r", "s", "t", "u"}, false},
		{"21 lines 20", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 20, []string{"b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u"}, false},
		{"21 lines 21", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 21, []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u"}, false},
		{"21 lines 22", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", 22, []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ExtractBottomLines(strings.NewReader(tt.s), tt.n)
			if (err != nil) != tt.wantErr {
				t.Errorf("ExtractBottomLines() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExtractBottomLines() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExtractLastLine(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    string
		wantErr bool
	}{
		{"Empty string", "", "", false},
		{"One line", "Hello", "Hello", false},
		{"One line and newline", "Hello\n", "Hello", false},
		{"Two lines", "Hello\nWorld", "World", false},
		{"Two lines and newline", "Hello\nWorld\n", "World", false},
		{"Three lines with newline", "Hello\n\nWorld\n", "World", false},
		{"All newlines", "\n\n\n", "", false},
		{"21 lines", "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\n", "u", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ExtractLastLine(strings.NewReader(tt.s))
			if (err != nil) != tt.wantErr {
				t.Errorf("ExtractLastLine() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExtractLastLine() got = %v, want %v", got, tt.want)
			}
		})
	}
}

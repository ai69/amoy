package amoy

import (
	"strings"
	"unicode"

	"github.com/1set/gut/ystring"
)

// EmptyStr is the missing empty string for Go :)
var EmptyStr string

// TruncateStr renders a truncated string of the given length, or original one if it's shorter.
func TruncateStr(s string, limit int) string {
	runes := []rune(s)
	if l := len(runes); l > limit {
		if limit <= 0 {
			return EmptyStr
		}
		return string(runes[0:limit])
	}
	return s
}

// ReverseStr returns a reversed string of the given string.
func ReverseStr(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

// SubstrAfterFirst returns a substring which starts after the first occurrence of target and continues to the end, or empty string if the target is not found or empty.
func SubstrAfterFirst(s, sub string) string {
	if s == EmptyStr || sub == EmptyStr {
		return EmptyStr
	}
	idx := strings.Index(s, sub)
	if idx < 0 {
		return EmptyStr
	}
	return s[idx+len(sub):]
}

// SubstrAfterLast returns a substring which starts after the last occurrence of target and continues to the end, or empty string if the target is not found or empty.
func SubstrAfterLast(s, sub string) string {
	if s == EmptyStr || sub == EmptyStr {
		return EmptyStr
	}
	idx := strings.LastIndex(s, sub)
	if idx < 0 {
		return EmptyStr
	}
	return s[idx+len(sub):]
}

// SubstrBeforeFirst returns a substring which starts starts at the begin of a string and continues to the first occurrence of target, or empty string if the target is not found or empty.
func SubstrBeforeFirst(s, sub string) string {
	if s == EmptyStr || sub == EmptyStr {
		return EmptyStr
	}
	idx := strings.Index(s, sub)
	if idx < 0 {
		return EmptyStr
	}
	return s[0:idx]
}

// SubstrBeforeLast returns a substring which starts starts at the begin of a string and continues to the last occurrence of target, or empty string if the target is not found or empty.
func SubstrBeforeLast(s, sub string) string {
	if s == EmptyStr || sub == EmptyStr {
		return EmptyStr
	}
	idx := strings.LastIndex(s, sub)
	if idx < 0 {
		return EmptyStr
	}
	return s[0:idx]
}

// SubstrOrOrigin returns the original string if the target is missed in substr function.
func SubstrOrOrigin(subFunc func(string, string) string, s, sub string) string {
	if result := subFunc(s, sub); ystring.IsNotEmpty(result) {
		return result
	}
	return s
}

// ContainsChinese returns true if the string contains Chinese characters (Hanzhi).
func ContainsChinese(s string) bool {
	for _, r := range s {
		if unicode.Is(unicode.Han, r) {
			return true
		}
	}
	return false
}

// ContainsKorean returns true if the string contains Korean characters (Hangul).
func ContainsKorean(s string) bool {
	for _, r := range s {
		if unicode.Is(unicode.Hangul, r) {
			return true
		}
	}
	return false
}

// ContainsJapanese returns true if the string contains Japanese characters (Kanji, Hiragana, Katakana).
func ContainsJapanese(s string) bool {
	for _, r := range s {
		if unicode.Is(unicode.Han, r) || unicode.Is(unicode.Hiragana, r) || unicode.Is(unicode.Katakana, r) {
			return true
		}
	}
	return false
}

// TrimLeftSpace returns a string with all leading spaces removed.
func TrimLeftSpace(s string) string {
	return strings.TrimLeftFunc(s, unicode.IsSpace)
}

// TrimRightSpace returns a string with all trailing spaces removed.
func TrimRightSpace(s string) string {
	return strings.TrimRightFunc(s, unicode.IsSpace)
}

// TrimInnerSpace returns a string with all inner, leading, trailing spaces removed.
func TrimInnerSpace(s string) string {
	return ystring.Shrink(s, EmptyStr)
}

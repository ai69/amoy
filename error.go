package amoy

import (
	"errors"
	"net"
	"os"
)

// ErrorCause returns the cause of an error.
func ErrorCause(err error) error {
	for {
		cause := errors.Unwrap(err)
		if cause == nil {
			return err
		}
		err = cause
	}
}

// IsTimeoutError checks if error is timeout error.
func IsTimeoutError(err error) bool {
	if err == nil {
		return false
	}
	return os.IsTimeout(err)
}

// IsNoNetworkError indicates whether the error is caused by no network.
func IsNoNetworkError(err error) bool {
	if err == nil {
		return false
	}
	var dnsError *net.DNSError
	return errors.As(err, &dnsError)
}

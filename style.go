package amoy

import (
	"strings"

	"github.com/1set/gut/ystring"
	"github.com/muesli/termenv"
)

var (
	term            = termenv.ColorProfile()
	lineBreakLength = 100

	// Simple style functions
	StyleIndex     = makeFlexFgStyle("238", "250")
	StyleMessage   = makeFgStyle("200")
	StyleName      = makeFgStyle("207")
	StyleDate      = makeFgStyle("82")
	StyleHighlight = makeFgStyle("227")
	StyleLabel     = makeFgStyle("51")
	StyleAmount    = makeFgStyle("207")
	StyleDot       = colorFg("•", "236")
	StyleDash      = colorFg("-", "236")
	StyleLineBreak = colorFg(strings.Repeat("█", lineBreakLength), "246")
)

var (
	styleUserName = makeFgStyle("160")
	styleAt       = colorFg("@", "123")
	styleDomain   = makeFgStyle("199")
)

// StyleLabeledLineBreak renders a line break with a label in the middle.
func StyleLabeledLineBreak(label string) string {
	maxLen := lineBreakLength - 3*2 // for label with max length like "=[ LABEL ]="
	if len(label) > maxLen {
		label = TruncateStr(label, maxLen)
	}
	labelLen := ystring.Length(label)
	leftPadLen := (lineBreakLength - labelLen - 4) / 2
	rightPadLen := lineBreakLength - labelLen - 4 - leftPadLen
	return strings.Repeat("=", leftPadLen) + "[ " + label + " ]" + strings.Repeat("=", rightPadLen)
}

// StyleEmail renders a string of email with terminal colors.
func StyleEmail(email string) string {
	if parts := strings.Split(email, "@"); len(parts) >= 2 {
		return styleUserName(parts[0]) + styleAt + styleDomain(parts[1])
	}
	return styleUserName(email)
}

// StyleBold renders a string in bold.
func StyleBold(val string) string {
	return termenv.String(val).Bold().String()
}

// StyleCrossOut renders a string with cross-out.
func StyleCrossOut(val string) string {
	return termenv.String(val).CrossOut().String()
}

// Color a string's foreground with the given value.
func colorFg(val, color string) string {
	return termenv.String(val).Foreground(term.Color(color)).String()
}

// Return a function that will colorize the foreground of a given string.
func makeFgStyle(color string) func(string) string {
	return termenv.Style{}.Foreground(term.Color(color)).Styled
}

// Return a function that will colorize the foreground of a given string.
func makeBgStyle(color string) func(string) string {
	return termenv.Style{}.Background(term.Color(color)).Styled
}

// Return a function that will colorize the foreground of a given string.
func makeFlexFgStyle(colorL, colorD string) func(string) string {
	var color string
	if termenv.HasDarkBackground() {
		color = colorD
	} else {
		color = colorL
	}
	return termenv.Style{}.Foreground(term.Color(color)).Styled
}

// Color a string's foreground and background with the given value.
func makeFgBgStyle(fg, bg string) func(string) string {
	return termenv.Style{}.
		Foreground(term.Color(fg)).
		Background(term.Color(bg)).
		Styled
}

func makeFlexFgBgStyle(fg, bgL, bgD string) func(string) string {
	var bg string
	if termenv.HasDarkBackground() {
		bg = bgD
	} else {
		bg = bgL
	}
	return termenv.Style{}.
		Foreground(term.Color(fg)).
		Background(term.Color(bg)).
		Styled
}

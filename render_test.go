package amoy

import "testing"

func TestRenderSparkline(t *testing.T) {
	tests := []struct {
		name string
		nums []float64
		want string
	}{
		{"Same", []float64{1, 1, 1, 1, 1}, `▁▁▁▁▁`},
		{"Peak", []float64{1, 2, 3, 4, 5, 6, 7, 8, 7, 6, 5, 4, 3, 2, 1}, `▁▂▃▄▅▆▇█▇▆▅▄▃▂▁`},
		{"Low and High", []float64{0, 1, 19.9, 20}, `▁▁▇█`},
		{"Negative", []float64{-20, -10, 0, 10, 20}, `▁▂▄▆█`},
		{"Normal", []float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, `▁▁▂▃▄▄▅▆▇█`},
		{"Large Numbers", []float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 100, 200}, `▁▁▁▁▁▁▁▁▁▁▄█`},
		{"Random Numbers", []float64{4, 2, 1, 6, 3, 9, 1, 4, 2, 15, 14, 9, 8, 6, 10, 13, 15, 12, 10, 5, 3, 6, 1}, `▂▁▁▃▂▅▁▂▁█▇▅▄▃▅▇█▆▅▃▂▃▁`},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RenderSparkline(tt.nums); got != tt.want {
				t.Errorf("RenderSparkline(%v) = %v, want %v", tt.nums, got, tt.want)
			}
		})
	}
}

func TestRenderPartialBlock(t *testing.T) {
	tests := []struct {
		name   string
		num    float64
		length int
		want   string
	}{
		{"Zero", 0, 1, `░`},
		{"One", 1, 1, `█`},
		{"Half", 0.5, 1, `▌`},
		{"Quarter", 0.25, 1, `▎`},
		{"Three Quarters", 0.75, 1, `▊`},
		{"Eighth", 0.125, 1, `▏`},
		{"Negative", -1, 1, `░`},
		{"Large", 2, 1, `█`},
		{"Length 2", 0.5, 2, `█░`},
		{"Length 3", 0.5, 3, `█▌░`},
		{"Length 4", 0.5, 4, `██░░`},
		{"Length 4 More", 0.748, 4, `███░`},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RenderPartialBlock(tt.num, tt.length); got != tt.want {
				t.Errorf("RenderPartialBlock(%.2f, %d) = %v, want %v", tt.num, tt.length, got, tt.want)
			}
		})
	}
}

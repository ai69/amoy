package amoy

import (
	"context"
	"errors"

	"golang.org/x/sync/errgroup"
)

type (
	// TaskRunFunc is a function that runs a task and returns the result with error.
	TaskRunFunc func(ctx context.Context, taskID int) (interface{}, error)
	// TaskResultFunc is a function that handles the result of a task.
	TaskResultFunc func(ctx context.Context, taskID int, result interface{}, err error)
)

// ParallelTaskRun runs a given number of tasks and handles results in parallel.
func ParallelTaskRun(ctx context.Context, workerNum, taskNum int, runFunc TaskRunFunc, doneFunc TaskResultFunc) error {
	// precondition check
	if workerNum < 1 {
		return errors.New("invalid worker number")
	}
	if taskNum < 1 {
		return errors.New("invalid task number")
	}

	// correct worker number
	num := EnsureRange(workerNum, 1, taskNum)
	g, ctx := errgroup.WithContext(ctx)

	// send task
	taskCh := make(chan int)
	g.Go(func() error {
		defer close(taskCh)
		// Sending task IDs to channel
		for i := 0; i < taskNum; i++ {
			select {
			case taskCh <- i:
			case <-ctx.Done():
				return ctx.Err()
			}
		}
		return nil
	})

	// result channel
	type result struct {
		task int
		res  interface{}
		err  error
	}
	resultCh := make(chan result)

	// creating a worker pool to run tasks
	for i := 0; i < num; i++ {
		g.Go(func() error {
			for task := range taskCh {
				res, err := runFunc(ctx, task)
				select {
				case <-ctx.Done():
					return ctx.Err()
				default:
					resultCh <- result{task, res, err}
				}
			}
			return nil
		})
	}

	// receive results
	go func() {
		g.Wait()
		close(resultCh)
	}()
	for res := range resultCh {
		if doneFunc != nil {
			doneFunc(ctx, res.task, res.res, res.err)
		}
	}

	// waiting for all the goroutines to finish
	return g.Wait()
}

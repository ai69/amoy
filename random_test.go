package amoy

import (
	"testing"
	"time"
)

func TestFeelLucky(t *testing.T) {
	tests := []struct {
		name string
		rate float64
		want bool
	}{
		{"never", 0, false},
		{"always", 1, true},
		//{"am i lucky", 0.5, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FeelLucky(tt.rate); got != tt.want {
				t.Errorf("FeelLucky(%.6f) = %v, want %v", tt.rate, got, tt.want)
			}
		})
	}
}

func TestRandomTime(t *testing.T) {
	now := time.Now()
	tests := []struct {
		name    string
		start   time.Time
		d       time.Duration
		wantMin time.Time
		wantMax time.Time
	}{
		{"positive", now, Hours(1), now, now.Add(Hours(1))},
		{"negative", now, -Hours(2), now.Add(Hours(-2)), now},
		{"zero", now, 0, now, now},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RandomTime(tt.start, tt.d); !(!tt.wantMin.After(got) && !got.After(tt.wantMax)) {
				t.Errorf("RandomTime() = %v, want [%v, %v)", got, tt.wantMin, tt.wantMax)
			}
		})
	}
}

func TestRandomTimeBetween(t *testing.T) {
	now := time.Now()
	tests := []struct {
		name    string
		start   time.Time
		end     time.Time
		wantMin time.Time
		wantMax time.Time
	}{
		{"before", now, now.Add(Hours(1)), now, now.Add(Hours(1))},
		{"after", now, now.Add(Hours(-1)), now, now},
		{"same", now, now, now, now},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RandomTimeBetween(tt.start, tt.end); !(!tt.wantMin.After(got) && !got.After(tt.wantMax)) {
				t.Errorf("RandomTimeBetween() = %v, want [%v, %v)", got, tt.wantMin, tt.wantMax)
			}
		})
	}
}

func TestRandomDuration(t *testing.T) {
	tests := []struct {
		name    string
		min     time.Duration
		max     time.Duration
		wantMin time.Duration
		wantMax time.Duration
	}{
		{"normal", Hours(1), Hours(2), Hours(1), Hours(2)},
		{"reversed", Hours(4), Hours(2), Hours(2), Hours(2)},
		{"same", Hours(5), Hours(5), Hours(5), Hours(5)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RandomDuration(tt.min, tt.max); !(tt.wantMin <= got && got <= tt.wantMax) {
				t.Errorf("RandomDuration() = %v, want [%v, %v)", got, tt.wantMin, tt.wantMax)
			}
		})
	}
}

func TestRandomInt(t *testing.T) {
	tests := []struct {
		name string
		min  int
		max  int
	}{
		{"normal", 1, 10},
		{"nearby", 1, 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RandomInt(tt.min, tt.max); got < tt.min || tt.max <= got {
				t.Errorf("RandomInt(%v, %v) = %v, not included", tt.min, tt.max, got)
			}
		})
	}
}

func TestRandomFloat(t *testing.T) {
	tests := []struct {
		name string
		min  float64
		max  float64
	}{
		{"normal", 0, 1},
		{"nearby", 1, 2},
		{"wide", 1, 200},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RandomFloat(tt.min, tt.max); got < tt.min || tt.max <= got {
				t.Errorf("RandomFloat(%v, %v) = %v, not included", tt.min, tt.max, got)
			}
		})
	}
}

func TestRandomString(t *testing.T) {
	tests := []struct {
		name   string
		length int
	}{
		// {"zero", 0},
		{"one", 1},
		{"normal", 10},
		{"large", 1024},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RandomString(tt.length); len(got) != tt.length {
				t.Errorf("RandomString(%v) = %v, not enough", tt.length, got)
			}
		})
	}
}

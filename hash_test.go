package amoy

import (
	"reflect"
	"testing"
)

func TestFNV32a(t *testing.T) {
	tests := []struct {
		name string
		text string
		want uint32
	}{
		{"Empty", EmptyStr, 0x811c9dc5},
		{"A", "A", 0xc40bf6cc},
		{"a", "a", 0xe40c292c},
		{"b", "b", 0xe70c2de5},
		{"Aloha", "Aloha", 0xa2d84aca},
		{"Long", "人活着得有个好心态，这样才能咂摸出日子的那个甜味儿来。", 0x16951d1e},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FNV32a(tt.text); got != tt.want {
				t.Errorf("FNV32a(%v) = %v, want %v", tt.text, got, tt.want)
			}
		})
	}
}

func TestFNV64a(t *testing.T) {
	tests := []struct {
		name string
		text string
		want uint64
	}{
		{"Empty", EmptyStr, 0xcbf29ce484222325},
		{"A", "A", 0xaf63fc4c860222ec},
		{"a", "a", 0xaf63dc4c8601ec8c},
		{"b", "b", 0xaf63df4c8601f1a5},
		{"Aloha", "Aloha", 0x48ace7cbbea0d4aa},
		{"Long", "人活着得有个好心态，这样才能咂摸出日子的那个甜味儿来。", 0x06d36942d24bac9e},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FNV64a(tt.text); got != tt.want {
				t.Errorf("FNV64a(%v) = %v, want %v", tt.text, got, tt.want)
			}
		})
	}
}

func TestFNV128a(t *testing.T) {
	tests := []struct {
		name string
		text string
		want []byte
	}{
		{"Empty", EmptyStr, []byte{0x6c, 0x62, 0x27, 0x2e, 0x07, 0xbb, 0x01, 0x42, 0x62, 0xb8, 0x21, 0x75, 0x62, 0x95, 0xc5, 0x8d}},
		{"A", "A", []byte{0xd2, 0x28, 0xcb, 0x69, 0x4f, 0x1a, 0x8c, 0xaf, 0x78, 0x91, 0x2b, 0x70, 0x4e, 0x4a, 0x62, 0x04}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FNV128a(tt.text); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FNV128a(%v) = %v, want %v", tt.text, got, tt.want)
			}
		})
	}
}

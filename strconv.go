package amoy

import "strconv"

/* Itoa */

// Itoa is equivalent to strconv.Itoa(i).
func Itoa(i int) string {
	return strconv.Itoa(i)
}

// Itoa64 is equivalent to strconv.FormatInt(i, 10).
func Itoa64(i int64) string {
	return strconv.FormatInt(i, 10)
}

// Itoa32 is equivalent to strconv.FormatInt(int64(i), 10).
func Itoa32(i int32) string {
	return strconv.FormatInt(int64(i), 10)
}

// Itoa16 is equivalent to strconv.FormatInt(int64(i), 10).
func Itoa16(i int16) string {
	return strconv.FormatInt(int64(i), 10)
}

// Itoa8 is equivalent to strconv.FormatInt(int64(i), 10).
func Itoa8(i int8) string {
	return strconv.FormatInt(int64(i), 10)
}

/* Utoa */

// Utoa is equivalent to strconv.FormatUint(uint64(i), 10).
func Utoa(i uint) string {
	return strconv.FormatUint(uint64(i), 10)
}

// Utoa64 is equivalent to strconv.FormatUint(i, 10).
func Utoa64(i uint64) string {
	return strconv.FormatUint(i, 10)
}

// Utoa32 is equivalent to strconv.FormatUint(uint64(i), 10).
func Utoa32(i uint32) string {
	return strconv.FormatUint(uint64(i), 10)
}

// Utoa16 is equivalent to strconv.FormatUint(uint64(i), 10).
func Utoa16(i uint16) string {
	return strconv.FormatUint(uint64(i), 10)
}

// Utoa8 is equivalent to strconv.FormatUint(uint64(i), 10).
func Utoa8(i uint8) string {
	return strconv.FormatUint(uint64(i), 10)
}

/* Atoi */

// Atoi is equivalent to strconv.Atoi(s).
func Atoi(s string) (int, error) {
	return strconv.Atoi(s)
}

// Atoi64 is equivalent to strconv.ParseInt(s, 10, 64).
func Atoi64(s string) (int64, error) {
	return strconv.ParseInt(s, 10, 64)
}

// Atoi32 is equivalent to strconv.ParseInt(s, 10, 32).
func Atoi32(s string) (int32, error) {
	i, err := strconv.ParseInt(s, 10, 32)
	if err != nil {
		return 0, err
	}
	return int32(i), nil
}

// Atoi16 is equivalent to strconv.ParseInt(s, 10, 16).
func Atoi16(s string) (int16, error) {
	i, err := strconv.ParseInt(s, 10, 16)
	if err != nil {
		return 0, err
	}
	return int16(i), nil
}

// Atoi8 is equivalent to strconv.ParseInt(s, 10, 8).
func Atoi8(s string) (int8, error) {
	i, err := strconv.ParseInt(s, 10, 8)
	if err != nil {
		return 0, err
	}
	return int8(i), nil
}

/* Atou */

// Atou is equivalent to strconv.ParseUint(s, 10, 0).
func Atou(s string) (uint, error) {
	i, err := strconv.ParseUint(s, 10, 0)
	if err != nil {
		return 0, err
	}
	return uint(i), nil
}

// Atou64 is equivalent to strconv.ParseUint(s, 10, 64).
func Atou64(s string) (uint64, error) {
	return strconv.ParseUint(s, 10, 64)
}

// Atou32 is equivalent to strconv.ParseUint(s, 10, 32).
func Atou32(s string) (uint32, error) {
	i, err := strconv.ParseUint(s, 10, 32)
	if err != nil {
		return 0, err
	}
	return uint32(i), nil
}

// Atou16 is equivalent to strconv.ParseUint(s, 10, 16).
func Atou16(s string) (uint16, error) {
	i, err := strconv.ParseUint(s, 10, 16)
	if err != nil {
		return 0, err
	}
	return uint16(i), nil
}

// Atou8 is equivalent to strconv.ParseUint(s, 10, 8).
func Atou8(s string) (uint8, error) {
	i, err := strconv.ParseUint(s, 10, 8)
	if err != nil {
		return 0, err
	}
	return uint8(i), nil
}

/* Atof */

// Atof64 is equivalent to strconv.ParseFloat(s, 64).
func Atof64(s string) (float64, error) {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0, err
	}
	return f, nil
}

// Atof32 is equivalent to strconv.ParseFloat(s, 32).
func Atof32(s string) (float32, error) {
	f, err := strconv.ParseFloat(s, 32)
	if err != nil {
		return 0, err
	}
	return float32(f), nil
}

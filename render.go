package amoy

import (
	"math"
	"strings"

	tw "github.com/olekukonko/tablewriter"
)

// RenderTableString renders the rows as table and returns as string for console.
func RenderTableString(header []string, rows [][]string) string {
	s := strings.Builder{}
	table := tw.NewWriter(&s)
	table.SetHeader(header)
	table.SetHeaderAlignment(tw.ALIGN_LEFT)
	table.SetHeaderLine(false)
	table.SetTablePadding("\t")
	table.SetAlignment(tw.ALIGN_LEFT)
	table.SetAutoWrapText(false)
	table.SetAutoFormatHeaders(true)
	table.SetBorders(tw.Border{Left: true, Top: false, Right: true, Bottom: false})
	table.SetCenterSeparator(EmptyStr)
	table.SetColumnSeparator(EmptyStr)
	table.SetRowSeparator(EmptyStr)

	for _, r := range rows {
		table.Append(r)
	}

	table.Render()
	return s.String()
}

var (
	sparkLineLevels = []rune("▁▂▃▄▅▆▇█")
	blocks          = []rune(`░▏▎▍▌▋▊▉█`)
)

// RenderSparkline generates a sparkline string like ▅▆▂▂▅▇▂▂▃▆▆▆▅▃ from a slice of float64.
func RenderSparkline(nums []float64) string {
	n := len(nums)
	if n == 0 {
		return ""
	}
	min := math.Inf(1)
	max := math.Inf(-1)
	for _, y := range nums {
		if y < min {
			min = y
		}
		if y > max {
			max = y
		}
	}
	if max == min {
		return strings.Repeat(string(sparkLineLevels[0]), n)
	}
	var (
		line  = make([]rune, n)
		ratio = (float64(len(sparkLineLevels)) - 1) / (max - min)
	)
	for i := range nums {
		j := int(math.Floor(ratio * (nums[i] - min)))
		line[i] = sparkLineLevels[j]
	}
	return string(line)
}

// RenderPartialBlock generates a partial block string like █▍░░ from a float64 between 0 and 1.
func RenderPartialBlock(num float64, length int) string {
	// length must be positive
	if length < 1 {
		length = 1
	}
	// num must be between 0 and 1
	if num < 0 {
		num = 0
	} else if num > 1 {
		num = 1
	}

	// total number of blocks, i.e. each rune contains 8 blocks
	total := length * 8
	prog := int(math.Round(num * float64(total)))

	var sb strings.Builder
	// full blocks
	if n := prog / 8; n > 0 {
		sb.WriteString(strings.Repeat(string(blocks[8]), n))
	}
	// partial block
	if n := prog % 8; n > 0 {
		sb.WriteRune(blocks[n])
	}
	// empty blocks
	if n := (total - prog) / 8; n > 0 {
		sb.WriteString(strings.Repeat(string(blocks[0]), n))
	}
	return sb.String()
}

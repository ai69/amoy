package amoy

import (
	"regexp"

	ys "github.com/1set/gut/ystring"
)

// NamedValues represents a named value map for named capturing groups.
type NamedValues map[string]string

// ExtractNamedValues returns a named value map with the given compiled regular expression and original string.
func ExtractNamedValues(r *regexp.Regexp, str string) NamedValues {
	subMatchMap := make(NamedValues)
	if r == nil {
		return subMatchMap
	}
	if match := r.FindStringSubmatch(str); len(match) > 0 {
		for i, name := range r.SubexpNames() {
			if i != 0 && ys.IsNotEmpty(name) {
				subMatchMap[name] = match[i]
			}
		}
	}
	return subMatchMap
}

// IsEmpty indicates if the given map is empty.
func (l NamedValues) IsEmpty() bool {
	return len(l) == 0
}

// RegexMatch reports whether the string s contains any match of the regular expression pattern.
func RegexMatch(pat, s string) (bool, error) {
	return regexp.MatchString(pat, s)
}

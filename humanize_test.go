package amoy

import (
	"math"
	"testing"
	"time"
)

func TestHumanizeDuration(t *testing.T) {
	tests := []struct {
		name     string
		duration time.Duration
		want     string
	}{
		{"1 Second", 1 * Second, "1 sec"},
		{"2 Seconds", 2 * Second, "2 secs"},
		{"51 Minutes 52 Seconds", 51*Minute + 52*Second, "51 mins 52 secs"},
		{"3 Days 2 Hours 1 Minutes", 3*Day + 2*Hour + 1*Minute, "3 days 2 hours 1 min"},
		{"1 Week", 1 * Week, "7 days"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := HumanizeDuration(tt.duration); got != tt.want {
				t.Errorf("HumanizeDuration() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHumanizeBytes(t *testing.T) {
	tests := []struct {
		name string
		s    uint64
		want string
	}{
		{"Sample", 82854982, "83 MB"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := HumanizeBytes(tt.s); got != tt.want {
				t.Errorf("HumanizeBytes() = %v, wantF %v", got, tt.want)
			}
		})
	}
}

func TestHumanizeIBytes(t *testing.T) {
	tests := []struct {
		name string
		s    uint64
		want string
	}{
		{"Sample", 82854982, "79 MiB"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := HumanizeIBytes(tt.s); got != tt.want {
				t.Errorf("HumanizeIBytes() = %v, wantF %v", got, tt.want)
			}
		})
	}
}

func TestParseHumanizedBytes(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    uint64
		wantErr bool
	}{
		{"Sample 1", "42 MB", 42000000, false},
		{"Sample 2", "42 mib", 44040192, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseHumanizedBytes(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseHumanizedBytes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ParseHumanizedBytes() got = %v, wantF %v", got, tt.want)
			}
		})
	}
}

func TestComputeSI(t *testing.T) {
	tests := []struct {
		name  string
		input float64
		wantF float64
		wantS string
	}{
		{"Sample", 2.2345e-12, 2.2345, "p"},
		{"Attempt", 0.01, 10, "m"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotF, gotS := ComputeSI(tt.input)
			if math.Abs(gotF-tt.wantF) > 1e-6 {
				t.Errorf("ComputeSI() gotF = %v, wantF %v", gotF, tt.wantF)
			}
			if gotS != tt.wantS {
				t.Errorf("ComputeSI() gotS = %v, wantF %v", gotS, tt.wantS)
			}
		})
	}
}

func TestParseSI(t *testing.T) {
	tests := []struct {
		name    string
		input   string
		wantF   float64
		wantS   string
		wantErr bool
	}{
		{"Sample", "2.2345 pF", 2.2345e-12, "F", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotF, gotS, err := ParseSI(tt.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseSI() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if math.Abs(gotF-tt.wantF) > 1e-6 {
				t.Errorf("ParseSI() gotF = %v, wantF %v", gotF, tt.wantF)
			}
			if gotS != tt.wantS {
				t.Errorf("ParseSI() gotS = %v, wantF %v", gotS, tt.wantS)
			}
		})
	}
}

func TestSI(t *testing.T) {
	tests := []struct {
		name  string
		input float64
		unit  string
		want  string
	}{
		{"Sample1", 1000000, "B", "1 MB"},
		{"Sample2", 2.2345e-12, "F", "2.2345 pF"},
		{"Attempt1", 1000, "", "1 k"},
		{"Attempt2", 1024, "", "1.024 k"},
		{"Attempt3", 2300000000, "Hz", "2.3 GHz"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SI(tt.input, tt.unit); got != tt.want {
				t.Errorf("SI() = %v, wantF %v", got, tt.want)
			}
		})
	}
}

func TestSIWithDigits(t *testing.T) {
	tests := []struct {
		name     string
		input    float64
		decimals int
		unit     string
		want     string
	}{
		{"Sample1", 1000000, 0, "B", "1 MB"},
		{"Sample2", 2.2345e-12, 2, "F", "2.23 pF"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SIWithDigits(tt.input, tt.decimals, tt.unit); got != tt.want {
				t.Errorf("SIWithDigits() = %v, wantF %v", got, tt.want)
			}
		})
	}
}

func TestFtoa(t *testing.T) {
	tests := []struct {
		name string
		num  float64
		want string
	}{
		{"Attempt1", 0.123, "0.123"},
		{"Attempt2", 1.000, "1"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Ftoa(tt.num); got != tt.want {
				t.Errorf("Ftoa() = %v, wantF %v", got, tt.want)
			}
		})
	}
}

func TestFtoaWithDigits(t *testing.T) {
	tests := []struct {
		name   string
		num    float64
		digits int
		want   string
	}{
		{"Attempt1", 0.123, 2, "0.12"},
		{"Attempt2", 1.000, 2, "1"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FtoaWithDigits(tt.num, tt.digits); got != tt.want {
				t.Errorf("FtoaWithDigits() = %v, wantF %v", got, tt.want)
			}
		})
	}
}

package amoy

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"time"

	"github.com/1set/gut/ystring"
)

// HTTPClientOptions represents options for HTTPClient.
type HTTPClientOptions struct {
	// Client is the underlying http.Client, you can set it to use your own client with transport.
	Client *http.Client
	// Timeout is the maximum amount of time a dial will wait for a connect to complete.
	Timeout time.Duration
	// UserAgent is the User-Agent header value.
	UserAgent string
	// Headers is the default HTTP headers.
	Headers map[string]string
	// Insecure indicates whether to skip TLS verification.
	Insecure bool
	// DisableRedirect indicates whether to disable redirect for HTTP 301, 302, 303, 307, 308.
	DisableRedirect bool
	// Username is the username for basic authentication.
	Username string
	// Password is the password for basic authentication.
	Password string
	// BearerToken is the bearer token for token authentication. It will override the basic authentication if it's set together.
	BearerToken string
}

// HTTPClient is a wrapper of http.Client with some helper methods.
type HTTPClient struct {
	*http.Client
	timeout time.Duration
	headers map[string]string // default headers for user agent, auth, etc.
}

var (
	defaultHTTPClientOpts = &HTTPClientOptions{
		Timeout:   30 * time.Second,
		UserAgent: fmt.Sprintf("Go-amoy-http/%s", Version), // "User-Agent": "Go-http-client/2.0",
	}
)

// NewHTTPClient creates a new HTTPClient.
func NewHTTPClient(opts *HTTPClientOptions) *HTTPClient {
	// if opts is nil, use default options
	if opts == nil {
		opts = defaultHTTPClientOpts
	} else {
		// if opts is not nil, but some fields are empty, use default options
		if opts.Timeout <= 0 {
			opts.Timeout = defaultHTTPClientOpts.Timeout
		}
		if ystring.IsEmpty(opts.UserAgent) {
			opts.UserAgent = defaultHTTPClientOpts.UserAgent
		}
	}

	// create a new HTTPClient
	hc := opts.Client
	if hc == nil {
		// didn't bring its own HTTP client, create a new one with timeout
		hc = &http.Client{
			Timeout: opts.Timeout,
		}
	}
	cli := &HTTPClient{
		Client:  hc,
		timeout: opts.Timeout,
		headers: make(map[string]string, 2+len(opts.Headers)),
	}

	// clone the default transport and set InsecureSkipVerify to true
	if opts.Insecure {
		tr := http.DefaultTransport.(*http.Transport).Clone()
		tr.TLSClientConfig.InsecureSkipVerify = true
		cli.Client.Transport = tr
	}

	// disable redirect
	if opts.DisableRedirect {
		cli.Client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}
	}

	// set user agent header
	if ystring.IsNotEmpty(opts.UserAgent) {
		cli.headers["User-Agent"] = opts.UserAgent
	}

	// set auth header, if basic auth or bearer token both exist, use bearer token
	if ystring.IsNotEmpty(opts.Username) || ystring.IsNotEmpty(opts.Password) {
		auth := opts.Username + ":" + opts.Password
		ba := base64.StdEncoding.EncodeToString([]byte(auth))
		cli.headers["Authorization"] = "Basic " + ba
	}
	if ystring.IsNotEmpty(opts.BearerToken) {
		cli.headers["Authorization"] = "Bearer " + opts.BearerToken
	}

	// merge default headers and user headers
	if opts.Headers != nil {
		for k, v := range opts.Headers {
			cli.headers[k] = v
		}
	}

	return cli
}

// Custom performs a custom request with given method, url, query arguments, headers and io.Reader payload as body.
func (c *HTTPClient) Custom(method, url string, queryArgs, headers map[string]string, payload io.Reader) ([]byte, error) {
	return c.sendRequest(method, url, queryArgs, headers, payload)
}

// Get performs a GET request. It shadows the http.Client.Get method.
func (c *HTTPClient) Get(url string, queryArgs, headers map[string]string) ([]byte, error) {
	return c.sendRequest(http.MethodGet, url, queryArgs, headers, nil)
}

// GetJSON performs a GET request, parses the JSON-encoded response data and stores in the value pointed to by result.
func (c *HTTPClient) GetJSON(url string, queryArgs, headers map[string]string, result interface{}) ([]byte, error) {
	// set content type to application/json
	if headers == nil {
		headers = make(map[string]string, 1)
	}
	headers["Content-Type"] = "application/json"
	// send request
	resp, err := c.sendRequest(http.MethodGet, url, queryArgs, headers, nil)
	if err != nil {
		return nil, err
	}
	// unmarshal the response body into result
	if err = json.Unmarshal(resp, result); err != nil {
		return resp, fmt.Errorf("amoy http: response: %w", err)
	}
	return resp, nil
}

// Post performs a POST request with given io.Reader payload as body. It shadows the http.Client.Post method.
func (c *HTTPClient) Post(url string, queryArgs, headers map[string]string, payload io.Reader) ([]byte, error) {
	return c.sendRequest(http.MethodPost, url, queryArgs, headers, payload)
}

// PostData performs a POST request with given bytes payload as body.
func (c *HTTPClient) PostData(url string, queryArgs, headers map[string]string, payload []byte) ([]byte, error) {
	return c.sendRequest(http.MethodPost, url, queryArgs, headers, bytes.NewReader(payload))
}

// PostForm performs a POST request with given form data as body.
func (c *HTTPClient) PostForm(url string, queryArgs, headers map[string]string, form url.Values) ([]byte, error) {
	// set content type to application/x-www-form-urlencoded
	if headers == nil {
		headers = make(map[string]string, 1)
	}
	headers["Content-Type"] = "application/x-www-form-urlencoded"
	// send request
	return c.sendRequest(http.MethodPost, url, queryArgs, headers, strings.NewReader(form.Encode()))
}

// PostJSON performs a POST request with given JSON payload as body, parses the JSON-encoded response data and stores in the value pointed to by result.
func (c *HTTPClient) PostJSON(url string, queryArgs, headers map[string]string, payload, result interface{}) ([]byte, error) {
	// set content type to application/json
	if headers == nil {
		headers = make(map[string]string, 1)
	}
	headers["Content-Type"] = "application/json"
	// marshal payload
	var body io.Reader
	if !IsInterfaceNil(payload) {
		bs, err := json.Marshal(payload)
		if err != nil {
			return nil, fmt.Errorf("amoy http: request: %w", err)
		}
		body = bytes.NewReader(bs)
	}
	// send request
	resp, err := c.sendRequest(http.MethodPost, url, queryArgs, headers, body)
	if err != nil {
		return nil, err
	}
	// unmarshal the response body into result
	if err = json.Unmarshal(resp, result); err != nil {
		return resp, fmt.Errorf("amoy http: response: %w", err)
	}
	return resp, nil
}

func (c *HTTPClient) sendRequest(method, url string, queryArgs, headers map[string]string, payload io.Reader) ([]byte, error) {
	// create a new request
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		return nil, fmt.Errorf("amoy http: request: %w", err)
	}

	// apply query arguments
	if queryArgs != nil {
		q := req.URL.Query()
		for k, v := range queryArgs {
			q.Add(k, v)
		}
		req.URL.RawQuery = q.Encode()
	}
	// apply default headers
	if c.headers != nil {
		for k, v := range c.headers {
			req.Header.Set(k, v)
		}
	}
	// apply user headers
	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}

	// send request
	resp, err := c.Client.Do(req)
	if err != nil {
		if IsTimeoutError(err) {
			return nil, fmt.Errorf("amoy http: time out: %v", c.timeout)
		} else if IsNoNetworkError(err) {
			return nil, fmt.Errorf("amoy http: no such host: %w", err)
		} else {
			return nil, fmt.Errorf("amoy http: error: %w", err)
		}
	}
	defer resp.Body.Close()

	// check response status code
	if !((http.StatusOK <= resp.StatusCode) && (resp.StatusCode < http.StatusMultipleChoices)) {
		return nil, fmt.Errorf("amoy http: status code: %d", resp.StatusCode)
	}

	// read response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("amoy http: response body: %w", err)
	}
	return body, nil
}

// PostJSONWithHeaders sends payload in JSON to target URL with given timeout and headers and parses response as JSON.
func PostJSONWithHeaders(url string, dataReq, dataResp interface{}, timeout time.Duration, headers map[string]string) error {
	return postJSON(url, dataReq, dataResp, timeout, headers, EmptyStr, EmptyStr)
}

// PostJSONAndDumps sends payload in JSON to target URL with given timeout and headers and dumps response and parses response as JSON.
func PostJSONAndDumps(url string, dataReq, dataResp interface{}, timeout time.Duration, headers map[string]string, dumpReqPath, dumpRespPath string) error {
	return postJSON(url, dataReq, dataResp, timeout, headers, dumpReqPath, dumpRespPath)
}

func postJSON(url string, dataReq, dataResp interface{}, timeout time.Duration, headers map[string]string, dumpReqPath, dumpRespPath string) error {
	var (
		client = http.Client{
			Timeout: timeout,
		}
		req      *http.Request
		resp     *http.Response
		reqJSON  []byte
		respJSON []byte
		err      error
	)

	// build payload
	if !IsInterfaceNil(dataReq) {
		if reqJSON, err = json.Marshal(dataReq); err != nil {
			return fmt.Errorf("fail to marshal request, error: %w, data: %s", err, dataReq)
		}
	}

	// build request
	if req, err = http.NewRequest("POST", url, bytes.NewReader(reqJSON)); err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	for k, v := range headers {
		req.Header.Set(k, v)
	}

	// send request
	if resp, err = client.Do(req); err != nil {
		return err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	// read response
	if respJSON, err = ioutil.ReadAll(resp.Body); err != nil {
		return err
	}

	// dump request
	if ystring.IsNotBlank(dumpReqPath) {
		if data, err := httputil.DumpRequest(req, false); err == nil {
			_ = ioutil.WriteFile(dumpReqPath, append(data, reqJSON...), 0644)
		}
	}

	// dump response
	if ystring.IsNotBlank(dumpRespPath) {
		data, err := httputil.DumpResponse(resp, false)
		if err == nil {
			_ = ioutil.WriteFile(dumpRespPath, append(data, respJSON...), 0644)
		}
	}

	// check status code
	if !(200 <= resp.StatusCode && resp.StatusCode <= 299) {
		return fmt.Errorf("http error status, code: %d, body: %s", resp.StatusCode, respJSON)
	}

	// parse response
	if err := json.Unmarshal(respJSON, dataResp); err != nil {
		return fmt.Errorf("fail to unmarshal response, error: %w, body: %s", err, respJSON)
	}
	return nil
}

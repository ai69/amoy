package amoy

import (
	"reflect"
	"testing"
	"time"
)

func TestDate_Preview(t *testing.T) {
	t.Logf("ZeroDate = %v", ZeroDate)
	t.Logf("MinDate  = %v", MinDate)
	t.Logf("MaxDate  = %v", MaxDate)

	t.Logf("Today    : %v", Today())
	t.Logf("UTCToday : %v", UTCToday())
	t.Logf("Yesterday: %v", Yesterday())
	t.Logf("UTCYesterday: %v", UTCYesterday())
	t.Logf("Tomorrow : %v", Tomorrow())
	t.Logf("UTCTomorrow : %v", UTCTomorrow())

	t.Logf("1stDayOfYear   : %v", FirstDayOfYear())
	t.Logf("LastDayOfYear  : %v", LastDayOfYear())
	t.Logf("1stDayOfMonth  : %v", FirstDayOfMonth())
	t.Logf("LastDayOfMonth : %v", LastDayOfMonth())

	t.Logf("ThisYear  : %v", ThisYear())
	t.Logf("LastYear  : %v", LastYear())
	t.Logf("NextYear  : %v", NextYear())
}

func TestDate_Sub(t *testing.T) {
	tests := []struct {
		name string
		d    Date
		t    Date
		want time.Duration
	}{
		{"Sub Zero", NewDate(2021, 10, 1), NewDate(2021, 10, 1), ZeroDuration},
		{"Sub 1d", NewDate(2021, 10, 2), NewDate(2021, 10, 3), Days(-1)},
		{"Sub -1d", NewDate(2021, 10, 3), NewDate(2021, 10, 2), Days(1)},
		{"Sub 10d", NewDate(2022, 11, 11), NewDate(2022, 11, 1), Days(10)},
		{"First-1d", NewDate(1, 1, 1), NewDate(0, 12, 31), Days(1)},
		{"Last+1d", NewDate(9999, 12, 31), NewDate(10000, 1, 1), Days(-1)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.d.Sub(tt.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("%v Sub(%v): got %v, want %v", tt.d, tt.t, got, tt.want)
			}
		})
	}
}

func TestDate_Add(t *testing.T) {
	tests := []struct {
		name string
		d    Date
		t    time.Duration
		want Date
	}{
		{"Add Zero", NewDate(2021, 10, 1), ZeroDuration, NewDate(2021, 10, 1)},
		{"Add 1d", NewDate(2021, 10, 2), Days(1), NewDate(2021, 10, 3)},
		{"Add -1d", NewDate(2021, 10, 3), Days(-1), NewDate(2021, 10, 2)},
		{"First-1d", NewDate(1, 1, 1), Days(-1), NewDate(0, 12, 31)},
		{"Last+1d", NewDate(9999, 12, 31), Days(1), NewDate(10000, 1, 1)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.d.Add(tt.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("%v Add(%v): got %v, want %v", tt.d, tt.t, got, tt.want)
			}
		})
	}
}

func TestDate_AddDay(t *testing.T) {
	tests := []struct {
		name string
		d    Date
		n    int
		want Date
	}{
		{"Add Zero", NewDate(2021, 10, 1), 0, NewDate(2021, 10, 1)},
		{"Add 1d", NewDate(2021, 10, 2), 1, NewDate(2021, 10, 3)},
		{"Add -1d", NewDate(2021, 10, 3), -1, NewDate(2021, 10, 2)},
		{"Add 100d", NewDate(2021, 10, 2), 100, NewDate(2022, 1, 10)},
		{"Add -100d", NewDate(2021, 10, 3), -100, NewDate(2021, 6, 25)},
		{"Add 1000d", NewDate(2021, 1, 1), 1000, NewDate(2023, 9, 28)},
		{"Add -1000d", NewDate(2021, 1, 1), -1000, NewDate(2018, 4, 7)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.d.AddDay(tt.n); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("%v AddDay(%v): got %v, want %v", tt.d, tt.n, got, tt.want)
			}
		})
	}
}

func TestDate_AddMonth(t *testing.T) {
	tests := []struct {
		name string
		d    Date
		n    int
		want Date
	}{
		{"Add Zero", NewDate(2021, 10, 1), 0, NewDate(2021, 10, 1)},
		{"Add 1m", NewDate(2021, 10, 2), 1, NewDate(2021, 11, 2)},
		{"Add -1m", NewDate(2021, 10, 3), -1, NewDate(2021, 9, 3)},
		{"Add 3m", NewDate(2021, 10, 5), 3, NewDate(2022, 1, 5)},
		{"Add -12m", NewDate(2021, 10, 6), -12, NewDate(2020, 10, 6)},
		{"Add 100m", NewDate(2021, 1, 1), 100, NewDate(2029, 5, 1)},
		{"Add -100m", NewDate(2021, 1, 1), -100, NewDate(2012, 9, 1)},
		{"Add 1000m", NewDate(2021, 1, 1), 1000, NewDate(2104, 5, 1)},
		{"Add -1000m", NewDate(2021, 1, 1), -1000, NewDate(1937, 9, 1)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.d.AddMonth(tt.n); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("%v AddMonth(%v): got %v, want %v", tt.d, tt.n, got, tt.want)
			}
		})
	}
}

func TestDate_AddYear(t *testing.T) {
	tests := []struct {
		name string
		d    Date
		n    int
		want Date
	}{
		{"Add Zero", NewDate(2021, 10, 1), 0, NewDate(2021, 10, 1)},
		{"Add 1y", NewDate(2021, 10, 2), 1, NewDate(2022, 10, 2)},
		{"Add -1y", NewDate(2021, 10, 3), -1, NewDate(2020, 10, 3)},
		{"Add 3y", NewDate(2021, 10, 5), 3, NewDate(2024, 10, 5)},
		{"Add -6y", NewDate(2021, 10, 6), -6, NewDate(2015, 10, 6)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.d.AddYear(tt.n); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("%v AddYear(%v): got %v, want %v", tt.d, tt.n, got, tt.want)
			}
		})
	}
}

func TestDate_After(t *testing.T) {
	tests := []struct {
		name string
		a    Date
		b    Date
		want bool
	}{
		{"Same", NewDate(2021, 10, 10), NewDate(2021, 10, 10), false},
		{"Day_Before", NewDate(2021, 10, 9), NewDate(2021, 10, 10), false},
		{"Day_After", NewDate(2021, 10, 11), NewDate(2021, 10, 10), true},
		{"Month_Before", NewDate(2021, 10, 10), NewDate(2021, 11, 10), false},
		{"Month_After", NewDate(2021, 10, 10), NewDate(2021, 9, 10), true},
		{"Year_Before", NewDate(2021, 10, 10), NewDate(2022, 10, 10), false},
		{"Year_After", NewDate(2021, 10, 10), NewDate(2020, 10, 10), true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.a.After(tt.b); got != tt.want {
				t.Errorf("%v After(%v): got %v, want %v", tt.a, tt.b, got, tt.want)
			}
		})
	}
}

func TestDate_Before(t *testing.T) {
	tests := []struct {
		name string
		a    Date
		b    Date
		want bool
	}{
		{"Same", NewDate(2021, 10, 10), NewDate(2021, 10, 10), false},
		{"Day_Before", NewDate(2021, 10, 9), NewDate(2021, 10, 10), true},
		{"Day_After", NewDate(2021, 10, 11), NewDate(2021, 10, 10), false},
		{"Month_Before", NewDate(2021, 10, 10), NewDate(2021, 11, 10), true},
		{"Month_After", NewDate(2021, 10, 10), NewDate(2021, 9, 10), false},
		{"Year_Before", NewDate(2021, 10, 10), NewDate(2022, 10, 10), true},
		{"Year_After", NewDate(2021, 10, 10), NewDate(2020, 10, 10), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.a.Before(tt.b); got != tt.want {
				t.Errorf("%v Before(%v): got %v, want %v", tt.a, tt.b, got, tt.want)
			}
		})
	}
}

func TestDate_Equal_IsSameDay(t *testing.T) {
	tests := []struct {
		name string
		a    Date
		b    Date
		want bool
	}{
		{"Same", NewDate(2021, 10, 10), NewDate(2021, 10, 10), true},
		{"Day_Before", NewDate(2021, 10, 9), NewDate(2021, 10, 10), false},
		{"Day_After", NewDate(2021, 10, 11), NewDate(2021, 10, 10), false},
		{"Month_Before", NewDate(2021, 10, 10), NewDate(2021, 11, 10), false},
		{"Month_After", NewDate(2021, 10, 10), NewDate(2021, 9, 10), false},
		{"Year_Before", NewDate(2021, 10, 10), NewDate(2022, 10, 10), false},
		{"Year_After", NewDate(2021, 10, 10), NewDate(2020, 10, 10), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.a.Equal(tt.b); got != tt.want {
				t.Errorf("%v Equal(%v): got %v, want %v", tt.a, tt.b, got, tt.want)
			}
			if got := tt.b.IsSameDay(tt.a); got != tt.want {
				t.Errorf("%v IsSameDay(%v): got %v, want %v", tt.b, tt.a, got, tt.want)
			}
		})
	}
}

func TestDate_IsSameYear(t *testing.T) {
	tests := []struct {
		name string
		a    Date
		b    Date
		want bool
	}{
		{"Same", NewDate(2021, 10, 10), NewDate(2021, 10, 10), true},
		{"Day_Before", NewDate(2021, 10, 9), NewDate(2021, 10, 10), true},
		{"Day_After", NewDate(2021, 10, 11), NewDate(2021, 10, 10), true},
		{"Month_Before", NewDate(2021, 10, 10), NewDate(2021, 11, 10), true},
		{"Month_After", NewDate(2021, 10, 10), NewDate(2021, 9, 10), true},
		{"Year_Before", NewDate(2021, 10, 10), NewDate(2022, 10, 10), false},
		{"Year_After", NewDate(2021, 10, 10), NewDate(2020, 10, 10), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.a.IsSameYear(tt.b); got != tt.want {
				t.Errorf("%v IsSameYear(%v): got %v, want %v", tt.a, tt.b, got, tt.want)
			}
		})
	}
}

func TestDate_IsSameMonth(t *testing.T) {
	tests := []struct {
		name string
		a    Date
		b    Date
		want bool
	}{
		{"Same", NewDate(2021, 10, 10), NewDate(2021, 10, 10), true},
		{"Day_Before", NewDate(2021, 10, 9), NewDate(2021, 10, 10), true},
		{"Day_After", NewDate(2021, 10, 11), NewDate(2021, 10, 10), true},
		{"Month_Before", NewDate(2021, 10, 10), NewDate(2021, 11, 10), false},
		{"Month_After", NewDate(2021, 10, 10), NewDate(2021, 9, 10), false},
		{"Year_Before", NewDate(2021, 10, 10), NewDate(2022, 10, 10), false},
		{"Year_After", NewDate(2021, 10, 10), NewDate(2020, 10, 10), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.a.IsSameMonth(tt.b); got != tt.want {
				t.Errorf("%v IsSameMonth(%v): got %v, want %v", tt.a, tt.b, got, tt.want)
			}
		})
	}
}

func TestNewDate(t *testing.T) {
	tests := []struct {
		name  string
		year  int
		month int
		day   int
		want  Date
	}{
		{"Normal", 2021, 10, 19, Date{2021, 10, 19}},
		{"Underflow Day", 2021, 10, 0, Date{2021, 9, 30}},
		{"Overflow Day", 2021, 9, 31, Date{2021, 10, 1}},
		{"Overflow Month", 2021, 14, 1, Date{2022, 2, 1}},
		{"Overflow Day & Month", 2021, 14, 40, Date{2022, 3, 12}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewDate(tt.year, tt.month, tt.day); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewDate(%d, %d, %d) = %v, want %v", tt.year, tt.month, tt.day, got, tt.want)
			}
		})
	}
}

func TestParseDate(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    *Date
		wantErr bool
	}{
		{"Normal", "2021-10-01", &Date{2021, 10, 1}, false},
		{"Extra", "2023-02-07 18:11:10 PST", nil, true},
		{"Short", "2021-10-1", nil, true},
		{"Overflow", "2021-09-31", nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseDate(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseDate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseDate() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDate_MarshalJSON(t *testing.T) {
	tests := []struct {
		name    string
		d       Date
		want    []byte
		wantErr bool
	}{
		{"Normal", NewDate(2010, 12, 23), []byte(`"2010-12-23"`), false},
		{"Digits", NewDate(2011, 1, 2), []byte(`"2011-01-02"`), false},
		{"Zero", Date{}, []byte(`"0000-00-00"`), false},
		{"Overflow", NewDate(10000, 1, 2), nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.d.MarshalJSON()
			if (err != nil) != tt.wantErr {
				t.Errorf("MarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MarshalJSON() got = %s, want %s", got, tt.want)
			}
		})
	}
}

func TestDate_MarshalText(t *testing.T) {
	tests := []struct {
		name    string
		d       Date
		want    []byte
		wantErr bool
	}{
		{"Normal", NewDate(2010, 12, 23), []byte(`2010-12-23`), false},
		{"Digits", NewDate(2011, 1, 2), []byte(`2011-01-02`), false},
		{"Zero", Date{}, []byte(`0000-00-00`), false},
		{"Overflow", NewDate(10000, 1, 2), nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotText, err := tt.d.MarshalText()
			if (err != nil) != tt.wantErr {
				t.Errorf("MarshalText() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotText, tt.want) {
				t.Errorf("MarshalText() got = %s, want %s", gotText, tt.want)
			}
		})
	}
}

func TestDate_UnmarshalJSON(t *testing.T) {
	tests := []struct {
		name    string
		want    *Date
		data    []byte
		wantErr bool
	}{
		{"Normal", &Date{2010, 12, 23}, []byte(`"2010-12-23"`), false},
		{"Digits", &Date{2011, 1, 2}, []byte(`"2011-01-02"`), false},
		{"Zero", nil, []byte(`0000-00-00`), true},
		{"Overflow", nil, []byte(`2011-01-32`), true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got Date
			if err := got.UnmarshalJSON(tt.data); (err != nil) != tt.wantErr {
				t.Errorf("UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.want == nil {
				return
			}
			if !reflect.DeepEqual(got, *tt.want) {
				t.Errorf("UnmarshalJSON() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDate_UnmarshalText(t *testing.T) {
	tests := []struct {
		name    string
		want    *Date
		data    []byte
		wantErr bool
	}{
		{"Normal", &Date{2010, 12, 23}, []byte(`2010-12-23`), false},
		{"Digits", &Date{2011, 1, 2}, []byte(`2011-01-02`), false},
		{"Zero", nil, []byte(`0000-00-00`), true},
		{"Overflow", nil, []byte(`2011-01-32`), true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got Date
			if err := got.UnmarshalText(tt.data); (err != nil) != tt.wantErr {
				t.Errorf("UnmarshalText() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.want == nil {
				return
			}
			if !reflect.DeepEqual(got, *tt.want) {
				t.Errorf("UnmarshalText() got = %v, want %v", got, tt.want)
			}
		})
	}
}

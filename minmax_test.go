package amoy

import (
	"testing"
)

func TestMinMax(t *testing.T) {
	if i := uint(0); MinUint != i {
		t.Errorf("MinUint() = %v, want %v", MinUint, i)
	}
	if i := uint(18446744073709551615); MaxUint != i {
		t.Errorf("MaxUint() = %v, want %v", MaxUint, i)
	}
	if i := uint32(0); MinUint32 != i {
		t.Errorf("MinUint32() = %v, want %v", MinUint32, i)
	}
	if i := uint32(4294967295); MaxUint32 != i {
		t.Errorf("MaxUint32() = %v, want %v", MaxUint32, i)
	}
	if i := uint64(0); MinUint64 != i {
		t.Errorf("MinUint64() = %v, want %v", MinUint64, i)
	}
	if i := uint64(18446744073709551615); MaxUint64 != i {
		t.Errorf("MaxUint64() = %v, want %v", MaxUint64, i)
	}

	if i := -9223372036854775808; MinInt != i {
		t.Errorf("MinInt() = %v, want %v", MinInt, i)
	}
	if i := 9223372036854775807; MaxInt != i {
		t.Errorf("MaxInt() = %v, want %v", MaxInt, i)
	}
	if i := int32(-2147483648); MinInt32 != i {
		t.Errorf("MinInt32() = %v, want %v", MinInt32, i)
	}
	if i := int32(2147483647); MaxInt32 != i {
		t.Errorf("MaxInt32() = %v, want %v", MaxInt32, i)
	}
	if i := int64(-9223372036854775808); MinInt64 != i {
		t.Errorf("MinInt64() = %v, want %v", MinInt64, i)
	}
	if i := int64(9223372036854775807); MaxInt64 != i {
		t.Errorf("MaxInt64() = %v, want %v", MaxInt64, i)
	}

	t.Logf("MinUint32 = %v = %#x", MinUint32, MinUint32)
	t.Logf("MaxUint32 = %v = %#x", MaxUint32, MaxUint32)
	t.Logf("MinUint64 = %v = %#x", MinUint64, MinUint64)
	t.Logf("MaxUint64 = %v = %#x", MaxUint64, MaxUint64)
	t.Logf("MinUint = %v = %#x", MinUint, MinUint)
	t.Logf("MaxUint = %v = %#x", MaxUint, MaxUint)

	t.Logf("MinInt32 = %v = %#x", MinInt32, MinInt32)
	t.Logf("MaxInt32 = %v = %#x", MaxInt32, MaxInt32)
	t.Logf("MinInt64 = %v = %#x", MinInt64, MinInt64)
	t.Logf("MaxInt64 = %v = %#x", MaxInt64, MaxInt64)
	t.Logf("MinInt = %v = %#x", MinInt, MinInt)
	t.Logf("MaxInt = %v = %#x", MaxInt, MaxInt)
}

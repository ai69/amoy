package amoy

import "math"

const (
	// MinUint the smallest possible value of uint.
	MinUint = uint(0)
	// MaxUint the largest possible value of uint.
	MaxUint = ^uint(0)

	// MinUint32 the smallest possible value of uint32.
	MinUint32 = uint32(0)
	// MaxUint32 the largest possible value of uint32.
	MaxUint32 = ^uint32(0)

	// MinUint64 the smallest possible value of uint64.
	MinUint64 = uint64(0)
	// MaxUint64 the largest possible value of uint64.
	MaxUint64 = ^uint64(0)

	// MinInt the smallest possible value of int.
	MinInt = -MaxInt - 1
	// MaxInt the largest possible value of int.
	MaxInt = int(^uint(0) >> 1)

	// MinInt32 the smallest possible value of int.
	MinInt32 = -MaxInt32 - 1
	// MaxInt32 the largest possible value of int.
	MaxInt32 = int32(^uint32(0) >> 1)

	// MinInt64 the smallest possible value of int.
	MinInt64 = -MaxInt64 - 1
	// MaxInt64 the largest possible value of int.
	MaxInt64 = int64(^uint64(0) >> 1)
)

var (
	// PositiveInfinity indicates the positive infinity value.
	PositiveInfinity = math.Inf(1)
	// NegativeInfinity indicates the negative infinity value.
	NegativeInfinity = math.Inf(-1)
)

package amoy

// MergeMaps returns a new map as union set of given maps.
func MergeMaps(ms ...map[string]string) map[string]string {
	res := make(map[string]string)
	for _, m := range ms {
		for k, v := range m {
			res[k] = v
		}
	}
	return res
}

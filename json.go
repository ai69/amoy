package amoy

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
)

var (
	emptyJSON = "{}"
)

// PrintJSON outputs data in JSON with indent to console.
func PrintJSON(data interface{}) {
	fmt.Println(convertJSON(data, 2))
}

// PrintOneLineJSON outputs data in JSON in one line.
func PrintOneLineJSON(data interface{}) {
	fmt.Println(convertJSON(data, 0))
}

// ToJSON returns JSON string of data with indent.
func ToJSON(data interface{}) string {
	return convertJSON(data, 2)
}

// ToOneLineJSON returns JSON string of data in one line.
func ToOneLineJSON(data interface{}) string {
	return convertJSON(data, 0)
}

func convertJSON(data interface{}, indent int) string {
	var bf bytes.Buffer
	enc := json.NewEncoder(&bf)
	if indent > 0 {
		enc.SetIndent("", strings.Repeat(" ", indent))
	}
	enc.SetEscapeHTML(false)

	var res string
	if err := enc.Encode(data); err != nil {
		res = emptyJSON
	} else {
		res = strings.TrimSpace(bf.String())
	}
	return res
}

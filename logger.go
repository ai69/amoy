package amoy

import (
	"os"
	"strings"

	"github.com/1set/gut/yos"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	lj "gopkg.in/natefinch/lumberjack.v2"
)

// Logger is a wrapper of uber/zap logger with dynamic log level.
type Logger struct {
	logL     *zap.Logger
	logS     *zap.SugaredLogger
	minLevel *zap.AtomicLevel
}

// LogConfig stands for config of logging.
type LogConfig struct {
	ConsoleFormat string
	FileFormat    string
	Monochrome    bool
	MaxFileSizeMB int
	MaxBackups    int
	CompressFile  bool
}

var (
	getEncoderConfig = func(lvlEnc zapcore.LevelEncoder) *zapcore.EncoderConfig {
		return &zapcore.EncoderConfig{
			TimeKey:        "time",
			LevelKey:       "level",
			NameKey:        "logger",
			CallerKey:      "caller",
			MessageKey:     "msg",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    lvlEnc,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeDuration: zapcore.StringDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		}
	}
	// check if the level is greater than or equal to error
	allLevelEnabler = zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return true
	})
	// log encoders
	consoleColorEncoder = zapcore.NewConsoleEncoder(*getEncoderConfig(zapcore.CapitalColorLevelEncoder))
	consoleMonoEncoder  = zapcore.NewConsoleEncoder(*getEncoderConfig(zapcore.CapitalLevelEncoder))
	jsonEncoder         = zapcore.NewJSONEncoder(*getEncoderConfig(zapcore.LowercaseLevelEncoder))
	// std log writers
	writeStdout = zapcore.AddSync(os.Stdout)
	writeStderr = zapcore.AddSync(os.Stderr)
)

// NewLogger returns a Logger with given log path and debug mode.
func NewLogger(fileName string, debug bool, cfgs ...LogConfig) *Logger {
	// log enablers
	minLevel := zap.NewAtomicLevelAt(zap.DebugLevel)
	customLevelEnabler := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= minLevel.Level()
	})
	normalLevelEnabler := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl < zapcore.ErrorLevel && lvl >= minLevel.Level()
	})
	errorLevelEnabler := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= zapcore.ErrorLevel && lvl >= minLevel.Level()
	})

	cfg := LogConfig{
		ConsoleFormat: "console",
		FileFormat:    "json",
		Monochrome:    false,
		MaxFileSizeMB: 100,
		CompressFile:  true,
	}
	if len(cfgs) > 0 {
		cfg = cfgs[0]
	}

	// log level encoder
	consoleEncoder := consoleColorEncoder
	if yos.IsOnWindows() || cfg.Monochrome {
		consoleEncoder = consoleMonoEncoder
	}

	// combine core for logger
	var cores []zapcore.Core
	switch strings.ToLower(cfg.ConsoleFormat) {
	case "console":
		cores = []zapcore.Core{
			zapcore.NewCore(consoleEncoder, writeStdout, normalLevelEnabler),
			zapcore.NewCore(consoleEncoder, writeStderr, errorLevelEnabler),
		}
	case "json":
		fallthrough
	default:
		cores = []zapcore.Core{
			zapcore.NewCore(jsonEncoder, writeStdout, normalLevelEnabler),
			zapcore.NewCore(jsonEncoder, writeStderr, errorLevelEnabler),
		}
	}

	// set log file path as per parameters
	if logPath := strings.TrimSpace(fileName); len(logPath) > 0 {
		logFile := zapcore.AddSync(&lj.Logger{
			Filename: logPath,
			MaxSize:  cfg.MaxFileSizeMB,
			Compress: cfg.CompressFile,
		})

		switch strings.ToLower(cfg.FileFormat) {
		case "console":
			cores = append(cores, zapcore.NewCore(consoleEncoder, logFile, customLevelEnabler))
		case "json":
			fallthrough
		default:
			cores = append(cores, zapcore.NewCore(jsonEncoder, logFile, customLevelEnabler))
		}
	}

	// combine option for logger
	options := []zap.Option{
		zap.AddCaller(),
		zap.AddStacktrace(zap.ErrorLevel),
	}
	// set debug mode as per parameters
	if debug {
		options = append(options, zap.Development())
	}

	// build the logger
	logL := zap.New(zapcore.NewTee(cores...), options...)
	return &Logger{
		logL:     logL,
		logS:     logL.Sugar(),
		minLevel: &minLevel,
	}
}

// Logger returns a zap logger inside the wrapper.
func (l *Logger) Logger() *zap.Logger {
	return l.logL
}

// LoggerSugared returns a sugared zap logger inside the wrapper.
func (l *Logger) LoggerSugared() *zap.SugaredLogger {
	return l.logS
}

// SetLogLevel sets the log level of loggers inside the wrapper.
func (l *Logger) SetLogLevel(level string) {
	if err := l.minLevel.UnmarshalText([]byte(level)); err != nil {
		l.logL.DPanic("fail to set log level", zap.Error(err))
	}
}

// GetLogLevel returns the log level of loggers inside the wrapper.
func (l *Logger) GetLogLevel() zapcore.Level {
	return l.minLevel.Level()
}

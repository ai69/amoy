package amoy

// EmojiDone returns an emoji char representing the given completion state.
func EmojiDone(done bool) string {
	if done {
		return `☑️`
	}
	return `⏳`
}

// EmojiBool returns an emoji char representing the given boolean value.
func EmojiBool(yes bool) string {
	if yes {
		return "✅"
	}
	return "❌"
}

// CharBool returns a Unicode char representing the given boolean value.
func CharBool(yes bool) string {
	if yes {
		return "✔"
	}
	return "✘"
}

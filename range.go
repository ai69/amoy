package amoy

// EnsureRange returns the value if it's between min and max, otherwise it returns min or max, whichever is closer. It panics if min is greater than max.
func EnsureRange(value, min, max int) int {
	if min > max {
		panic("min cannot be greater than max")
	}
	if value < min {
		return min
	}
	if value > max {
		return max
	}
	return value
}

// UpdateMinMaxFloat64 updates the min and max values if the value is less than the current min or greater than the current max.
//
// The function takes three arguments:
//
// * value: The value to compare against the current min and max.
// * min: A pointer to the current min value.
// * max: A pointer to the current max value.
func UpdateMinMaxFloat64(value float64, min, max *float64) {
	if value < *min {
		*min = value
	}
	if value > *max {
		*max = value
	}
}

// UpdateMinMaxInt updates the min and max values if the value is less than the current min or greater than the current max.
//
// The function takes three arguments:
//
// * value: The value to compare against the current min and max.
// * min: A pointer to the current min value.
// * max: A pointer to the current max value.
func UpdateMinMaxInt(value int, min, max *int) {
	if value < *min {
		*min = value
	}
	if value > *max {
		*max = value
	}
}

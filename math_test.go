package amoy

import (
	"fmt"
	"math"
	"testing"
)

func TestFractionCeil(t *testing.T) {
	tests := []struct {
		name string
		a    int
		b    int
		want int
	}{
		// {"divided by zero", 2, 0, MinInt},
		{"1/1", 1, 1, 1},
		{"2/2", 2, 2, 1},
		{"2/1", 2, 1, 2},
		{"1/2", 1, 2, 1},
		{"-1/2", -1, 2, 0},
		{"1/1000000000", 1, 1000000000, 1},
		{"999999999/1000000000", 999999999, 1000000000, 1},
		{"1000000001/1000000000", 1000000001, 1000000000, 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FractionCeil(tt.a, tt.b); got != tt.want {
				t.Errorf("FractionCeil(%d, %d) = %v, want %v", tt.a, tt.b, got, tt.want)
			}
		})
	}
}

func TestFractionFloor(t *testing.T) {
	tests := []struct {
		name string
		a    int
		b    int
		want int
	}{
		// {"divided by zero", 2, 0, MinInt},
		{"1/1", 1, 1, 1},
		{"2/2", 2, 2, 1},
		{"2/1", 2, 1, 2},
		{"1/2", 1, 2, 0},
		{"-1/2", -1, 2, -1},
		{"1/1000000000", 1, 1000000000, 0},
		{"999999999/1000000000", 999999999, 1000000000, 0},
		{"1000000001/1000000000", 1000000001, 1000000000, 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FractionFloor(tt.a, tt.b); got != tt.want {
				t.Errorf("FractionFloor(%d, %d) = %v, want %v", tt.a, tt.b, got, tt.want)
			}
		})
	}
}

func TestFractionTrunc(t *testing.T) {
	tests := []struct {
		name string
		a    int
		b    int
		want int
	}{
		// {"divided by zero", 2, 0, MinInt},
		{"1/1", 1, 1, 1},
		{"2/2", 2, 2, 1},
		{"2/1", 2, 1, 2},
		{"1/2", 1, 2, 0},
		{"-1/2", -1, 2, 0},
		{"1/1000000000", 1, 1000000000, 0},
		{"999999999/1000000000", 999999999, 1000000000, 0},
		{"1000000001/1000000000", 1000000001, 1000000000, 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FractionTrunc(tt.a, tt.b); got != tt.want {
				t.Errorf("FractionTrunc(%d, %d) = %v, want %v", tt.a, tt.b, got, tt.want)
			}
		})
	}
}

func TestFractionRound(t *testing.T) {
	tests := []struct {
		name string
		a    int
		b    int
		want int
	}{
		// {"divided by zero", 2, 0, MinInt},
		{"1/1", 1, 1, 1},
		{"2/2", 2, 2, 1},
		{"2/1", 2, 1, 2},
		{"1/2", 1, 2, 1},
		{"-1/2", -1, 2, -1},
		{"1/1000000000", 1, 1000000000, 0},
		{"999999999/1000000000", 999999999, 1000000000, 1},
		{"1000000001/1000000000", 1000000001, 1000000000, 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FractionRound(tt.a, tt.b); got != tt.want {
				t.Errorf("FractionRound(%d, %d) = %v, want %v", tt.a, tt.b, got, tt.want)
			}
		})
	}
}

func TestFractionFractional(t *testing.T) {
	tests := []struct {
		name string
		a    int
		b    int
		want float64
	}{
		{"divided by zero", 2, 0, math.NaN()},
		{"1/1", 1, 1, 0},
		{"2/2", 2, 2, 0},
		{"2/1", 2, 1, 0},
		{"1/2", 1, 2, 0.5},
		{"-1/2", -1, 2, -0.5},
		{"1/1000000000", 1, 1000000000, 1e-9},
		{"999999999/1000000000", 999999999, 1000000000, 0.999999999},
		{"1000000001/1000000000", 1000000001, 1000000000, 1e-9},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FractionFractional(tt.a, tt.b); math.Abs(got-tt.want) > 1e-10 {
				t.Errorf("FractionFractional(%d, %d) = %v, want %v", tt.a, tt.b, got, tt.want)
			}
		})
	}
}

func TestFractional(t *testing.T) {
	tests := []struct {
		name string
		f    float64
		want float64
	}{
		{"zero", 0, 0},
		{"one", 1, 0},
		{"dot one", 2.1, 0.1},
		{"pi", math.Pi, math.Pi - 3},
		{"e", math.E, math.E - 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Fractional(tt.f); math.Abs(got-tt.want) > 1e-10 {
				t.Errorf("Fractional(%v) = %v, want %v", tt.f, got, tt.want)
			}
		})
	}
}

func TestCountDigit(t *testing.T) {
	tests := []struct {
		num  int
		want int
	}{
		{0, 1},
		{1, 1},
		{-1, 1},
		{9, 1},
		{10, 2},
		{99, 2},
		{100, 3},
		{-100, 3},
		{100000000000, 12},
		{5392592365935746, 16},
	}
	for _, tt := range tests {
		name := fmt.Sprintf("Count(%d)", tt.num)
		t.Run(name, func(t *testing.T) {
			if got := CountDigit(tt.num); got != tt.want {
				t.Errorf("CountDigit(%v) = %v, want %v", tt.num, got, tt.want)
			}
		})
	}
}

func TestRoundToFixed(t *testing.T) {
	tests := []struct {
		num       float64
		precision int
		want      float64
	}{
		{1.23456789, -1, 1.23456789},
		{1.23456789, 0, 1},
		{1.23456789, 1, 1.2},
		{1.23456789, 2, 1.23},
		{1.23456789, 3, 1.235},
		{1.23456789, 4, 1.2346},
		{1.23456789, 5, 1.23457},
		{1.23456789, 6, 1.234568},
		{1.23456789, 7, 1.2345679},
		{1.23456789, 8, 1.23456789},
		{1.23456789, 12, 1.23456789},
		{1.23456789, 13, 1.23456789},
		{0.01, 1, 0},
		{0.01, 2, 0.01},
	}
	for _, tt := range tests {
		name := fmt.Sprintf("(%f,%d)", tt.num, tt.precision)
		t.Run(name, func(t *testing.T) {
			if got := RoundToFixed(tt.num, tt.precision); math.Abs(got-tt.want) > 1e-10 {
				t.Errorf("RoundToFixed(%f, %d) = %v, want %v", tt.num, tt.precision, got, tt.want)
			}
		})
	}
}

func TestPercentageStr(t *testing.T) {
	tests := []struct {
		name string
		a    int
		b    int
		want string
	}{
		{"Zero numerator", 0, 100, "0%"},
		{"Zero denominator", 100, 0, "0%"},
		{"Both zero", 0, 0, "0%"},
		{"Both one", 1, 1, "100%"},
		{"1/2", 1, 2, "50%"},
		{"1/3", 1, 3, "33.33%"},
		{"1/4", 1, 4, "25%"},
		{"123867/4678912", 123867, 4678912, "2.64%"},
		{"1/100000", 1, 100000, "0%"},
		{"1/10000", 1, 10000, "0.01%"},
		{"9999/10000", 9999, 10000, "99.99%"},
		{"10/1", 10, 1, "1000%"},
		{"-1/4", -1, 4, "-25%"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PercentageStr(tt.a, tt.b); got != tt.want {
				t.Errorf("PercentageStr(%d, %d) = %v, want %v", tt.a, tt.b, got, tt.want)
			}
		})
	}
}

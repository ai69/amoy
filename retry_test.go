package amoy

import (
	"errors"
	"testing"
	"time"
)

type mockFuncWrapper struct {
	cnt int
	num int
	err error
}

func newMockFuncWrapper(num int, err error) *mockFuncWrapper {
	return &mockFuncWrapper{
		cnt: 0,
		num: num,
		err: err,
	}
}

func (m *mockFuncWrapper) GetFunc() func() error {
	return func() error {
		m.cnt++
		if m.num == m.cnt {
			return nil
		}
		return m.err
	}
}

func (m *mockFuncWrapper) Count() int {
	return m.cnt
}

func TestBackOffRetry(t *testing.T) {
	myError := errors.New("one error")
	tests := []struct {
		name      string
		workWrap  *mockFuncWrapper
		times     uint
		delay     time.Duration
		wantTimes int
		wantErr   bool
	}{
		{"One success", newMockFuncWrapper(1, myError), 3, 10 * time.Millisecond, 1, false},
		{"Two success", newMockFuncWrapper(2, myError), 3, 10 * time.Millisecond, 2, false},
		{"Three success", newMockFuncWrapper(3, myError), 3, 10 * time.Millisecond, 3, false},
		{"Not success", newMockFuncWrapper(4, myError), 3, 10 * time.Millisecond, 3, true},
		{"Never success", newMockFuncWrapper(0, myError), 3, 10 * time.Millisecond, 3, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := BackOffRetry(tt.workWrap.GetFunc(), tt.times, tt.delay); (err != nil) != tt.wantErr {
				t.Errorf("BackOffRetry() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.workWrap.Count() != tt.wantTimes {
				t.Errorf("BackOffRetry() run times = %v, wantTimes %v", tt.workWrap.Count(), tt.wantTimes)
			}
		})
	}
}

func TestBackOffRetryIf(t *testing.T) {
	myError := errors.New("one error")
	tests := []struct {
		name      string
		workWrap  *mockFuncWrapper
		retryIf   RetryIfFunc
		times     uint
		delay     time.Duration
		wantTimes int
		wantErr   bool
	}{
		{"One success", newMockFuncWrapper(1, myError), alwaysTrueFunc, 3, 10 * time.Millisecond, 1, false},
		{"Two success", newMockFuncWrapper(2, myError), alwaysTrueFunc, 3, 10 * time.Millisecond, 2, false},
		{"Three success", newMockFuncWrapper(3, myError), alwaysTrueFunc, 3, 10 * time.Millisecond, 3, false},
		{"Not success", newMockFuncWrapper(4, myError), alwaysTrueFunc, 3, 10 * time.Millisecond, 3, true},
		{"Never success", newMockFuncWrapper(0, myError), alwaysTrueFunc, 3, 10 * time.Millisecond, 3, true},
		{"Just quit", newMockFuncWrapper(3, myError), alwaysFalseFunc, 1, 10 * time.Millisecond, 1, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := BackOffRetryIf(tt.workWrap.GetFunc(), tt.retryIf, tt.times, tt.delay); (err != nil) != tt.wantErr {
				t.Errorf("BackOffRetryIf() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.workWrap.Count() != tt.wantTimes {
				t.Errorf("BackOffRetryIf() run times = %v, wantTimes %v", tt.workWrap.Count(), tt.wantTimes)
			}
		})
	}
}

func TestFixedRetry(t *testing.T) {
	myError := errors.New("one error")
	tests := []struct {
		name      string
		workWrap  *mockFuncWrapper
		times     uint
		delay     time.Duration
		wantTimes int
		wantErr   bool
	}{
		{"One success", newMockFuncWrapper(1, myError), 3, 10 * time.Millisecond, 1, false},
		{"Two success", newMockFuncWrapper(2, myError), 3, 10 * time.Millisecond, 2, false},
		{"Three success", newMockFuncWrapper(3, myError), 3, 10 * time.Millisecond, 3, false},
		{"Not success", newMockFuncWrapper(4, myError), 3, 10 * time.Millisecond, 3, true},
		{"Never success", newMockFuncWrapper(0, myError), 3, 10 * time.Millisecond, 3, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := FixedRetry(tt.workWrap.GetFunc(), tt.times, tt.delay); (err != nil) != tt.wantErr {
				t.Errorf("FixedRetry() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.workWrap.Count() != tt.wantTimes {
				t.Errorf("FixedRetry() run times = %v, wantTimes %v", tt.workWrap.Count(), tt.wantTimes)
			}
		})
	}
}

func TestFixedRetryIf(t *testing.T) {
	myError := errors.New("one error")
	tests := []struct {
		name      string
		workWrap  *mockFuncWrapper
		retryIf   RetryIfFunc
		times     uint
		delay     time.Duration
		wantTimes int
		wantErr   bool
	}{
		{"One success", newMockFuncWrapper(1, myError), alwaysTrueFunc, 3, 10 * time.Millisecond, 1, false},
		{"Two success", newMockFuncWrapper(2, myError), alwaysTrueFunc, 3, 10 * time.Millisecond, 2, false},
		{"Three success", newMockFuncWrapper(3, myError), alwaysTrueFunc, 3, 10 * time.Millisecond, 3, false},
		{"Not success", newMockFuncWrapper(4, myError), alwaysTrueFunc, 3, 10 * time.Millisecond, 3, true},
		{"Never success", newMockFuncWrapper(0, myError), alwaysTrueFunc, 3, 10 * time.Millisecond, 3, true},
		{"Just quit", newMockFuncWrapper(3, myError), alwaysFalseFunc, 1, 10 * time.Millisecond, 1, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := FixedRetryIf(tt.workWrap.GetFunc(), tt.retryIf, tt.times, tt.delay); (err != nil) != tt.wantErr {
				t.Errorf("FixedRetryIf() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.workWrap.Count() != tt.wantTimes {
				t.Errorf("FixedRetryIf() run times = %v, wantTimes %v", tt.workWrap.Count(), tt.wantTimes)
			}
		})
	}
}

func TestSimpleRetry(t *testing.T) {
	myError := errors.New("one error")
	tests := []struct {
		name      string
		workWrap  *mockFuncWrapper
		wantTimes int
		wantErr   bool
	}{
		{"One success", newMockFuncWrapper(1, myError), 1, false},
		{"Two success", newMockFuncWrapper(2, myError), 2, false},
		{"Three success", newMockFuncWrapper(3, myError), 3, false},
		{"Not success", newMockFuncWrapper(4, myError), 3, true},
		{"Never success", newMockFuncWrapper(0, myError), 3, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := SimpleRetry(tt.workWrap.GetFunc()); (err != nil) != tt.wantErr {
				t.Errorf("SimpleRetry() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.workWrap.Count() != tt.wantTimes {
				t.Errorf("SimpleRetry() run times = %v, wantTimes %v", tt.workWrap.Count(), tt.wantTimes)
			}
		})
	}
}

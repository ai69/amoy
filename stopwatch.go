package amoy

import (
	"bufio"
	"fmt"
	"os"
	"time"

	"github.com/1set/gut/yos"
)

// Stopwatch provides a set of methods to measure elapsed time.
type Stopwatch struct {
	start time.Time
}

// NewStopwatch returns a Stopwatch instance which just starts to measure.
func NewStopwatch() *Stopwatch {
	return &Stopwatch{start: time.Now()}
}

// Elapsed returns the total elapsed time measured by the current instance.
func (sw *Stopwatch) Elapsed() time.Duration {
	return time.Since(sw.start)
}

// ElapsedMilliseconds returns the total elapsed time measured by the current instance, in milliseconds.
func (sw *Stopwatch) ElapsedMilliseconds() int64 {
	return time.Since(sw.start).Milliseconds()
}

// Show outputs elapsed time measured by the current instance to console.
func (sw *Stopwatch) Show() {
	var (
		elapsed = sw.Elapsed()
		title   = "⏱️ Time Cost"
	)
	if !yos.IsOnWindows() {
		title = StyleDate(title)
	}
	f := bufio.NewWriter(os.Stdout)
	defer f.Flush()
	fmt.Fprintf(f, "%s: %v\n", title, elapsed)
}

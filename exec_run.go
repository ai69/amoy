package amoy

import (
	"bytes"
	"errors"
	"os/exec"
	"strings"

	sp "bitbucket.org/creachadair/shell"
	"github.com/1set/gut/yos"
)

var (
	errInvalidCommand  = errors.New("invalid command")
	errMissingProcInfo = errors.New("missing process info")
)

// RunQuietCommand runs the command quietly and returns no standard output nor standard error.
func RunQuietCommand(command string) error {
	cmd, err := parseSingleRawCommand(command)
	if err != nil {
		return err
	}
	return cmd.Run()
}

// RunCombinedCommand runs the command and returns its combined standard output and standard error.
func RunCombinedCommand(command string) ([]byte, error) {
	if cmd, err := parseSingleRawCommand(command); err != nil {
		return nil, err
	} else if out, err := cmd.CombinedOutput(); err != nil {
		return nil, err
	} else {
		return out, nil
	}
}

// RunSimpleCommand simply runs the command and returns its standard output and standard error.
func RunSimpleCommand(command string) ([]byte, []byte, error) {
	cmd, err := parseSingleRawCommand(command)
	if err != nil {
		return nil, nil, err
	}

	var (
		bufOut bytes.Buffer
		bufErr bytes.Buffer
	)
	cmd.Stdout = &bufOut
	cmd.Stderr = &bufErr
	err = cmd.Run()
	return bufOut.Bytes(), bufErr.Bytes(), err
}

/*
RunPipeCommand(cmd)
RunCommand(cmd, envs, timeout)
*/

func parseSingleRawCommand(raw string) (*exec.Cmd, error) {
	// HACK: workaround for escape issue of creachadair/shell on Windows
	if yos.IsOnWindows() {
		raw = strings.ReplaceAll(raw, `\`, `\\`)
	}

	if parts, ok := sp.Split(raw); !ok {
		return nil, errInvalidCommand
	} else if _, err := exec.LookPath(parts[0]); err != nil {
		return nil, err
	} else {
		return exec.Command(parts[0], parts[1:]...), nil
	}
}

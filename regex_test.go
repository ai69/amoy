package amoy

import (
	"reflect"
	"regexp"
	"testing"
)

func TestExtractNamedValues(t *testing.T) {
	type args struct {
		r   *regexp.Regexp
		str string
	}
	tests := []struct {
		name string
		args args
		want NamedValues
	}{
		{"nil regex", args{nil, "Hello"}, map[string]string{}},
		{"empty regex", args{regexp.MustCompile(""), "Hello"}, map[string]string{}},
		{"not match", args{regexp.MustCompile(`(?P<url>https?://[/.\-\w]+)/(?P<key>[\w]{10,})/?`), "Hello"}, map[string]string{}},
		{"match empty", args{regexp.MustCompile(`(?P<val>[^/]*)/(?P<key>[\w]*)/?`), `//`}, map[string]string{"val": EmptyStr, "key": EmptyStr}},
		{"match", args{regexp.MustCompile(`(?P<val>[^/]*)/(?P<key>[\w]*)/?`), `123/hello/`}, map[string]string{"val": "123", "key": "hello"}},
		{"match unnamed", args{regexp.MustCompile(`([^/]*)/([\w]*)/?`), `123/hello/`}, map[string]string{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ExtractNamedValues(tt.args.r, tt.args.str); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExtractNamedValues() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRegexMatch(t *testing.T) {
	tests := []struct {
		name    string
		pat     string
		s       string
		want    bool
		wantErr bool
	}{
		{"invalid pattern", "[[", "hello", false, true},
		{"match pattern", "(?i)^hello", "Hello World", true, false},
		{"unmatch pattern", "(?i)^hello", "Halo World", false, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := RegexMatch(tt.pat, tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("RegexMatch() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("RegexMatch() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkRegexMatch(b *testing.B) {
	var (
		p1 = "(?i)^hello"
		s1 = "Hello World"
		p2 = "(?i)^hello[^|&%]+W(.*?)"
		s2 = "Hello_World"
		p3 = "zln##@%v$%^%#h234"
		s3 = "Hello World!"
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = RegexMatch(p1, s1)
		_, _ = RegexMatch(p2, s2)
		_, _ = RegexMatch(p3, s3)
	}
}

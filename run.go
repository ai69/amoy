package amoy

import (
	"context"
	"errors"
	"time"
)

var (
	appStart = time.Now()

	// ErrWaitTimeout indicates timeout after waiting.
	ErrWaitTimeout = errors.New("amoy: wait timeout")
)

// AppUpTime returns time elapsed since the app started.
func AppUpTime() time.Duration {
	return time.Since(appStart)
}

// AppStartTime returns time when the app started.
func AppStartTime() time.Time {
	return appStart
}

// DelayRun waits for the duration to elapse and then calls task function, if it's not cancelled by the handler returns first.
// It's like time.AfterFunc but returns context.CancelFunc instead of time.Timer.
func DelayRun(interval time.Duration, task func()) context.CancelFunc {
	ctx, cancel := context.WithCancel(context.Background())
	go func(ctx context.Context) {
		t := time.NewTimer(interval)
		defer t.Stop()
		select {
		case <-t.C:
			task()
		case <-ctx.Done():
		}
	}(ctx)
	return cancel
}

// RunWaitTimeout calls the task function, and waits for the result for the given max timeout.
func RunWaitTimeout(timeout time.Duration, task func() error) error {
	errCh := make(chan error)
	go func() {
		errCh <- task()
	}()

	t := time.NewTimer(timeout)
	defer t.Stop()
	select {
	case <-t.C:
		return ErrWaitTimeout
	case err := <-errCh:
		return err
	}
}

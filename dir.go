package amoy

import (
	"github.com/1set/gut/yos"
	homedir "github.com/mitchellh/go-homedir"
)

// GetHomeDir returns the home directory of the current user, or the nested directory inside home directory if passed in as arguments.
func GetHomeDir(dirs ...string) (string, error) {
	base, err := homedir.Dir()
	if err != nil {
		return EmptyStr, err
	}
	if len(dirs) == 0 {
		return base, nil
	}
	return yos.JoinPath(base, yos.JoinPath(dirs...)), nil
}

// CreateHomeDir creates the nested directory inside home directory if passed in as arguments.
func CreateHomeDir(dirs ...string) (string, error) {
	path, err := GetHomeDir(dirs...)
	if err != nil {
		return EmptyStr, err
	}
	if err = yos.MakeDir(path); err != nil {
		return EmptyStr, err
	}
	return path, nil
}

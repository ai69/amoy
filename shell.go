package amoy

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"runtime"
	"strings"
)

// ChangeDir changes the current working directory.
func ChangeDir(path string) {
	if err := os.Chdir(path); err != nil {
		log.Fatalf("fail to change dir: %v", err)
	}
}

// CurrentFunctionName returns name of current function.
func CurrentFunctionName() string {
	pc, _, _, _ := runtime.Caller(1)
	return runtime.FuncForPC(pc).Name()
}

// FunctionName returns name of the given function.
func FunctionName(f interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name()
}

// ShortFunctionName returns short name of the given function.
func ShortFunctionName(f interface{}) string {
	name := FunctionName(f)
	if p := strings.LastIndex(name, "."); p >= 0 && len(name) > p {
		return name[p+1:]
	}
	return name
}

// GetEnvVar returns the value of the given environment variable or the fallback.
func GetEnvVar(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// IsInterfaceNil returns true if the given interface is nil.
func IsInterfaceNil(i interface{}) bool {
	if i == nil {
		return true
	}
	defer func() { recover() }()
	switch reflect.TypeOf(i).Kind() {
	case reflect.Ptr, reflect.UnsafePointer, reflect.Interface, reflect.Slice, reflect.Map, reflect.Chan, reflect.Func:
		return reflect.ValueOf(i).IsNil()
	}
	return false
}

var (
	// ErrNoPipeData is returned when no data was read from the stdin pipe.
	ErrNoPipeData = errors.New("amoy: no pipe data")
)

// ReadStdinPipe reads stdin data via pipe and returns the data.
func ReadStdinPipe() ([]byte, error) {
	fi, err := os.Stdin.Stat()
	if err != nil {
		return nil, fmt.Errorf(`amoy: %w`, err)
	}
	if fi.Mode()&os.ModeNamedPipe != 0 {
		b, err := ioutil.ReadAll(os.Stdin)
		if err != nil {
			return b, fmt.Errorf(`amoy: %w`, err)
		}
		return b, nil
	}
	return nil, ErrNoPipeData
}

package amoy

import (
	"encoding/gob"
	"encoding/json"
	"io/ioutil"
	"os"
)

// SaveJSONFile saves object as a JSON file.
func SaveJSONFile(data interface{}, fileName string) error {
	var (
		bytes []byte
		err   error
	)
	if !IsInterfaceNil(data) {
		if bytes, err = json.MarshalIndent(data, "", "  "); err != nil {
			return err
		}
	}
	return ioutil.WriteFile(fileName, bytes, 0644)
}

// LoadJSONFile loads object from the given JSON file.
func LoadJSONFile(data interface{}, fileName string) error {
	var (
		bytes []byte
		err   error
	)
	if bytes, err = ioutil.ReadFile(fileName); err != nil {
		return err
	}
	return json.Unmarshal(bytes, data)
}

// SaveGobFile saves object as a gob file.
func SaveGobFile(data interface{}, fileName string) error {
	// open for writing
	f, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer f.Close()
	// save to gob file
	enc := gob.NewEncoder(f)
	return enc.Encode(data)
}

// LoadGobFile loads object from the given gob file.
func LoadGobFile(data interface{}, fileName string) error {
	// open for reading
	f, err := os.Open(fileName)
	if err != nil {
		return err
	}
	defer f.Close()
	// load from gob file
	dec := gob.NewDecoder(f)
	return dec.Decode(data)
}

package amoy

import (
	"testing"
	"unsafe"
)

func TestCurrentFunctionName(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{"Repeat1", "bitbucket.org/ai69/amoy.TestCurrentFunctionName.func1"},
		{"Repeat2", "bitbucket.org/ai69/amoy.TestCurrentFunctionName.func1"},
		{"Repeat3", "bitbucket.org/ai69/amoy.TestCurrentFunctionName.func1"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CurrentFunctionName(); got != tt.want {
				t.Errorf("CurrentFunctionName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func myFunc1() {}
func myFunc2() {}
func myFunc3() {}

func TestFunctionName(t *testing.T) {
	tests := []struct {
		name string
		f    interface{}
		want string
	}{
		{"Func1", myFunc1, "bitbucket.org/ai69/amoy.myFunc1"},
		{"Func2", myFunc2, "bitbucket.org/ai69/amoy.myFunc2"},
		{"Func3", myFunc3, "bitbucket.org/ai69/amoy.myFunc3"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FunctionName(tt.f); got != tt.want {
				t.Errorf("FunctionName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestShortFunctionName(t *testing.T) {
	tests := []struct {
		name string
		f    interface{}
		want string
	}{
		{"Func1", myFunc1, "myFunc1"},
		{"Func2", myFunc2, "myFunc2"},
		{"Func3", myFunc3, "myFunc3"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ShortFunctionName(tt.f); got != tt.want {
				t.Errorf("ShortFunctionName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsInterfaceNil(t *testing.T) {
	type customStruct struct {
		Number int
	}
	type customInterface interface{}
	var (
		sliceNil    []int
		mapNil      map[string]int
		strNil      *string
		structEmpty customStruct
		structNil   *customStruct
		chanNil     chan int
		funcNil     func(string) error
	)
	tests := []struct {
		name string
		i    interface{}
		want bool
	}{
		{"Nil", nil, true},
		{"Int 0", 0, false},
		{"Int 1", 1, false},
		{"String Empty", "", false},
		{"String Has", "Hello", false},
		{"Slice Nil", sliceNil, true},
		{"Slice Empty", []int{}, false},
		{"Slice Has", []int{1, 2, 3}, false},
		{"Map Nil", mapNil, true},
		{"Map Empty", map[string]int{}, false},
		{"Map Has", map[string]int{"a": 1, "b": 2}, false},
		{"Ptr String Nil", strNil, true},
		{"Ptr Struct Nil", structNil, true},
		{"Ptr Struct Empty", &structEmpty, false},
		{"Struct Empty", structEmpty, false},
		{"Unsafe Pointer Nil", unsafe.Pointer(nil), true},
		{"Unsafe Pointer Struct Nil", unsafe.Pointer(structNil), true},
		{"Unsafe Pointer Struct Empty", unsafe.Pointer(&structEmpty), false},
		{"Custom Interface Nil", customInterface(nil), true},
		{"Custom Interface Struct Nil", customInterface(structNil), true},
		{"Custom Interface Struct Empty", customInterface(&structEmpty), false},
		{"Chan Nil", chanNil, true},
		{"Chan Empty", make(chan int), false},
		{"Chan Has", make(chan int, 1), false},
		{"Func Nil", funcNil, true},
		{"Func Has", func(string) error { return nil }, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsInterfaceNil(tt.i); got != tt.want {
				t.Errorf("IsInterfaceNil() = %v, want %v", got, tt.want)
			}
		})
	}
}

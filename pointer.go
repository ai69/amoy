package amoy

import "time"

// BoolPtr returns a pointer to the bool value passed in.
func BoolPtr(b bool) *bool {
	return &b
}

// BytePtr returns a pointer to the byte value passed in.
func BytePtr(b byte) *byte {
	return &b
}

// RunePtr returns a pointer to the rune value passed in.
func RunePtr(r rune) *rune {
	return &r
}

// IntPtr returns a pointer to the int value passed in.
func IntPtr(i int) *int {
	return &i
}

// Int8Ptr returns a pointer to the int8 value passed in.
func Int8Ptr(i int8) *int8 {
	return &i
}

// Int16Ptr returns a pointer to the int16 value passed in.
func Int16Ptr(i int16) *int16 {
	return &i
}

// Int32Ptr returns a pointer to the int32 value passed in.
func Int32Ptr(i int32) *int32 {
	return &i
}

// Int64Ptr returns a pointer to the int64 value passed in.
func Int64Ptr(i int64) *int64 {
	return &i
}

// UintPtr returns a pointer to the uint value passed in.
func UintPtr(u uint) *uint {
	return &u
}

// Uint8Ptr returns a pointer to the uint8 value passed in.
func Uint8Ptr(u uint8) *uint8 {
	return &u
}

// Uint16Ptr returns a pointer to the uint16 value passed in.
func Uint16Ptr(u uint16) *uint16 {
	return &u
}

// Uint32Ptr returns a pointer to the uint32 value passed in.
func Uint32Ptr(u uint32) *uint32 {
	return &u
}

// Uint64Ptr returns a pointer to the uint64 value passed in.
func Uint64Ptr(u uint64) *uint64 {
	return &u
}

// UintptrPtr returns a pointer to the uintptr value passed in.
func UintptrPtr(u uintptr) *uintptr {
	return &u
}

// Float32Ptr returns a pointer to the float32 value passed in.
func Float32Ptr(f float32) *float32 {
	return &f
}

// Float64Ptr returns a pointer to the float64 value passed in.
func Float64Ptr(f float64) *float64 {
	return &f
}

// Complex64Ptr returns a pointer to the complex64 value passed in.
func Complex64Ptr(c complex64) *complex64 {
	return &c
}

// Complex128Ptr returns a pointer to the complex128 value passed in.
func Complex128Ptr(c complex128) *complex128 {
	return &c
}

// StringPtr returns a pointer to the string value passed in.
func StringPtr(s string) *string {
	return &s
}

// ErrorPtr returns a pointer to the error value passed in.
func ErrorPtr(e error) *error {
	return &e
}

// DurationPtr returns a pointer to the time.Duration value passed in.
func DurationPtr(d time.Duration) *time.Duration {
	return &d
}

// TimePtr returns a pointer to the time.Time value passed in.
func TimePtr(t time.Time) *time.Time {
	return &t
}

// InterfacePtr returns a pointer to the interface value passed in.
func InterfacePtr(i interface{}) *interface{} {
	return &i
}

// StringSlicePtr returns a pointer to the []string value passed in.
func StringSlicePtr(s []string) *[]string {
	return &s
}

// IntSlicePtr returns a pointer to the []int value passed in.
func IntSlicePtr(s []int) *[]int {
	return &s
}

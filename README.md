# amoy

[![go.dev reference](https://img.shields.io/badge/go.dev-reference-007d9c?logo=go&logoColor=white&style=flat-square)](https://pkg.go.dev/bitbucket.org/ai69/amoy)

Just another collection of Go utilities among yet another packages.

> Telling a programmer there's already a library to do X is like telling a songwriter there's already a song about love.

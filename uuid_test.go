package amoy

import (
	"testing"
)

func TestGetUUID(t *testing.T) {
	tests := []func() string{
		GetUUID,
		GetUB32,
		GetSID,
		GetCDKey,
	}
	for _, tt := range tests {
		t.Run(ShortFunctionName(tt), func(t *testing.T) {
			if got := tt(); got != EmptyStr {
				t.Logf("got %q of %d", got, len(got))
			} else {
				t.Errorf("%v() got unexpected empty %v", FunctionName(t), got)
			}
		})
	}
}

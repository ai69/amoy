package amoy

import "testing"

func TestEnsureRange(t *testing.T) {
	tests := []struct {
		name  string
		value int
		min   int
		max   int
		want  int
	}{
		{"min", -1, 0, 1, 0},
		{"max", 2, 0, 1, 1},
		{"normal", 1, 0, 2, 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EnsureRange(tt.value, tt.min, tt.max); got != tt.want {
				t.Errorf("EnsureRange() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUpdateMinMaxFloat64(t *testing.T) {
	tests := []struct {
		name    string
		value   float64
		min     float64
		max     float64
		wantMin float64
		wantMax float64
	}{
		{"all same", 1.0, 1.0, 1.0, 1.0, 1.0},
		{"equal to min", 1.0, 1.0, 2.0, 1.0, 2.0},
		{"equal to max", 2.0, 1.0, 2.0, 1.0, 2.0},
		{"middle", 1.5, 1.0, 2.0, 1.0, 2.0},
		{"smaller", 0.5, 1.0, 2.0, 0.5, 2.0},
		{"larger", 2.5, 1.0, 2.0, 1.0, 2.5},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			UpdateMinMaxFloat64(tt.value, &tt.min, &tt.max)
			if tt.min != tt.wantMin || tt.max != tt.wantMax {
				t.Errorf("UpdateMinMaxFloat64() = (%v, %v), want (%v, %v)", tt.min, tt.max, tt.wantMin, tt.wantMax)
			}
		})
	}
}

func TestUpdateMinMaxInt(t *testing.T) {
	tests := []struct {
		name    string
		value   int
		min     int
		max     int
		wantMin int
		wantMax int
	}{
		{"all same", 1, 1, 1, 1, 1},
		{"equal to min", 1, 1, 2, 1, 2},
		{"equal to max", 2, 1, 2, 1, 2},
		{"middle", 2, 1, 3, 1, 3},
		{"smaller", 0, 1, 2, 0, 2},
		{"larger", 3, 1, 2, 1, 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			UpdateMinMaxInt(tt.value, &tt.min, &tt.max)
			if tt.min != tt.wantMin || tt.max != tt.wantMax {
				t.Errorf("UpdateMinMaxInt() = (%v, %v), want (%v, %v)", tt.min, tt.max, tt.wantMin, tt.wantMax)
			}
		})
	}
}

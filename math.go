package amoy

import (
	"math"
)

// FractionCeil returns the least integer value greater than or equal to a/b.
func FractionCeil(a, b int) int {
	return int(math.Ceil(float64(a) / float64(b)))
}

// FractionFloor returns the greatest integer value less than or equal to a/b.
func FractionFloor(a, b int) int {
	return int(math.Floor(float64(a) / float64(b)))
}

// FractionFractional returns the fractional part of floating-point number represented by a/b.
func FractionFractional(a, b int) float64 {
	_, f := math.Modf(float64(a) / float64(b))
	return f
}

// FractionTrunc returns the integer value of a/b.
func FractionTrunc(a, b int) int {
	return int(math.Trunc(float64(a) / float64(b)))
}

// FractionRound returns the nearest integer, rounding half away from zero of a/b.
func FractionRound(a, b int) int {
	return int(math.Round(float64(a) / float64(b)))
}

// Fractional returns the fractional part of floating-point number f.
func Fractional(f float64) float64 {
	_, f = math.Modf(f)
	return f
}

// CountDigit returns the number of digits of a number.
func CountDigit(num int) int {
	if num == 0 {
		return 1
	}
	return int(math.Floor(math.Log10(math.Abs(float64(num))) + 1))
}

// PercentageStr returns the percentage of a/b as a string.
func PercentageStr(a, b int) string {
	var s string
	if b == 0 || a == 0 {
		s = "0"
	} else if a == b {
		s = "100"
	} else {
		s = FtoaWithDigits(float64(a)/float64(b)*100, 2)
	}
	return s + "%"
}

// RoundToFixed returns the rounding floating number with given precision.
func RoundToFixed(num float64, precision int) float64 {
	if precision < 0 || precision > 12 {
		return num
	}
	mul := math.Pow(10, float64(precision))
	return math.Round(num*mul) / mul
}

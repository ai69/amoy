package amoy

import (
	"reflect"
	"testing"
)

func TestParseIntSequence(t *testing.T) {
	type args struct {
		s        string
		splitSep string
		rangeSep string
	}
	tests := []struct {
		name    string
		args    args
		want    []int
		wantErr bool
	}{
		{name: "Empty", args: args{s: "", splitSep: ",", rangeSep: "-"}, want: nil, wantErr: true},
		{name: "Separator1", args: args{s: ",", splitSep: ",", rangeSep: "-"}, want: nil, wantErr: true},
		{name: "Separator1", args: args{s: "-", splitSep: ",", rangeSep: "-"}, want: nil, wantErr: true},
		{name: "No Separator1", args: args{s: "42", splitSep: "", rangeSep: "-"}, want: nil, wantErr: true},
		{name: "No Separator2", args: args{s: "42", splitSep: ",", rangeSep: ""}, want: nil, wantErr: true},
		{name: "Same Separators", args: args{s: "42", splitSep: "-", rangeSep: "-"}, want: nil, wantErr: true},
		{name: "Too Many Separators", args: args{s: "1-2-3", splitSep: "-", rangeSep: "-"}, want: nil, wantErr: true},
		{name: "Reversed Range", args: args{s: "5-3", splitSep: ",", rangeSep: "-"}, want: nil, wantErr: true},
		{name: "Invalid Number1", args: args{s: "1>5,9,10", splitSep: ",", rangeSep: "-"}, want: nil, wantErr: true},
		{name: "Invalid Number2", args: args{s: "1-5,9,a", splitSep: ",", rangeSep: "-"}, want: nil, wantErr: true},
		{name: "Invalid Number3", args: args{s: "a-5,9,10", splitSep: ",", rangeSep: "-"}, want: nil, wantErr: true},
		{name: "Invalid Number4", args: args{s: "1-b,9,10", splitSep: ",", rangeSep: "-"}, want: nil, wantErr: true},
		{name: "Only Parts", args: args{s: "1,2,3", splitSep: ",", rangeSep: "-"}, want: []int{1, 2, 3}, wantErr: false},
		{name: "Only Ranges", args: args{s: "1-5", splitSep: ",", rangeSep: "-"}, want: []int{1, 2, 3, 4, 5}, wantErr: false},
		{name: "Same Range", args: args{s: "5-5", splitSep: ",", rangeSep: "-"}, want: []int{5}, wantErr: false},
		{name: "Hybrid", args: args{s: "1-5,6,9", splitSep: ",", rangeSep: "-"}, want: []int{1, 2, 3, 4, 5, 6, 9}, wantErr: false},
		{name: "Order", args: args{s: "5,1-3,7", splitSep: ",", rangeSep: "-"}, want: []int{1, 2, 3, 5, 7}, wantErr: false},
		{name: "Overlap", args: args{s: "5,1-6,8,2-4", splitSep: ",", rangeSep: "-"}, want: []int{1, 2, 3, 4, 5, 6, 8}, wantErr: false},
		{name: "Large Numbers", args: args{s: "99-101,1024", splitSep: ",", rangeSep: "-"}, want: []int{99, 100, 101, 1024}, wantErr: false},
		{name: "Negative Numbers", args: args{s: "-99~-95,1024", splitSep: ",", rangeSep: "~"}, want: []int{-99, -98, -97, -96, -95, 1024}, wantErr: false},
		{name: "Large Sequence", args: args{s: "1-100", splitSep: ",", rangeSep: "-"}, want: []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100}, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseIntSequence(tt.args.s, tt.args.splitSep, tt.args.rangeSep)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseIntSequence() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseIntSequence() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDistinctInts(t *testing.T) {
	tests := []struct {
		name string
		ori  []int
		want []int
	}{
		{"Nil", nil, []int{}},
		{"Empty", []int{}, []int{}},
		{"One", []int{42}, []int{42}},
		{"Two1", []int{10, 100}, []int{10, 100}},
		{"Two2", []int{100, 10}, []int{100, 10}},
		{"Same", []int{100, 100}, []int{100}},
		{"Unique", []int{53, 11, 26}, []int{53, 11, 26}},
		{"Normal", []int{7, 0, 0, 1, 2, 1, 0, 3, 5, 1, 4, 4, 4, 0, 3}, []int{7, 0, 1, 2, 3, 5, 4}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DistinctInts(tt.ori); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DistinctInts(%v) = %v, want %v", tt.ori, got, tt.want)
			}
		})
	}
}

func TestMergeIntSequence(t *testing.T) {
	tests := []struct {
		name     string
		num      []int
		splitSep string
		rangeSep string
		want     string
	}{
		{"None", []int{}, ",", "-", ""},
		{"Single One", []int{1}, ",", "-", "1"},
		{"Duplicate Zero", []int{0, 0, 0, 0, 0, 0}, ",", "-", "0"},
		{"Duplicate One", []int{2, 2}, ",", "-", "2"},
		{"More Duplicate One", []int{3, 3, 3}, ",", "-", "3"},
		{"Separate Two", []int{1, 3}, ",", "-", "1,3"},
		{"Unordered & Duplicate Two", []int{5, 3, 5, 3, 5, 5}, ",", "-", "3,5"},
		{"Nearby Two", []int{4, 5, 4, 5}, ",", "-", "4-5"},
		{"Simple Sequence", []int{1, 2, 3, 4, 5}, ",", "-", "1-5"},
		{"Two Sequences", []int{1, 2, 3, 4, 5, 8, 9, 10}, ",", "-", "1-5,8-10"},
		{"Unordered Sequences", []int{5, 1, 2, 9, 10, 1, 1, 3, 4, 2, 5, 8, 9}, ",", "-", "1-5,8-10"},
		{"Mixed Sequences", []int{5, 1, 2, 9, 10, 1, 3, 4, 2, 5, 8, 9, 100, 102, 101, 80}, ",", "-", "1-5,8-10,80,100-102"},
		{"Large Sequence", []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100}, ",", "-", "1-100"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MergeIntSequence(tt.num, tt.splitSep, tt.rangeSep); got != tt.want {
				t.Errorf("MergeIntSequence(%v, %s, %s) = %q, want %q", tt.num, tt.splitSep, tt.rangeSep, got, tt.want)
			}
		})
	}
}

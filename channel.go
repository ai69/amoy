package amoy

import "errors"

// BlockForever blocks current goroutine forever.
func BlockForever() {
	<-make(chan struct{})
}

// AccumulatedChannel returns a channel that accumulates values from given channel.
func AccumulatedChannel(inCh <-chan []byte, size int) <-chan []byte {
	if size <= 0 {
		panic(errors.New("invalid slice size"))
	}

	outCh := make(chan []byte, 1024)
	go func(in <-chan []byte, out chan []byte) {
		// close output channel on exit
		defer close(out)

		var (
			buffer = make([]byte, size, size)
			pos    = 0
		)
		for {
			select {
			case data, ok := <-in:
				// once the input channel is closed, we should dump left and exit
				if !ok {
					if pos > 0 {
						out <- buffer[:pos]
					}
					return
				}

				for dataSize := len(data); dataSize > 0; dataSize = len(data) {
					// calculate rest space in buffer
					remainSize := size - pos

					// if data is not bigger than rest, append to buffer
					if dataSize <= remainSize {
						copy(buffer[pos:pos+dataSize], data)
						data = nil
						pos += dataSize
					} else {
						// if data is bigger than rest, append first part to buffer, and keep the rest in data
						copy(buffer[pos:size], data[:remainSize])
						data = data[remainSize:]
						pos += remainSize
					}

					// if buffer is full, send to output channel, and reset buffer
					if pos == size {
						out <- buffer
						buffer = make([]byte, size, size)
						pos = 0
					}
				}
			}
		}
	}(inCh, outCh)
	return outCh
}

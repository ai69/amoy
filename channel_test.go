package amoy

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/1set/gut/yhash"
	"github.com/1set/gut/yrand"
)

func BenchmarkAccumulatedChannel(b *testing.B) {
	var (
		dataSize  = 2048
		bufSize   = 500
		chunkSize = 128
	)
	rawStr, _ := yrand.StringBase62(dataSize)
	rawBytes := []byte(rawStr)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		inCh := make(chan []byte)
		outCh := AccumulatedChannel(inCh, bufSize)
		done := make(chan struct{})
		go func() {
			for range outCh {
				//revive:disable:empty-block
				// yes, it's empty
			}
			close(done)
		}()
		fillChannelRandomChunk(inCh, rawBytes, chunkSize, chunkSize)
		close(inCh)
		<-done
	}
}

func TestAccumulatedChannel(t *testing.T) {
	tests := []struct {
		dataSize  int
		batchSize int
		minSize   int
		maxSize   int
	}{
		{0, 1, 1, 1},
		{1, 1, 1, 1},
		{1024, 1, 1, 1},
		{1024, 10, 2, 2},
		{1024, 10, 2, 5},
		{1024, 1024, 2, 5},
		{1024, 1025, 2, 5},
		{1024, 10, 1024, 1024},
		{1024, 1024, 1024, 1024},
		{1024, 10, 2048, 2048},
	}
	for _, tt := range tests {
		name := fmt.Sprintf("%d->(%d,%d-%d)", tt.dataSize, tt.batchSize, tt.minSize, tt.maxSize)
		t.Run(name, func(t *testing.T) {
			var (
				rawBytes []byte
				cumBytes []byte
			)
			rawStr, _ := yrand.StringBase62(tt.dataSize)
			if len(rawStr) > 0 {
				rawBytes = []byte(rawStr)
			}

			inCh := make(chan []byte)
			outCh := AccumulatedChannel(inCh, tt.batchSize)
			done := make(chan struct{})
			go func() {
				for v := range outCh {
					cumBytes = append(cumBytes, v...)
				}
				close(done)
			}()
			fillChannelRandomChunk(inCh, rawBytes, tt.minSize, tt.maxSize)
			close(inCh)
			<-done

			if !reflect.DeepEqual(rawBytes, cumBytes) {
				rh, _ := yhash.BytesMD5(rawBytes)
				ch, _ := yhash.BytesMD5(cumBytes)
				t.Errorf("AccumulatedChannel([%d]%v) != [%d]%v breaks data", len(rawBytes), rh, len(cumBytes), ch)
			}
		})
	}
}

func TestBlockForever(t *testing.T) {
	go func() {
		BlockForever()
		t.Errorf("BlockForever() should not return")
	}()
	SleepForMilliseconds(10)
}

// fillChannelRandomChunk fills the channel with given data in random chunk sizes (min <= size < max).
func fillChannelRandomChunk(in chan []byte, data []byte, min, max int) {
	var pos int
	for {
		l := min
		if min < max {
			l, _ = yrand.IntRange(min, max)
		}
		if l == 0 {
			in <- []byte{}
			continue
		}
		if pos+l > len(data) {
			l = len(data) - pos
		}
		in <- data[pos : pos+l]
		pos += l
		if pos >= len(data) {
			break
		}
	}
}

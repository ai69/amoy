package amoy

import (
	"fmt"
	"strings"
	"testing"

	"github.com/1set/gut/yrand"
)

func BenchmarkReplaceString_Single_CaseSensitive_Literal(b *testing.B) {
	s := "aloha Hello hello word WORLD World 123456"
	replace := map[string]string{"hello": "Goodbye"}
	opt := ReplaceStringOptions{replace, false, false}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = ReplaceString(s, opt)
	}
}

func BenchmarkReplaceString_Multiple_CaseSensitive_Literal(b *testing.B) {
	s := "aloha Hello hello word WORLD World 123456"
	replace := map[string]string{"hello": "Goodbye", "world": "Earth"}
	opt := ReplaceStringOptions{replace, false, false}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = ReplaceString(s, opt)
	}
}

func BenchmarkReplaceString_Single_CaseInsensitive_Literal(b *testing.B) {
	s := "aloha Hello hello word WORLD World 123456"
	replace := map[string]string{"hello": "Goodbye"}
	opt := ReplaceStringOptions{replace, true, false}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = ReplaceString(s, opt)
	}
}

func BenchmarkReplaceString_Multiple_CaseInsensitive_Literal(b *testing.B) {
	s := "aloha Hello hello word WORLD World 123456"
	replace := map[string]string{"hello": "Goodbye", "world": "Earth"}
	opt := ReplaceStringOptions{replace, true, false}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = ReplaceString(s, opt)
	}
}

func BenchmarkReplaceString_Single_CaseInsensitive_Imitation(b *testing.B) {
	s := "aloha Hello hello word WORLD World 123456"
	replace := map[string]string{"hello": "Goodbye"}
	opt := ReplaceStringOptions{replace, true, true}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = ReplaceString(s, opt)
	}
}

func BenchmarkReplaceString_Multiple_CaseInsensitive_Imitation(b *testing.B) {
	s := "aloha Hello hello word WORLD World 123456"
	replace := map[string]string{"hello": "Goodbye", "world": "Earth"}
	opt := ReplaceStringOptions{replace, true, true}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = ReplaceString(s, opt)
	}
}

func TestReplaceString(t *testing.T) {
	optsNilReplace := ReplaceStringOptions{}
	optsEmptyReplace := ReplaceStringOptions{
		Replacements:    map[string]string{},
		CaseInsensitive: false,
		ImitateResult:   false,
	}
	optsInvalidReplace := ReplaceStringOptions{
		Replacements:    map[string]string{"": "123"},
		CaseInsensitive: false,
		ImitateResult:   false,
	}
	optsMixedReplace := ReplaceStringOptions{
		Replacements:    map[string]string{"": "456", "hello": "goodbye"},
		CaseInsensitive: false,
		ImitateResult:   false,
	}
	optsSingleReplace := ReplaceStringOptions{
		Replacements:    map[string]string{"hello": "goodbye"},
		CaseInsensitive: false,
		ImitateResult:   false,
	}
	s0 := "Hello hello word WORLD"
	replace0 := map[string]string{"hello": "Goodbye"}
	s1 := "ABCDEFGHIJKLMNOPQRSTUVWXYZSTUVWXABD"
	s2 := "abcdefghijklmnopqrstuvwxyzstuvwxabd"
	replace1 := map[string]string{
		"ABC":    "123",
		"STUVWX": "888ABCGH",
		"GH":     "6789",
		"STUVW":  "999",
	}
	replace2 := map[string]string{
		"ABC":    "11",
		"ABCDEF": "22222",
		"DEF":    "3",
		"GH":     "6789",
	}
	replace3 := map[string]string{
		`standard`: `{{ .AppName }}`,
		`bitbucket.org/ai69/keiki/kgengo/standard`: `{{ .ModulePath }}`,
		`Copyright (c) 2022 Hyori`:                 `Copyright (c) {{ .Now.UTC.Year }} {{ .Author }}`,
	}
	replace4 := map[string]string{
		"":               "888",
		"abc":            "123",
		"hello-world":    "good-bye",
		"github.com":     "gitlab.com",
		"github.com/abc": "bitbucket.org/def",
	}
	tests := []struct {
		name string
		s    string
		opt  ReplaceStringOptions
		want string
	}{
		{"Default Options", "aloha", optsNilReplace, "aloha"},
		{"Empty Replace", "aloha", optsEmptyReplace, "aloha"},
		{"Empty Src", "", optsSingleReplace, ""},
		{"Invalid Replace", "aloha", optsInvalidReplace, "aloha"},
		{"Single Replace", "hello world", optsSingleReplace, "goodbye world"},
		{"Mixed Replace", "hello world", optsMixedReplace, "goodbye world"},
		{"No Match", s1, optsSingleReplace, s1},
		{"Single Case-sensitive and Imitation", s0, ReplaceStringOptions{replace0, false, true}, "Hello goodbye word WORLD"},
		{"Single Case-insensitive and Literal", s0, ReplaceStringOptions{replace0, true, false}, "Goodbye Goodbye word WORLD"},
		{"Multiple Case-sensitive and Literal - Replace 1", s1, ReplaceStringOptions{replace1, false, false}, "123DEF6789IJKLMNOPQR888ABCGHYZ888ABCGHABD"},
		{"Multiple Case-sensitive and Literal - Replace 2", s1, ReplaceStringOptions{replace2, false, false}, "222226789IJKLMNOPQRSTUVWXYZSTUVWXABD"},
		{"Multiple Case-sensitive and Imitation - Miss 1", s2, ReplaceStringOptions{replace1, false, true}, s2},
		{"Multiple Case-sensitive and Imitation - Miss 2", s2, ReplaceStringOptions{replace2, false, true}, s2},
		{"Multiple Case-insensitive and Literal - Replace 1", s2, ReplaceStringOptions{replace1, true, false}, "123def6789ijklmnopqr888ABCGHyz888ABCGHabd"},
		{"Multiple Case-insensitive and Literal - Replace 2", s2, ReplaceStringOptions{replace2, true, false}, "222226789ijklmnopqrstuvwxyzstuvwxabd"},
		{"Multiple Case-insensitive and Imitation - Replace 1", s2, ReplaceStringOptions{replace1, true, true}, "123def6789ijklmnopqr888abcghyz888abcghabd"},
		{"Multiple Case-insensitive and Imitation - Replace 2", s2, ReplaceStringOptions{replace2, true, true}, "222226789ijklmnopqrstuvwxyzstuvwxabd"},
		{"Real Case 1", "# standard\n\n```bash\ngo install bitbucket.org/ai69/keiki/kgengo/standard\n```\n", ReplaceStringOptions{replace3, true, true}, "# {{ .appname }}\n\n```bash\ngo install {{ .modulepath }}\n```\n"},
		{"Real Case 2", "https://gitlab.com/ab3C/repo\n", ReplaceStringOptions{replace4, true, true}, "https://gitlab.com/ab3C/repo\n"},
		{"Real Case 3", "https://github.com/abc/repo\n", ReplaceStringOptions{replace4, true, true}, "https://bitbucket.org/def/repo\n"},
		{"Real Case 4", "https://github.com/Hello-World", ReplaceStringOptions{replace4, true, true}, "https://gitlab.com/Good-Bye"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ReplaceString(tt.s, tt.opt); got != tt.want {
				t.Errorf("ReplaceString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_replaceMultipleString(t *testing.T) {
	rps1 := []*replacePair{
		&replacePair{"STUVWX", "888ABCGH"},
		&replacePair{"STUVW", "999"},
		&replacePair{"ABC", "123"},
		&replacePair{"GH", "6789"},
	}
	rps2 := []*replacePair{
		&replacePair{"ABC", "11"},
		&replacePair{"ABCDEF", "22222"},
		&replacePair{"DEF", "3"},
		&replacePair{"GH", "6789"},
	}
	rps3 := []*replacePair{
		&replacePair{"ABCDEF", "22222"},
		&replacePair{"ABC", "11"},
		&replacePair{"DEF", "3"},
		&replacePair{"GH", "6789"},
	}
	tests := []struct {
		name       string
		s          string
		rps        []*replacePair
		ignoreCase bool
		imitateOld bool
		want       string
	}{
		{"Nil Pairs", "", nil, false, false, ""},
		{"Empty Pairs", "", []*replacePair{}, false, false, ""},
		{"Empty Src", "", []*replacePair{&replacePair{"abc", "123"}}, false, false, ""},
		{"Empty Old", "Aloha", []*replacePair{&replacePair{"", "123"}}, false, false, "Aloha"},
		{"Empty New", "Aloha 123", []*replacePair{&replacePair{"Aloha", ""}}, false, false, " 123"},
		{"Case-sensitive and Literal - Miss", "Hello World", []*replacePair{&replacePair{"hello", "aloha"}}, false, false, "Hello World"},
		{"Case-sensitive and Literal - Hit", "Hello World", []*replacePair{&replacePair{"Hello", "aloha"}}, false, false, "aloha World"},
		{"Case-sensitive and Imitation - Miss", "Hello World", []*replacePair{&replacePair{"hello", "aloha"}}, false, true, "Hello World"},
		{"Case-sensitive and Imitation - Hit", "Hello World", []*replacePair{&replacePair{"Hello", "aloha"}}, false, true, "Aloha World"},
		{"Case-insensitive and Literal - Miss", "Hello World", []*replacePair{&replacePair{"halo", "aloha"}}, true, false, "Hello World"},
		{"Case-insensitive and Literal - Hit", "Hello World", []*replacePair{&replacePair{"hello", "aloha"}}, true, false, "aloha World"},
		{"Case-insensitive and Imitation - Miss", "Hello World", []*replacePair{&replacePair{"halo", "aloha"}}, true, true, "Hello World"},
		{"Case-insensitive and Imitation - Hit", "Hello World", []*replacePair{&replacePair{"hello", "aloha"}}, true, true, "Aloha World"},
		{"Case-insensitive and Imitation - Order 1", "ABCDEFGHIJKLMNOPQRSTUVWXYZSTUVWXABD", rps1, true, true, "123DEF6789IJKLMNOPQR888ABCGHYZ888ABCGHABD"},
		{"Case-insensitive and Imitation - Order 2", "ABCDEFGHIJKLMNOPQRSTUVWXYZSTUVWXABD", rps2, true, true, "1136789IJKLMNOPQRSTUVWXYZSTUVWXABD"},
		{"Case-insensitive and Imitation - Order 3", "ABCDEFGHIJKLMNOPQRSTUVWXYZSTUVWXABD", rps3, true, true, "222226789IJKLMNOPQRSTUVWXYZSTUVWXABD"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := replaceMultipleString(tt.s, tt.rps, tt.ignoreCase, tt.imitateOld); got != tt.want {
				t.Errorf("replaceMultipleString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_replaceMultipleString_FuzzVerify(t *testing.T) {
	const (
		targetTimes = 50
		srcSize     = 4096
		pairSize    = 10
	)
	gen := func() (string, []string, []*replacePair) {
		s, _ := yrand.StringBase62(srcSize)
		bp := make([]string, 0, pairSize*2)
		rp := make([]*replacePair, 0, pairSize)
		for i := pairSize; i > 0; i-- {
			olds, _ := yrand.StringBase62(i*2 - 1)
			news, _ := yrand.StringBase62(i*2 - 2)
			bp = append(bp, olds, news)
			rp = append(rp, &replacePair{olds, news})
		}
		return s, bp, rp
	}
	for i := 0; i < targetTimes; i++ {
		t.Run(fmt.Sprintf("Try%d", i+1), func(t *testing.T) {
			s, bp, rp := gen()
			replacer := strings.NewReplacer(bp...)
			res1 := replacer.Replace(s)
			res2 := replaceMultipleString(s, rp, false, false)
			if res1 != res2 {
				t.Errorf("replaceMultipleString(%q, %v) got = %q, want %q", s, bp, res2, res1)
			}
		})
	}
}

func Test_replaceSingleString(t *testing.T) {
	tests := []struct {
		name       string
		s          string
		rp         *replacePair
		ignoreCase bool
		imitateOld bool
		want       string
	}{
		{"Nil Pair", "aloha", nil, false, false, "aloha"},
		{"Empty Src", "", &replacePair{"abc", "123"}, false, false, ""},
		{"Empty Pair", "aloha", &replacePair{"", ""}, false, false, "aloha"},
		{"Empty Old", "aloha", &replacePair{"", "123"}, false, false, "aloha"},
		{"Empty New", "aloha 123", &replacePair{"aloha", ""}, false, false, " 123"},
		{"Case-sensitive and Literal - Miss", "Hello World", &replacePair{"hello", "aloha"}, false, false, "Hello World"},
		{"Case-sensitive and Literal - Hit", "Hello World", &replacePair{"Hello", "aloha"}, false, false, "aloha World"},
		{"Case-insensitive and Literal - Miss", "Hello World", &replacePair{"halo", "aloha"}, true, false, "Hello World"},
		{"Case-insensitive and Literal - Hit", "Hello World", &replacePair{"hello", "aloha"}, true, false, "aloha World"},
		{"Case-sensitive and Imitation - Miss", "Hello World", &replacePair{"hello", "aloha"}, false, true, "Hello World"},
		{"Case-sensitive and Imitation - Hit", "Hello World", &replacePair{"Hello", "aloha"}, false, true, "Aloha World"},
		{"Case-insensitive and Imitation - Miss", "Hello World", &replacePair{"halo", "aloha"}, true, true, "Hello World"},
		{"Case-insensitive and Imitation - Hit", "Hello World", &replacePair{"hello", "aloha"}, true, true, "Aloha World"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := replaceSingleString(tt.s, tt.rp, tt.ignoreCase, tt.imitateOld); got != tt.want {
				t.Errorf("replaceSingleString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_imitateString(t *testing.T) {
	tests := []struct {
		name string
		src  string
		dest string
		want string
	}{
		{"Empty", "", "", ""},
		{"Empty old", "", "Aloha", "Aloha"},
		{"Empty new", "Aloha", "", ""},
		{"Lower case", "aloha", "HELLO", "hello"},
		{"Upper case", "ALOHA", "hello", "HELLO"},
		{"Title case", "Aloha", "hello", "Hello"},
		{"Title words 1", "Aloha", "hello world", "Hello World"},
		{"Title words 2", "Aloha", "good-job", "Good-Job"},
		{"Title words 3", "Aloha", "loco.moco", "Loco.Moco"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := imitateString(tt.src, tt.dest); got != tt.want {
				t.Errorf("imitateString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getStringCaseType(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want stringCaseType
	}{
		{"Empty", "", stringCaseMisc},
		{"Single Lower", "a", stringCaseLower},
		{"Word Lower", "aloha", stringCaseLower},
		{"Single Upper", "A", stringCaseUpper},
		{"Word Upper", "ALOHA", stringCaseUpper},
		{"One Word Title", "Aloha", stringCaseTitle},
		{"Two Words Title", "Aloha World", stringCaseTitle},
		{"Dashed Title", "Aloha-World", stringCaseTitle},
		{"Mixed Word 1", "AlohA", stringCaseMisc},
		{"Mixed Word 2", "ALOHa", stringCaseMisc},
		{"Mixed Word 3", "alohA", stringCaseMisc},
		{"Mixed Title 1", "Aloha WORLD", stringCaseMisc},
		{"Mixed Title 2", "Aloha world", stringCaseMisc},
		{"Mixed Title 3", "aloha World", stringCaseMisc},
		{"Mixed Title 4", "ALOHA World", stringCaseMisc},
		{"Mixed Title 5", "AlOha World", stringCaseMisc},
		{"Mixed Title 6", "ALOHA world", stringCaseMisc},
		{"Non-word", "123$%^", stringCaseMisc},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getStringCaseType(tt.s); got != tt.want {
				t.Errorf("getStringCaseType(%q) = %v, want %v", tt.s, got, tt.want)
			}
		})
	}
}

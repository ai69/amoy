module bitbucket.org/ai69/amoy

go 1.16

require (
	bitbucket.org/creachadair/shell v0.0.7
	github.com/1set/gut v0.0.0-20201117175203-a82363231997
	github.com/BurntSushi/toml v1.2.1 // indirect
	github.com/avast/retry-go v3.0.0+incompatible
	github.com/google/uuid v1.3.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/muesli/termenv v0.13.0
	github.com/olekukonko/tablewriter v0.0.5
	go.uber.org/zap v1.24.0
	golang.org/x/sync v0.0.0-20220929204114-8fcdb60fdcc0
	golang.org/x/tools v0.1.12
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

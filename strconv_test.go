package amoy

import "testing"

func TestItoa(t *testing.T) {
	tests := []struct {
		name string
		i    int
		want string
	}{
		{"zero", 0, "0"},
		{"one", 1, "1"},
		{"ten", 10, "10"},
		{"1024", 1024, "1024"},
		{"min int32", -2147483648, "-2147483648"},
		{"min int64", -9223372036854775808, "-9223372036854775808"},
		{"max int32", 2147483647, "2147483647"},
		{"max int64", 9223372036854775807, "9223372036854775807"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Itoa(tt.i); got != tt.want {
				t.Errorf("Itoa() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestItoa64(t *testing.T) {
	tests := []struct {
		name string
		i    int64
		want string
	}{
		{"zero", 0, "0"},
		{"one", 1, "1"},
		{"ten", 10, "10"},
		{"1024", 1024, "1024"},
		{"min int32", -2147483648, "-2147483648"},
		{"min int64", -9223372036854775808, "-9223372036854775808"},
		{"max int32", 2147483647, "2147483647"},
		{"max int64", 9223372036854775807, "9223372036854775807"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Itoa64(tt.i); got != tt.want {
				t.Errorf("Itoa64() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestItoa32(t *testing.T) {
	tests := []struct {
		name string
		i    int32
		want string
	}{
		{"zero", 0, "0"},
		{"one", 1, "1"},
		{"ten", 10, "10"},
		{"1024", 1024, "1024"},
		{"min int32", -2147483648, "-2147483648"},
		{"max int32", 2147483647, "2147483647"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Itoa32(tt.i); got != tt.want {
				t.Errorf("Itoa32() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestItoa16(t *testing.T) {
	tests := []struct {
		name string
		i    int16
		want string
	}{
		{"zero", 0, "0"},
		{"one", 1, "1"},
		{"ten", 10, "10"},
		{"1024", 1024, "1024"},
		{"min int16", -32768, "-32768"},
		{"max int16", 32767, "32767"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Itoa16(tt.i); got != tt.want {
				t.Errorf("Itoa16() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestItoa8(t *testing.T) {
	tests := []struct {
		name string
		i    int8
		want string
	}{
		{"zero", 0, "0"},
		{"one", 1, "1"},
		{"ten", 10, "10"},
		{"102", 102, "102"},
		{"min int8", -128, "-128"},
		{"max int8", 127, "127"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Itoa8(tt.i); got != tt.want {
				t.Errorf("Itoa8() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUtoa(t *testing.T) {
	tests := []struct {
		name string
		i    uint
		want string
	}{
		{"zero", 0, "0"},
		{"one", 1, "1"},
		{"ten", 10, "10"},
		{"1024", 1024, "1024"},
		{"max uint32", 4294967295, "4294967295"},
		{"max uint64", 18446744073709551615, "18446744073709551615"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Utoa(tt.i); got != tt.want {
				t.Errorf("Utoa() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUtoa64(t *testing.T) {
	tests := []struct {
		name string
		i    uint64
		want string
	}{
		{"zero", 0, "0"},
		{"one", 1, "1"},
		{"ten", 10, "10"},
		{"1024", 1024, "1024"},
		{"max uint32", 4294967295, "4294967295"},
		{"max uint64", 18446744073709551615, "18446744073709551615"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Utoa64(tt.i); got != tt.want {
				t.Errorf("Utoa64() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUtoa32(t *testing.T) {
	tests := []struct {
		name string
		i    uint32
		want string
	}{
		{"zero", 0, "0"},
		{"one", 1, "1"},
		{"ten", 10, "10"},
		{"1024", 1024, "1024"},
		{"max uint32", 4294967295, "4294967295"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Utoa32(tt.i); got != tt.want {
				t.Errorf("Utoa32() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUtoa16(t *testing.T) {
	tests := []struct {
		name string
		u    uint16
		want string
	}{
		{"zero", 0, "0"},
		{"one", 1, "1"},
		{"max uint16", 65535, "65535"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Utoa16(tt.u); got != tt.want {
				t.Errorf("Utoa16() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUtoa8(t *testing.T) {
	tests := []struct {
		name string
		u    uint8
		want string
	}{
		{"zero", 0, "0"},
		{"one", 1, "1"},
		{"max uint8", 255, "255"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Utoa8(tt.u); got != tt.want {
				t.Errorf("Utoa8() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAtoi(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    int
		wantErr bool
	}{
		{"invalid char", "dac21c", 0, true},
		{"invalid 1,024", "1,024", 0, true},
		{"zero", "0", 0, false},
		{"one", "1", 1, false},
		{"min int32", "-2147483648", -2147483648, false},
		{"< min int32", "-2147483650", -2147483650, false},
		{"min int64", "-9223372036854775808", -9223372036854775808, false},
		{"< min int64", "-9223372036854775809", -9223372036854775808, true},
		{"max int32", "2147483647", 2147483647, false},
		{"> max int32", "2147483649", 2147483649, false},
		{"max int64", "9223372036854775807", 9223372036854775807, false},
		{"> max int64", "9223372036854775808", 9223372036854775807, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Atoi(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Atoi() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Atoi() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAtoi64(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    int64
		wantErr bool
	}{
		{"invalid char", "dac21c", 0, true},
		{"invalid 1,024", "1,024", 0, true},
		{"zero", "0", 0, false},
		{"one", "1", 1, false},
		{"min int32", "-2147483648", -2147483648, false},
		{"< min int32", "-2147483650", -2147483650, false},
		{"min int64", "-9223372036854775808", -9223372036854775808, false},
		{"< min int64", "-9223372036854775809", -9223372036854775808, true},
		{"max int32", "2147483647", 2147483647, false},
		{"> max int32", "2147483649", 2147483649, false},
		{"max int64", "9223372036854775807", 9223372036854775807, false},
		{"> max int64", "9223372036854775808", 9223372036854775807, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Atoi64(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Atoi64() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Atoi64() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAtoi32(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    int32
		wantErr bool
	}{
		{"invalid char", "dac21c", 0, true},
		{"invalid 1,024", "1,024", 0, true},
		{"zero", "0", 0, false},
		{"one", "1", 1, false},
		{"min int32", "-2147483648", -2147483648, false},
		{"< min int32", "-2147483650", 0, true},
		{"max int32", "2147483647", 2147483647, false},
		{"> max int32", "2147483649", 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Atoi32(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Atoi32() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Atoi32() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAtoi16(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    int16
		wantErr bool
	}{
		{"invalid char", "dac21c", 0, true},
		{"invalid 1,024", "1,024", 0, true},
		{"zero", "0", 0, false},
		{"one", "1", 1, false},
		{"min int16", "-32768", -32768, false},
		{"< min int16", "-32770", 0, true},
		{"max int16", "32767", 32767, false},
		{"> max int16", "32769", 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Atoi16(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Atoi16() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Atoi16() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAtoi8(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    int8
		wantErr bool
	}{
		{"invalid char", "dac21c", 0, true},
		{"invalid 1,024", "1,024", 0, true},
		{"zero", "0", 0, false},
		{"one", "1", 1, false},
		{"min int8", "-128", -128, false},
		{"< min int8", "-130", 0, true},
		{"max int8", "127", 127, false},
		{"> max int8", "129", 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Atoi8(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Atoi8() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Atoi8() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAtou(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    uint
		wantErr bool
	}{
		{"invalid char", "dac21c", 0, true},
		{"invalid 1,024", "1,024", 0, true},
		{"zero", "0", 0, false},
		{"one", "1", 1, false},
		{"max uint32", "4294967295", 4294967295, false},
		{"> max uint32", "4294967296", 4294967296, false},
		{"max uint64", "18446744073709551615", 18446744073709551615, false},
		{"> max uint64", "18446744073709551616", 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Atou(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Atou() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Atou() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAtou64(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    uint64
		wantErr bool
	}{
		{"invalid char", "dac21c", 0, true},
		{"invalid 1,024", "1,024", 0, true},
		{"zero", "0", 0, false},
		{"one", "1", 1, false},
		{"max uint32", "4294967295", 4294967295, false},
		{"> max uint32", "4294967296", 4294967296, false},
		{"max uint64", "18446744073709551615", 18446744073709551615, false},
		{"> max uint64", "18446744073709551616", 18446744073709551615, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Atou64(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Atou64() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Atou64() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAtou32(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    uint32
		wantErr bool
	}{
		{"invalid char", "dac21c", 0, true},
		{"invalid 1,024", "1,024", 0, true},
		{"zero", "0", 0, false},
		{"one", "1", 1, false},
		{"max uint32", "4294967295", 4294967295, false},
		{"> max uint32", "4294967296", 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Atou32(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Atou32() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Atou32() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAtou16(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    uint16
		wantErr bool
	}{
		{"invalid char", "dac21c", 0, true},
		{"invalid 1,024", "1,024", 0, true},
		{"zero", "0", 0, false},
		{"one", "1", 1, false},
		{"max uint16", "65535", 65535, false},
		{"> max uint16", "65536", 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Atou16(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Atou16() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Atou16() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAtou8(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    uint8
		wantErr bool
	}{
		{"invalid char", "dac21c", 0, true},
		{"invalid 1,024", "1,024", 0, true},
		{"zero", "0", 0, false},
		{"one", "1", 1, false},
		{"max uint8", "255", 255, false},
		{"> max uint8", "256", 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Atou8(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Atou8() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Atou8() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAtof64(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    float64
		wantErr bool
	}{
		{"invalid char", "dac21c", 0, true},
		{"invalid 1,024", "1,024", 0, true},
		{"zero", "0", 0, false},
		{"one", "1", 1, false},
		{"max float64", "1.7976931348623157e+308", 1.7976931348623157e+308, false},
		{"> max float64", "1.7976931348623158e+309", 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Atof64(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Atof64() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Atof64() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAtof32(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		want    float32
		wantErr bool
	}{
		{"invalid char", "dac21c", 0, true},
		{"invalid 1,024", "1,024", 0, true},
		{"zero", "0", 0, false},
		{"one", "1", 1, false},
		{"max float32", "3.4028235e+38", 3.4028235e+38, false},
		{"> max float32", "3.4028236e+38", 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Atof32(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Atof32() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Atof32() got = %v, want %v", got, tt.want)
			}
		})
	}
}

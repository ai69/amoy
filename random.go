package amoy

import (
	"errors"
	"time"

	"github.com/1set/gut/yrand"
)

// FeelLucky returns the random decision as per a given successful rate.
func FeelLucky(rate float64) bool {
	r, _ := yrand.Float64()
	return r < rate
}

// ChooseWeightStrMap selects a random string key according to weights in value, or panics for invalid weights or internal errors.
func ChooseWeightStrMap(weightMap map[string]float64) string {
	var (
		keys    []string
		weights []float64
	)
	for n, w := range weightMap {
		keys = append(keys, n)
		weights = append(weights, w)
	}
	pos, err := yrand.WeightedChoice(weights)
	if err != nil {
		panic(err)
	}
	return keys[pos]
}

var (
	errMinMaxRange = errors.New("min should be less than max")
)

// RandomInt returns a random int number in [min, max).
func RandomInt(min, max int) int {
	if min >= max {
		panic(errMinMaxRange)
	}
	n, _ := yrand.IntRange(min, max)
	return n
}

// RandomFloat returns a random float64 number in [min, max).
func RandomFloat(min, max float64) float64 {
	if min >= max {
		panic(errMinMaxRange)
	}
	f, _ := yrand.Float64()
	return (max-min)*f + min
}

// RandomString returns a random string with given length, length should be greater than 0.
func RandomString(length int) string {
	s, err := yrand.StringBase62(length)
	if err != nil {
		panic(err)
	}
	return s
}

// RandomTime returns a random time between [start, start+d) or [start+d, start) if d < 0.
func RandomTime(start time.Time, d time.Duration) time.Time {
	var delta int64
	if d > 0 {
		delta, _ = yrand.Int64Range(0, d.Nanoseconds())
	} else if d < 0 {
		delta, _ = yrand.Int64Range(d.Nanoseconds(), 0)
	} else {
		return start
	}
	return start.Add(time.Duration(delta) * time.Nanosecond)
}

// RandomTimeBetween returns a random time between [start, end).
func RandomTimeBetween(start, end time.Time) time.Time {
	delta := end.Sub(start)
	if delta <= 0 {
		return start
	}
	return RandomTime(start, delta)
}

// RandomDuration returns a random duration between [min, max).
func RandomDuration(min, max time.Duration) time.Duration {
	if max <= min {
		return max
	}
	delta, _ := yrand.Int64Range(min.Nanoseconds(), max.Nanoseconds())
	return time.Duration(delta) * time.Nanosecond
}

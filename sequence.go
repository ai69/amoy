package amoy

import (
	"fmt"
	"sort"
	"strings"
)

// ParseIntSequence parses sequences like "9-12,10,20" into slices of int like [9, 10, 11, 12, 20].
func ParseIntSequence(s, splitSep, rangeSep string) ([]int, error) {
	if splitSep == rangeSep || len(splitSep) == 0 || len(rangeSep) == 0 {
		return nil, fmt.Errorf("invalid separators")
	}
	var (
		temp       []int
		numA, numB int
		err        error
	)
	parts := strings.Split(s, splitSep)
	for _, p := range parts {
		ns := strings.Split(p, rangeSep)
		if len(ns) == 1 {
			if numA, err = Atoi(ns[0]); err != nil {
				return nil, err
			}
			temp = append(temp, numA)
		} else if len(ns) == 2 {
			if numA, err = Atoi(ns[0]); err != nil {
				return nil, err
			}
			if numB, err = Atoi(ns[1]); err != nil {
				return nil, err
			}
			if numA > numB {
				return nil, fmt.Errorf("invalid range bound: %d > %d", numA, numB)
			}
			for i := numA; i <= numB; i++ {
				temp = append(temp, i)
			}
		} else {
			return nil, fmt.Errorf("invalid range format: %s", p)
		}
	}
	result := DistinctInts(temp)
	sort.Ints(result)
	return result, nil
}

// MergeIntSequence merges int slices like [8, 8, 9, 10, 20, 12]. into sequences like "8-10,12,20".
func MergeIntSequence(num []int, splitSep, rangeSep string) string {
	if len(num) == 0 {
		return ""
	}
	sort.Ints(num)
	var (
		result    []string
		beginNum  int
		lastNum   int
		saveRange = func(bg, lt int) {
			if bg == lt {
				result = append(result, fmt.Sprintf("%d", bg))
			} else {
				result = append(result, fmt.Sprintf("%d%s%d", bg, rangeSep, lt))
			}
		}
	)
	for i, n := range num {
		if i == 0 {
			beginNum = n
		} else if n != lastNum+1 && n != lastNum {
			saveRange(beginNum, lastNum)
			beginNum = n
		}
		lastNum = n
	}
	saveRange(beginNum, lastNum)
	return strings.Join(result, splitSep)
}

// DistinctInts returns a new slice with the same order but without duplicate int elements.
func DistinctInts(ori []int) []int {
	exist := make(map[int]struct{})
	for _, n := range ori {
		exist[n] = struct{}{}
	}
	result := make([]int, 0)
	for _, n := range ori {
		if _, ok := exist[n]; ok {
			result = append(result, n)
			delete(exist, n)
		}
	}
	return result
}

package amoy

import (
	"bufio"
	"io/ioutil"
	"log"
	"os"

	gu "golang.org/x/tools/godoc/util"
)

// IsTextFile loads and checks if the given path is a text file.
func IsTextFile(path string) bool {
	buf, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalf("fail to read file: %v", err)
	}
	return gu.IsText(buf)
}

// TrimUTF8BOM removes the leading UTF-8 byte order mark from bytes.
func TrimUTF8BOM(b []byte) []byte {
	if len(b) >= 3 && b[0] == 0xef && b[1] == 0xbb && b[2] == 0xbf {
		return b[3:]
	}
	return b
}

var (
	filePerm       os.FileMode = 0644
	createFileFlag             = os.O_RDWR | os.O_CREATE | os.O_TRUNC
	appendFileFlag             = os.O_APPEND | os.O_CREATE | os.O_WRONLY
)

// ReadFileBytes reads the whole named file and returns the contents.
// It's a sugar actually, simply calls os.ReadFile like ioutil.ReadFile does since Go 1.16.
func ReadFileBytes(path string) ([]byte, error) {
	return os.ReadFile(path)
}

// ReadFileString reads the whole named file and returns the contents as a string.
func ReadFileString(path string) (string, error) {
	b, err := os.ReadFile(path)
	if err != nil {
		return EmptyStr, err
	}
	return string(b), nil
}

// WriteFileBytes writes the given data into a file.
func WriteFileBytes(path string, data []byte) error {
	return openFileWriteBytes(path, createFileFlag, data)
}

// AppendFileBytes writes the given data to the end of a file.
func AppendFileBytes(path string, data []byte) error {
	return openFileWriteBytes(path, appendFileFlag, data)
}

func openFileWriteBytes(path string, flag int, data []byte) error {
	file, err := os.OpenFile(path, flag, filePerm)
	if err != nil {
		return err
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	defer w.Flush()
	_, err = w.Write(data)
	return err
}

// WriteFileString writes the given content string into a file.
func WriteFileString(path string, content string) error {
	return openFileWriteString(path, createFileFlag, content)
}

// AppendFileString appends the given content string to the end of a file.
func AppendFileString(path string, content string) error {
	return openFileWriteString(path, appendFileFlag, content)
}

func openFileWriteString(path string, flag int, content string) error {
	file, err := os.OpenFile(path, flag, filePerm)
	if err != nil {
		return err
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	defer w.Flush()
	_, err = w.WriteString(content)
	return err
}

// IsSamePath returns true if two paths describe the same file.
func IsSamePath(p1, p2 string) (bool, error) {
	// compare string directly
	if p1 == p2 {
		return true, nil
	}
	// get file stats
	fi1, err := os.Stat(p1)
	if err != nil {
		return false, err
	}
	fi2, err := os.Stat(p2)
	if err != nil {
		return false, err
	}
	// compare with file stats
	return os.SameFile(fi1, fi2), nil
}

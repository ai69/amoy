package amoy

import (
	"context"
	"time"
)

// SleepForMilliseconds pauses the current goroutine for at least the n milliseconds.
func SleepForMilliseconds(n float64) {
	time.Sleep(Milliseconds(n))
}

// SleepForSeconds pauses the current goroutine for at least the n seconds.
func SleepForSeconds(n float64) {
	time.Sleep(Seconds(n))
}

// SleepForMinutes pauses the current goroutine for at least the n minutes.
func SleepForMinutes(n float64) {
	time.Sleep(Minutes(n))
}

// SleepForHours pauses the current goroutine for at least the n hours.
func SleepForHours(n float64) {
	time.Sleep(Hours(n))
}

// SleepForDays pauses the current goroutine for at least the n days.
func SleepForDays(n float64) {
	time.Sleep(Days(n))
}

// SleepWithContext pauses the current goroutine for the duration d or shorter duration if the context is cancelled.
// A negative or zero duration causes SleepWithContext to return immediately.
func SleepWithContext(ctx context.Context, d time.Duration) error {
	if d <= 0 {
		return nil
	}
	t := time.NewTimer(d)
	defer t.Stop()
	select {
	case <-t.C:
		return nil
	case <-ctx.Done():
		return ctx.Err()
	}
}

package amoy

import "testing"

func TestSubstrAfterFirst(t *testing.T) {
	tests := []struct {
		name string
		s    string
		sub  string
		want string
	}{
		{"all empty", EmptyStr, EmptyStr, EmptyStr},
		{"s is empty", EmptyStr, "Hello", EmptyStr},
		{"sub is empty", "Hello", EmptyStr, EmptyStr},
		{"sub is not found", "Hello", "W", EmptyStr},
		{"s and sub is the same", "Hello", "Hello", EmptyStr},
		{"s contains sub", "ABCDE", "BC", "DE"},
		{"sub contains s", "BC", "ABCDE", EmptyStr},
		{"sub is prefix", "ABCDE", "AB", "CDE"},
		{"sub is suffix", "ABCDE", "DE", ""},
		{"multiple sub", "ABCBCBCBCBCDE", "BC", "BCBCBCBCDE"},
		{"cjk", "我爱你爱你哦", "爱你", "爱你哦"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SubstrAfterFirst(tt.s, tt.sub); got != tt.want {
				t.Errorf("SubstrAfterFirst(%q, %q) = %q, want %q", tt.s, tt.sub, got, tt.want)
			}
		})
	}
}

func TestSubstrAfterLast(t *testing.T) {
	tests := []struct {
		name string
		s    string
		sub  string
		want string
	}{
		{"all empty", EmptyStr, EmptyStr, EmptyStr},
		{"s is empty", EmptyStr, "Hello", EmptyStr},
		{"sub is empty", "Hello", EmptyStr, EmptyStr},
		{"sub is not found", "Hello", "W", EmptyStr},
		{"s and sub is the same", "Hello", "Hello", EmptyStr},
		{"s contains sub", "ABCDE", "BC", "DE"},
		{"sub contains s", "BC", "ABCDE", EmptyStr},
		{"sub is prefix", "ABCDE", "AB", "CDE"},
		{"sub is suffix", "ABCDE", "DE", ""},
		{"multiple sub", "ABCBCBCBCBCDE", "BC", "DE"},
		{"cjk", "我爱你爱你哦", "爱你", "哦"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SubstrAfterLast(tt.s, tt.sub); got != tt.want {
				t.Errorf("SubstrAfterLast(%q, %q) = %q, want %q", tt.s, tt.sub, got, tt.want)
			}
		})
	}
}

func TestSubstrBeforeFirst(t *testing.T) {
	tests := []struct {
		name string
		s    string
		sub  string
		want string
	}{
		{"all empty", EmptyStr, EmptyStr, EmptyStr},
		{"s is empty", EmptyStr, "Hello", EmptyStr},
		{"sub is empty", "Hello", EmptyStr, EmptyStr},
		{"sub is not found", "Hello", "W", EmptyStr},
		{"s and sub is the same", "Hello", "Hello", EmptyStr},
		{"s contains sub", "ABCDE", "BC", "A"},
		{"sub contains s", "BC", "ABCDE", EmptyStr},
		{"sub is prefix", "ABCDE", "AB", ""},
		{"sub is suffix", "ABCDE", "DE", "ABC"},
		{"multiple sub", "ABCBCBCBCBCDE", "BC", "A"},
		{"cjk", "我爱你爱你哦", "爱你", "我"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SubstrBeforeFirst(tt.s, tt.sub); got != tt.want {
				t.Errorf("SubstrBeforeFirst(%q, %q) = %q, want %q", tt.s, tt.sub, got, tt.want)
			}
		})
	}
}

func TestSubstrBeforeLast(t *testing.T) {
	tests := []struct {
		name string
		s    string
		sub  string
		want string
	}{
		{"all empty", EmptyStr, EmptyStr, EmptyStr},
		{"s is empty", EmptyStr, "Hello", EmptyStr},
		{"sub is empty", "Hello", EmptyStr, EmptyStr},
		{"sub is not found", "Hello", "W", EmptyStr},
		{"s and sub is the same", "Hello", "Hello", EmptyStr},
		{"s contains sub", "ABCDE", "BC", "A"},
		{"sub contains s", "BC", "ABCDE", EmptyStr},
		{"sub is prefix", "ABCDE", "AB", ""},
		{"sub is suffix", "ABCDE", "DE", "ABC"},
		{"multiple sub", "ABCBCBCBCBCDE", "BC", "ABCBCBCBC"},
		{"cjk", "我爱你爱你哦", "爱你", "我爱你"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SubstrBeforeLast(tt.s, tt.sub); got != tt.want {
				t.Errorf("SubstrBeforeLast(%q, %q) = %q, want %q", tt.s, tt.sub, got, tt.want)
			}
		})
	}
}

func TestReverseStr(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{"empty", EmptyStr, EmptyStr},
		{"single", "A", "A"},
		{"single CJK", "悟", "悟"},
		{"normal", "ABC", "CBA"},
		{"CJK", "我爱你", "你爱我"},
		{"emoji", "👤🌋", "🌋👤"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ReverseStr(tt.s); got != tt.want {
				t.Errorf("ReverseStr() = %q, want %q", got, tt.want)
			}
		})
	}
}

func TestTruncateStr(t *testing.T) {
	tests := []struct {
		name  string
		s     string
		limit int
		want  string
	}{
		{"uncut", "hello", 6, "hello"},
		{"exact", "hello", 5, "hello"},
		{"cut", "hello", 3, "hel"},
		{"zero", "hello", 0, EmptyStr},
		{"negative", "hello", -2, EmptyStr},
		{"empty", EmptyStr, 1, EmptyStr},
		{"chinese", "實屬要著，積之以久應有相當成就。", 16, "實屬要著，積之以久應有相當成就。"},
		{"chinese1", "實屬要著，積之以久應有相當成就。", 15, "實屬要著，積之以久應有相當成就"},
		{"chinese2", "實屬要著，積之以久應有相當成就。", 14, "實屬要著，積之以久應有相當成"},
		{"chinese3", "實屬要著，積之以久應有相當成就。", 13, "實屬要著，積之以久應有相當"},
		{"emoji", "🍔🥪🌮🌯🥠🥞🍪🥮", 8, "🍔🥪🌮🌯🥠🥞🍪🥮"},
		{"emoji1", "🍔🥪🌮🌯🥠🥞🍪🥮", 7, "🍔🥪🌮🌯🥠🥞🍪"},
		{"emoji2", "🍔🥪🌮🌯🥠🥞🍪🥮", 4, "🍔🥪🌮🌯"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := TruncateStr(tt.s, tt.limit); got != tt.want {
				t.Errorf("TruncateStr(%q, %d) = %v, want %v", tt.s, tt.limit, got, tt.want)
			}
		})
	}
}

func BenchmarkContainsJapanese(b *testing.B) {
	cases := []string{
		`c!ty'super`,
		`𝐿𝑖𝑡𝑡𝑙𝑒𝐷𝑖𝑟𝑡𝑦𝑆𝑒𝑐𝑟𝑒𝑡`,
		`柯桥街道鉴湖景园`,
		`這個年過完會肥死`,
		`ホノカアボーイ`,
		`へんたいがな`,
		`妖艶さと卑猥さの探究者`,
		`抜群の美女`,
		`내 소중이 이쁘지`,
		`เลิฟวิลล่าโฮเทล`,
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for _, c := range cases {
			_ = ContainsJapanese(c)
		}
	}
}

func TestContainsChinese(t *testing.T) {
	tests := []struct {
		s    string
		want bool
	}{
		{`c!ty'super`, false},
		{`𝐿𝑖𝑡𝑡𝑙𝑒𝐷𝑖𝑟𝑡𝑦𝑆𝑒𝑐𝑟𝑒𝑡`, false},
		{`，，，。。。`, false},
		{`柯桥街道鉴湖景园`, true},
		{`這個年過完會肥死`, true},
		{`ホノカアボーイ`, false},
		{`へんたいがな`, false},
		{`妖艶さと卑猥さの探究者`, true},
		{`抜群の美女`, true},
		{`내 소중이 이쁘지`, false},
		{`เลิฟวิลล่าโฮเทล`, false},
	}
	for _, tt := range tests {
		t.Run(tt.s, func(t *testing.T) {
			if got := ContainsChinese(tt.s); got != tt.want {
				t.Errorf("ContainsChinese(%s) = %v, want %v", tt.s, got, tt.want)
			}
		})
	}
}

func TestContainsKorean(t *testing.T) {
	tests := []struct {
		s    string
		want bool
	}{
		{`c!ty'super`, false},
		{`𝐿𝑖𝑡𝑡𝑙𝑒𝐷𝑖𝑟𝑡𝑦𝑆𝑒𝑐𝑟𝑒𝑡`, false},
		{`，，，。。。`, false},
		{`柯桥街道鉴湖景园`, false},
		{`這個年過完會肥死`, false},
		{`ホノカアボーイ`, false},
		{`へんたいがな`, false},
		{`妖艶さと卑猥さの探究者`, false},
		{`抜群の美女`, false},
		{`내 소중이 이쁘지`, true},
		{`เลิฟวิลล่าโฮเทล`, false},
	}
	for _, tt := range tests {
		t.Run(tt.s, func(t *testing.T) {
			if got := ContainsKorean(tt.s); got != tt.want {
				t.Errorf("ContainsKorean(%s) = %v, want %v", tt.s, got, tt.want)
			}
		})
	}
}

func TestContainsJapanese(t *testing.T) {
	tests := []struct {
		s    string
		want bool
	}{
		{`c!ty'super`, false},
		{`𝐿𝑖𝑡𝑡𝑙𝑒𝐷𝑖𝑟𝑡𝑦𝑆𝑒𝑐𝑟𝑒𝑡`, false},
		{`，，，。。。`, false},
		{`柯桥街道鉴湖景园`, true},
		{`這個年過完會肥死`, true},
		{`ホノカアボーイ`, true},
		{`へんたいがな`, true},
		{`妖艶さと卑猥さの探究者`, true},
		{`抜群の美女`, true},
		{`내 소중이 이쁘지`, false},
		{`เลิฟวิลล่าโฮเทล`, false},
	}
	for _, tt := range tests {
		t.Run(tt.s, func(t *testing.T) {
			if got := ContainsJapanese(tt.s); got != tt.want {
				t.Errorf("ContainsJapanese(%s) = %v, want %v", tt.s, got, tt.want)
			}
		})
	}
}

func TestTrimLeftSpace(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{"empty", "", ""},
		{"all spaces", " \t \n ", ""},
		{"left spaces", " \t \n   123", "123"},
		{"right spaces", "456 \t \n   ", "456 \t \n   "},
		{"left and right spaces", " \t \n   7 8 9 \t \n   ", "7 8 9 \t \n   "},
		{"no spaces", "abc", "abc"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := TrimLeftSpace(tt.s); got != tt.want {
				t.Errorf("TrimLeftSpace(%q) = %q, want %q", tt.s, got, tt.want)
			}
		})
	}
}

func TestTrimRightSpace(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{"empty", "", ""},
		{"all spaces", " \t \n ", ""},
		{"left spaces", " \t \n   123", " \t \n   123"},
		{"right spaces", "456 \t \n   ", "456"},
		{"left and right spaces", " \t \n   7 8 9 \t \n   ", " \t \n   7 8 9"},
		{"no spaces", "abc", "abc"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := TrimRightSpace(tt.s); got != tt.want {
				t.Errorf("TrimRightSpace(%q) = %q, want %q", tt.s, got, tt.want)
			}
		})
	}
}

func TestTrimInnerSpace(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{"empty", "", ""},
		{"all spaces", " \t \n ", ""},
		{"left spaces", " \t \n   123", "123"},
		{"right spaces", "456 \t \n   ", "456"},
		{"left and right spaces", " \t \n   7 8 9 \t \n   ", "789"},
		{"no spaces", "abc", "abc"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := TrimInnerSpace(tt.s); got != tt.want {
				t.Errorf("TrimInnerSpace(%q) = %q, want %q", tt.s, got, tt.want)
			}
		})
	}
}

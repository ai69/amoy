package amoy

import (
	"errors"
	"fmt"
	"time"
)

var (
	// ZeroDate is the default Date with zero values.
	ZeroDate = Date{}

	// MinDate is the minimum Date value supported.
	MinDate = Date{1, 1, 1}

	// MaxDate is the maximum Date value supported.
	MaxDate = Date{9999, 12, 31}
)

// Date represents the missing data structure for date.
type Date struct {
	year  int
	month int
	day   int
}

func (d Date) String() string {
	return fmt.Sprintf(`%04d-%02d-%02d`, d.year, d.month, d.day)
}

// Year returns the year in which d occurs.
func (d Date) Year() int {
	return d.year
}

// Month returns the month of the year specified by d.
func (d Date) Month() time.Month {
	return time.Month(d.month)
}

// Day returns the day of the month specified by d.
func (d Date) Day() int {
	return d.day
}

// Sub returns the duration between two dates.
func (d Date) Sub(t Date) time.Duration {
	return d.LocalTime().Sub(t.LocalTime())
}

// SubInDay returns the duration between two dates in day.
func (d Date) SubInDay(t Date) int {
	return int(d.Sub(t).Hours() / 24)
}

// Add returns a Date instance for the duration after the given date.
func (d Date) Add(t time.Duration) Date {
	return NewDateFromTime(d.UTCTime().Add(t))
}

// AddDay returns a Date instance for given days after the current date.
func (d Date) AddDay(n int) Date {
	return NewDateFromTime(d.UTCTime().Add(Days(float64(n))))
}

// AddMonth returns a Date instance for given months after the current date.
func (d Date) AddMonth(n int) Date {
	t := NewUTCDay(d.year, time.Month(d.month+n), d.day)
	return NewDateFromTime(t)
}

// AddYear returns a Date instance for given years after the current date.
func (d Date) AddYear(n int) Date {
	t := NewUTCDay(d.year+n, time.Month(d.month), d.day)
	return NewDateFromTime(t)
}

// After indicates if current date is later than the given date.
func (d Date) After(t Date) bool {
	if d.year != t.year {
		return d.year > t.year
	} else if d.month != t.month {
		return d.month > t.month
	} else {
		return d.day > t.day
	}
}

// Before indicates if current date is earlier than the given date.
func (d Date) Before(t Date) bool {
	if d.year != t.year {
		return d.year < t.year
	} else if d.month != t.month {
		return d.month < t.month
	} else {
		return d.day < t.day
	}
}

// Equal indicates if current date and the given date are equal.
func (d Date) Equal(t Date) bool {
	return d.IsSameDay(t)
}

// IsZero indicates if current date is zero value.
func (d Date) IsZero() bool {
	return d.Equal(ZeroDate)
}

// IsSameYear returns true if current date and the given date are in the same year.
func (d Date) IsSameYear(t Date) bool {
	return d.year == t.year
}

// IsSameMonth returns true if current date and the given date are in the same month of the same year.
func (d Date) IsSameMonth(t Date) bool {
	return d.year == t.year && d.month == t.month
}

// IsSameDay returns true if current date and the given date are in the same day of the same month of the same year.
func (d Date) IsSameDay(t Date) bool {
	return d.year == t.year && d.month == t.month && d.day == t.day
}

// Time returns a time.Time with given location for current instance.
func (d Date) Time(loc *time.Location) time.Time {
	return time.Date(d.year, time.Month(d.month), d.day, 0, 0, 0, 0, loc)
}

// LocalTime returns a local time.Time for current instance.
func (d Date) LocalTime() time.Time {
	return d.Time(time.Local)
}

// UTCTime returns a UTC time.Time for current instance.
func (d Date) UTCTime() time.Time {
	return d.Time(time.UTC)
}

// Clone returns a copy of current instance.
func (d *Date) Clone() Date {
	return Date{
		d.year,
		d.month,
		d.day,
	}
}

// NewDateFromTime converts a time.Time into Date instance.
func NewDateFromTime(t time.Time) Date {
	return Date{
		t.Year(),
		int(t.Month()),
		t.Day(),
	}
}

// NewDate creates a new Date instance.
func NewDate(year, month, day int) Date {
	return NewDateFromTime(NewUTCDay(year, time.Month(month), day))
}

// ParseDate parses a formatted string into Date structure.
func ParseDate(s string) (*Date, error) {
	tt, err := time.ParseInLocation("2006-01-02", s, time.UTC)
	if err != nil {
		return nil, err
	}
	date := NewDateFromTime(tt)
	return &date, nil
}

// MarshalJSON implements the json.Marshaler interface.
// The time is a quoted string in ISO 8601 date format, with sub-second precision added if present.
func (d Date) MarshalJSON() ([]byte, error) {
	if y := d.Year(); y < 0 || y >= 10000 {
		return nil, errors.New("Date.MarshalJSON: year outside of range [0,9999]")
	}
	s := fmt.Sprintf(`"%04d-%02d-%02d"`, d.year, d.month, d.day)
	return []byte(s), nil
}

// UnmarshalJSON implements the json.Unmarshaler interface.
// The date is expected to be a quoted string in ISO 8601 date format.
func (d *Date) UnmarshalJSON(data []byte) error {
	s := string(data)
	// Ignore null, like in the main JSON package.
	if s == "null" {
		return nil
	}
	// Check if length is equal to len(`"YYYY-MM-DD"`) == 12
	if len(s) != 12 {
		return errors.New("Date.UnmarshalJSON: incomplete date")
	}
	// Parse the YYYY-MM-DD part
	dd, err := ParseDate(s[1:11])
	if err != nil {
		return err
	}
	*d = *dd
	return nil
}

// MarshalText implements the encoding.TextMarshaler interface.
// The time is formatted in ISO 8601 date format, with sub-second precision added if present.
func (d Date) MarshalText() (text []byte, err error) {
	if y := d.Year(); y < 0 || y >= 10000 {
		return nil, errors.New("Date.MarshalText: year outside of range [0,9999]")
	}
	s := fmt.Sprintf(`%04d-%02d-%02d`, d.year, d.month, d.day)
	return []byte(s), nil
}

// UnmarshalText implements the encoding.TextUnmarshaler interface.
// The time is expected to be in ISO 8601 date format.
func (d *Date) UnmarshalText(text []byte) error {
	// Parse the YYYY-MM-DD part
	dd, err := ParseDate(string(text))
	if err != nil {
		return err
	}
	*d = *dd
	return nil
}

// Today returns the current date.
func Today() Date {
	return NewDateFromTime(Now())
}

// UTCToday returns the current date in UTC.
func UTCToday() Date {
	return NewDateFromTime(UTCNow())
}

// Yesterday returns the date of yesterday.
func Yesterday() Date {
	return Today().AddDay(-1)
}

// UTCYesterday returns the date of yesterday in UTC.
func UTCYesterday() Date {
	return UTCToday().AddDay(-1)
}

// Tomorrow returns the date of tomorrow.
func Tomorrow() Date {
	return Today().AddDay(1)
}

// UTCTomorrow returns the date of tomorrow in UTC.
func UTCTomorrow() Date {
	return UTCToday().AddDay(1)
}

// FirstDayOfYear returns the first day of current year.
func FirstDayOfYear() Date {
	return Date{Now().Year(), 1, 1}
}

// LastDayOfYear returns the last day of current year.
func LastDayOfYear() Date {
	return Date{Now().Year(), 12, 31}
}

// FirstDayOfMonth returns the first day of current month.
func FirstDayOfMonth() Date {
	n := Now()
	return Date{n.Year(), int(n.Month()), 1}
}

// LastDayOfMonth returns the last day of current month.
func LastDayOfMonth() Date {
	return FirstDayOfMonth().AddMonth(1).AddDay(-1)
}

// ThisYear returns the year of current date.
func ThisYear() int {
	return Now().Year()
}

// LastYear returns the year of last year.
func LastYear() int {
	return ThisYear() - 1
}

// NextYear returns the year of next year.
func NextYear() int {
	return ThisYear() + 1
}

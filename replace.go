package amoy

import (
	"sort"
	"strings"
	"unicode"

	"github.com/1set/gut/ystring"
)

// ReplaceStringOptions indicates the options for the ReplaceString function.
type ReplaceStringOptions struct {
	// Replacements is a map of old-new string pairs.
	Replacements map[string]string
	// CaseInsensitive indicates if the match should be case-insensitive.
	CaseInsensitive bool
	// ImitateResult indicates if the result should be the imitation (about case) of the original string.
	ImitateResult bool
}

// ReplaceString replaces all occurrences of given strings with replacements, with options to make all the replacements
// case-insensitive and imitate the case of old string.
func ReplaceString(s string, opt ReplaceStringOptions) string {
	// if given string is empty, or replacements is empty, just return the original string
	if ystring.IsEmpty(s) || len(opt.Replacements) == 0 {
		return s
	}
	// extract replacement pairs from map
	pairs := make([]*replacePair, 0, len(opt.Replacements))
	for so, sn := range opt.Replacements {
		// if old string is empty, just skip it
		if ystring.IsNotEmpty(so) {
			pairs = append(pairs, &replacePair{so, sn})
		}
	}
	// quit if replacement pairs are actually empty
	if len(pairs) == 0 {
		return s
	}
	singlePair := len(pairs) == 1

	// sort replacement map by length pairs in order to handle the longest match first
	sort.SliceStable(pairs, func(i, j int) bool {
		return len(pairs[i].Old) > len(pairs[j].Old)
	})

	// use built-in replace method if it's case-sensitive and no imitation is required
	if !opt.CaseInsensitive && !opt.ImitateResult {
		if singlePair {
			// if there is only one replacement pair, use simple built-in replace method
			return strings.ReplaceAll(s, pairs[0].Old, pairs[0].New)
		}
		// if there are more than one replacement pairs, build a Replacer object and use it
		pl := make([]string, 0, 2*len(pairs))
		for _, p := range pairs {
			pl = append(pl, p.Old, p.New)
		}
		rp := strings.NewReplacer(pl...)
		return rp.Replace(s)
	}

	// for custom replacements, use various methods for single pair or multiple pairs
	if singlePair {
		return replaceSingleString(s, pairs[0], opt.CaseInsensitive, opt.ImitateResult)
	}
	return replaceMultipleString(s, pairs, opt.CaseInsensitive, opt.ImitateResult)
}

// replacePair is a struct indicates replacement with two fields, Old and New, both of which are strings.
type replacePair struct {
	Old string
	New string
}

// replaceSingleString replaces all occurrences of a given string with another string, with options to make all the replacements.
func replaceSingleString(s string, rp *replacePair, ignoreCase, imitateOld bool) string {
	// ignore empty pair
	if rp == nil || ystring.IsEmpty(rp.Old) {
		return s
	}
	// extract olds and news strings
	olds, news := rp.Old, rp.New
	// get full string to compare
	var tmp string
	if ignoreCase {
		tmp = strings.ToLower(s)
		olds = strings.ToLower(olds)
	} else {
		tmp = s
	}
	// compare and replace until no match found
	var (
		res strings.Builder
		pos = 0 // absolute position in original string
	)
	for {
		// find the match
		matchBegin := strings.Index(tmp, olds)
		if matchBegin < 0 {
			// copy the rest and quit for no more matches
			res.WriteString(s[pos:])
			break
		}
		if matchBegin > 0 {
			// copy the part before the match
			res.WriteString(s[pos : pos+matchBegin])
		}
		// replace the matched string with exactly new or imitate the old string
		matchEnd := matchBegin + len(olds)
		if imitateOld {
			newr := imitateString(s[pos+matchBegin:pos+matchEnd], news)
			res.WriteString(newr)
		} else {
			res.WriteString(news)
		}
		// truncate the processed part
		pos += matchEnd
		tmp = tmp[matchEnd:]
	}
	return res.String()
}

// replaceMultipleString replaces all occurrences of given strings with other strings, with options to make all the replacements.
func replaceMultipleString(s string, rps []*replacePair, ignoreCase, imitateOld bool) string {
	// ignore empty pairs
	if rps == nil || len(rps) == 0 {
		return s
	}
	// get full string to compare
	var ss string
	if ignoreCase {
		ss = strings.ToLower(s)
	} else {
		ss = s
	}
	// compare and replace until no match found
	type charRange struct {
		Low   int
		High  int
		Match int
	}
	matchRanges := make([]*charRange, 0, len(rps))
	leftRanges := make([]*charRange, 0, len(rps))
	leftRanges = append(leftRanges, &charRange{0, len(ss), 0})
	// for each replacement pair, record the matched and left char ranges
	for pairIndex, pair := range rps {
		// ignore pair with empty old string
		var old string
		if ignoreCase {
			old = strings.ToLower(pair.Old)
		} else {
			old = pair.Old
		}
		if ystring.IsEmpty(old) {
			continue
		}
		newLeftRanges := make([]*charRange, 0, len(leftRanges))
		// for each left range
		for _, lr := range leftRanges {
			low, high := lr.Low, lr.High
			for {
				// find the first longest match for current range
				matchBegin := strings.Index(ss[low:high], old)
				if matchBegin < 0 {
					newLeftRanges = append(newLeftRanges, &charRange{low, high, -1})
					break
				}
				if matchBegin > 0 {
					// skip if the match starts at the beginning of the range
					newLeftRanges = append(newLeftRanges, &charRange{low, low + matchBegin, -1})
				}
				// record the range, find in the rest
				matchEnd := matchBegin + len(old)
				matchRanges = append(matchRanges, &charRange{low + matchBegin, low + matchEnd, pairIndex})
				low += matchEnd
			}
		}
		// updates the left range
		leftRanges = newLeftRanges
	}
	// sort by the low index of each range
	sort.SliceStable(matchRanges, func(i, j int) bool {
		return matchRanges[i].Low < matchRanges[j].Low
	})
	// copy or replace
	var (
		res               strings.Builder
		mrCur, lrCur      *charRange
		mrIdx, lrIdx, pos = 0, 0, 0
	)
	for {
		if len(matchRanges) > mrIdx && len(leftRanges) > lrIdx {
			// first part
			matchPos := matchRanges[mrIdx].Low
			leftPos := leftRanges[lrIdx].Low
			if pos == matchPos {
				mrCur = matchRanges[mrIdx]
				pos = mrCur.High
				mrIdx++
			} else if pos == leftPos {
				lrCur = leftRanges[lrIdx]
				pos = lrCur.High
				lrIdx++
			}
		} else if len(leftRanges) > lrIdx {
			// only left is left
			lrCur = leftRanges[lrIdx]
			pos = lrCur.High
			lrIdx++
		} else if len(matchRanges) > mrIdx {
			// only match is left
			mrCur = matchRanges[mrIdx]
			pos = mrCur.High
			mrIdx++
		} else {
			// all the range are handled
			break
		}

		if lrCur != nil {
			// copy the left part directly
			res.WriteString(s[lrCur.Low:lrCur.High])
			lrCur = nil
		} else if mrCur != nil {
			// replace the matched string with exactly new or imitate the old string
			if imitateOld {
				newr := imitateString(s[mrCur.Low:mrCur.High], rps[mrCur.Match].New)
				res.WriteString(newr)
			} else {
				res.WriteString(rps[mrCur.Match].New)
			}
			mrCur = nil
		}
	}
	// result
	return res.String()
}

type stringCaseType uint8

const (
	stringCaseMisc stringCaseType = iota
	stringCaseLower
	stringCaseUpper
	stringCaseTitle
)

// imitateString returns a dest string imitating src string.
// if the source string is lowercase, lowercase the destination string;
// if the source string is uppercase, uppercase the destination string;
// if the source string is titlecase, titlecase the destination string;
// otherwise, do nothing.
func imitateString(old, new string) string {
	switch getStringCaseType(old) {
	case stringCaseLower:
		return strings.ToLower(new)
	case stringCaseUpper:
		return strings.ToUpper(new)
	case stringCaseTitle:
		return strings.Title(new)
	}
	return new
}

func getStringCaseType(s string) stringCaseType {
	const defaultFlag = uint8(0b111) // first 3 bits: 2-title, 1-upper, 0-lower
	caseFlag := defaultFlag
	wordStart := true
	for _, r := range s {
		if unicode.IsLower(r) {
			if wordStart {
				// start with lower case, it can't be title or upper case
				caseFlag &= 0b001
				wordStart = false
			} else {
				// has lower case after start, it can't be upper case
				caseFlag &= 0b101
			}
		} else if unicode.IsUpper(r) {
			if wordStart {
				// start with upper case, it can't be lower
				caseFlag &= 0b110
				wordStart = false
			} else {
				// has upper case after start, it can't be title or lower case
				caseFlag &= 0b010
			}
		} else if unicode.IsSpace(r) || r == '-' || r == '.' {
			// space indicates the start of a new word
			wordStart = true
		}
		if caseFlag == 0 {
			// all the flags are 0, it's misc
			return stringCaseMisc
		}
	}
	// make the judgement
	switch caseFlag {
	case 0b001:
		return stringCaseLower
	case 0b110:
		fallthrough
	case 0b010:
		return stringCaseUpper
	case 0b100:
		return stringCaseTitle
	default:
		return stringCaseMisc
	}
}

package amoy

import (
	"fmt"
	"time"
)

// Common durations. Inherited from libexec/src/time/time.go
//revive:disable:time-naming
const (
	Nanosecond  time.Duration = 1
	Microsecond               = 1000 * Nanosecond
	Millisecond               = 1000 * Microsecond
	Second                    = 1000 * Millisecond
	Minute                    = 60 * Second
	Hour                      = 60 * Minute
	Day                       = 24 * Hour
	Week                      = 7 * Day
	Year                      = 365 * Day
)

var (
	// ZeroDuration is a zero-value for time.Duration.
	ZeroDuration time.Duration

	// ZeroTime is a zero-value for time.Time.
	ZeroTime = time.Time{}
)

// GetUnixTime returns a current time in unix timestamp.
func GetUnixTime() int64 {
	return time.Now().Unix()
}

// ParseUnixTime converts a unix timestamp to local time.
func ParseUnixTime(u int64) time.Time {
	return time.Unix(u, 0)
}

// BeforeNow indicates if the given time is before current time.
func BeforeNow(t time.Time) bool {
	return time.Since(t) > 0
}

// AfterNow indicates if the given time is after current time.
func AfterNow(t time.Time) bool {
	return time.Until(t) > 0
}

// NowStr returns the current local time in RFC3339Nano format.
// e.g. 2021-09-01T12:52:33.250864+08:00
func NowStr() string {
	return time.Now().Format(time.RFC3339Nano)
}

// ShortNowStr returns the current local time in short format.
// e.g. 2021-09-01 12:52:33
func ShortNowStr() string {
	return time.Now().Format("2006-01-02 15:04:05")
}

// DateNowStr returns the date of current local time.
// e.g. 2021-09-01
func DateNowStr() string {
	return time.Now().Format("2006-01-02")
}

// Now returns the current local time. This value is simply time.Now for consistency.
var Now = time.Now

// UTCNow returns the current UTC time.
func UTCNow() time.Time {
	return time.Now().UTC()
}

// UTCNowStr returns the current UTC time in RFC3339Nano format.
// e.g. 2021-09-01T04:52:33.251188Z
func UTCNowStr() string {
	return UTCNow().UTC().Format(time.RFC3339Nano)
}

// ShortUTCNowStr returns the current UTC time in short format.
// e.g. 2021-09-01 04:52:33
func ShortUTCNowStr() string {
	return UTCNow().Format("2006-01-02 15:04:05")
}

// DateUTCNowStr returns the date of current UTC time.
// e.g. 2021-09-01
func DateUTCNowStr() string {
	return UTCNow().Format("2006-01-02")
}

// Milliseconds returns a duration of given milliseconds.
func Milliseconds(n float64) time.Duration {
	return time.Duration(float64(Millisecond) * n)
}

// Seconds returns a duration of given seconds.
func Seconds(n float64) time.Duration {
	return time.Duration(float64(Second) * n)
}

// Minutes returns a duration of given minutes.
func Minutes(n float64) time.Duration {
	return time.Duration(float64(Minute) * n)
}

// Hours returns a duration of given hours.
func Hours(n float64) time.Duration {
	return time.Duration(float64(Hour) * n)
}

// Days returns a duration of given days.
func Days(n float64) time.Duration {
	return time.Duration(float64(Day) * n)
}

// Weeks returns a duration of given weeks.
func Weeks(n float64) time.Duration {
	return time.Duration(float64(Week) * n)
}

// Years returns a duration of given years.
func Years(n float64) time.Duration {
	return time.Duration(float64(Year) * n)
}

// DayBegin returns the first moment of the given time in given location.
func DayBegin(t time.Time, loc *time.Location) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, loc)
}

// DayEnd returns the last moment of the given time in given location.
func DayEnd(t time.Time, loc *time.Location) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, loc).Add(Day).Add(-Nanosecond)
}

// LocalDayBegin returns the first moment of the given time in local timezone.
func LocalDayBegin(t time.Time) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, time.Local)
}

// LocalDayEnd returns the last moment of the given time in local timezone.
func LocalDayEnd(t time.Time) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, time.Local).Add(Day).Add(-Nanosecond)
}

// UTCDayBegin returns the first moment of the given time in UTC timezone.
func UTCDayBegin(t time.Time) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
}

// UTCDayEnd returns the last moment of the given time in UTC timezone.
func UTCDayEnd(t time.Time) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, time.UTC).Add(Day).Add(-Nanosecond)
}

// NewLocalDay returns the first moment of the given date in local timezone.
func NewLocalDay(year int, month time.Month, day int) time.Time {
	return time.Date(year, month, day, 0, 0, 0, 0, time.Local)
}

// NewUTCDay returns the first moment of the given date in UTC timezone.
func NewUTCDay(year int, month time.Month, day int) time.Time {
	return time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
}

// DurationToString returns a string representation (dd:HH:MM:SS.sss) of given duration.
func DurationToString(dr time.Duration) string {
	const (
		dMs = 24 * 60 * 60 * 1000
		hMs = 60 * 60 * 1000
		mMs = 60 * 1000
		sMs = 1000
	)
	t := int(dr.Milliseconds())
	d := t / dMs
	t = t % dMs

	h := t / hMs
	t = t % hMs

	m := t / mMs
	t = t % mMs

	s := t / sMs
	t = t % sMs
	if d > 0 {
		return fmt.Sprintf("%02d:%02d:%02d:%02d.%03d", d, h, m, s, t)
	}
	return fmt.Sprintf("%02d:%02d:%02d.%03d", h, m, s, t)
}

// MustParseDateString returns a time.Time of given date string (format: 2006-01-02) in UTC, or panics if it fails.
func MustParseDateString(s string) time.Time {
	t, err := time.ParseInLocation("2006-01-02", s, time.UTC)
	if err != nil {
		panic(fmt.Errorf("amoy: %w", err))
	}
	return t
}

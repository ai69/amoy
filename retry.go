package amoy

import (
	"time"

	rg "github.com/avast/retry-go"
)

var (
	alwaysTrueFunc  = func(_ error) bool { return true }
	alwaysFalseFunc = func(_ error) bool { return false }
)

// RetryIfFunc represents function signature of retry if function.
type RetryIfFunc func(error) bool

// SimpleRetry retries to execute given function for 3 times and with exponential backoff delay starting at 300ms.
func SimpleRetry(work func() error) error {
	return BackOffRetryIf(work, alwaysTrueFunc, 3, 300*time.Millisecond)
}

// BackOffRetry retries to execute given function with exponential backoff delay.
func BackOffRetry(work func() error, times uint, delay time.Duration) error {
	return BackOffRetryIf(work, alwaysTrueFunc, times, delay)
}

// BackOffRetryIf retries to execute given function with exponential backoff delay if condition meets.
func BackOffRetryIf(work func() error, retryIf RetryIfFunc, times uint, delay time.Duration) error {
	return rg.Do(
		work,
		rg.RetryIf(rg.RetryIfFunc(retryIf)),
		rg.Attempts(times),
		rg.DelayType(rg.BackOffDelay),
		rg.Delay(delay),
		rg.MaxDelay(10*time.Second),
		rg.MaxJitter(time.Duration(float64(delay)*0.2)),
		rg.LastErrorOnly(true),
	)
}

// FixedRetry retries to execute given function with consistent same delay.
func FixedRetry(work func() error, times uint, delay time.Duration) error {
	return FixedRetryIf(work, alwaysTrueFunc, times, delay)
}

// FixedRetryIf retries to execute given function with consistent same delay if condition meets.
func FixedRetryIf(work func() error, retryIf RetryIfFunc, times uint, delay time.Duration) error {
	return rg.Do(
		work,
		rg.RetryIf(rg.RetryIfFunc(retryIf)),
		rg.Attempts(times),
		rg.DelayType(rg.FixedDelay),
		rg.Delay(delay),
		rg.LastErrorOnly(true),
	)
}

package amoy

var releaseQuote = "一以贯之的努力，不得懈怠的人生。"

// Quote returns the quote of current version.
func Quote() string {
	return releaseQuote
}

var (
	// Version is the current version of amoy.
	Version = "v0.2.1"
)

package amoy

import "testing"

func TestToOneLineJSON(t *testing.T) {
	tests := []struct {
		name string
		data interface{}
		want string
	}{
		{"nil", nil, "null"},
		{"empty", map[string]string{}, `{}`},
		{"string", "hello", `"hello"`},
		{"int", 123, `123`},
		{"float", 123.456, `123.456`},
		{"bool", true, `true`},
		{"array", []string{"a", "b", "c"}, `["a","b","c"]`},
		{"object", map[string]string{"a": "bcd"}, `{"a":"bcd"}`},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ToOneLineJSON(tt.data); got != tt.want {
				t.Errorf("ToOneLineJSON(%v) = %q, want %q", tt.data, got, tt.want)
			}
		})
	}
}

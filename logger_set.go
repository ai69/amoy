package amoy

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// NewJSONLogger returns a Logger with given log path and debug mode and all output format is JSON.
func NewJSONLogger(fileName string, debug bool) *Logger {
	return NewLogger(fileName, debug, LogConfig{
		ConsoleFormat: "json",
		FileFormat:    "json",
		MaxFileSizeMB: 10,
		MaxBackups:    10,
		CompressFile:  true,
	})
}

// NewPersistentLogger returns a Logger with given log path and debug mode and output in console with color, save as json, and never cleans up.
func NewPersistentLogger(fileName string, debug bool) *Logger {
	return NewLogger(fileName, debug, LogConfig{
		ConsoleFormat: "console",
		FileFormat:    "json",
		MaxFileSizeMB: 10,
		CompressFile:  true,
	})
}

// NewMonochromeLogger returns a Logger with given log path and debug mode and output in console, save as json, and never cleans up.
func NewMonochromeLogger(fileName string, debug bool) *Logger {
	return NewLogger(fileName, debug, LogConfig{
		ConsoleFormat: "console",
		FileFormat:    "json",
		Monochrome:    true,
		MaxFileSizeMB: 10,
		CompressFile:  true,
	})
}

// NoopZapLogger returns a zap logger enabled at fatal level, it basically logs nothing.
func NoopZapLogger() *zap.Logger {
	logger, err := zap.Config{
		Level:             zap.NewAtomicLevelAt(zap.FatalLevel),
		Development:       false,
		DisableCaller:     true,
		DisableStacktrace: true,
		Encoding:          "console",
		EncoderConfig:     zapcore.EncoderConfig{},
	}.Build()
	if err != nil {
		panic(err)
	}
	return logger
}

// SimpleZapLogger returns a zap logger with color console as output.
func SimpleZapLogger() *zap.Logger {
	logger := NewPersistentLogger("", true)
	return logger.Logger().With(zap.Int("pid", os.Getpid()))
}

// SimpleZapSugaredLogger returns a sugared zap logger with color console as output.
func SimpleZapSugaredLogger() *zap.SugaredLogger {
	logger := NewPersistentLogger("", true)
	return logger.LoggerSugared().With(zap.Int("pid", os.Getpid()))
}

package amoy

import "io"

// DiscardWriter is a Writer on which all Write calls succeed without doing anything.
var DiscardWriter = io.Discard

// DiscardWriteCloser is a WriteCloser on which all Write calls succeed without doing anything.
var DiscardWriteCloser io.WriteCloser = discardCloser{}

type discardCloser struct{}

func (discardCloser) Write(p []byte) (int, error) {
	return len(p), nil
}

func (discardCloser) WriteString(s string) (int, error) {
	return len(s), nil
}

func (discardCloser) Close() error {
	return nil
}

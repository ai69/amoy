package amoy

import (
	"fmt"
	"testing"
)

func TestStyle(t *testing.T) {
	tests := []struct {
		name string
		val  string
		f    func(string) string
	}{
		{"Email", "hello@example.org", StyleEmail},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fmt.Printf("%s = %s\n", FunctionName(tt.f), tt.f(tt.val))
		})
	}
}

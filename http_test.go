package amoy

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
	"testing"
)

var (
	testHttpbinURL     = "https://httpbin.org"
	testHTTPRetryTimes = uint(5)
	skipHTTPTest       = false
)

func init() {
	testHttpbinURL = GetEnvVar("TEST_HTTPBIN_URL", "https://httpbin.org")
	fmt.Println("HTTPBin URL:", testHttpbinURL)
	skipHTTPTest = GetEnvVar("SKIP_HTTP_TEST", "false") == "true"
}

func TestHTTPClient_Get(t *testing.T) {
	if skipHTTPTest {
		t.Skip("Skip HTTP Test")
	}
	defaultUA := defaultHTTPClientOpts.UserAgent
	tests := []struct {
		name      string
		opts      *HTTPClientOptions
		urlSuffix string
		queryArgs map[string]string
		headers   map[string]string
		wantStr   []string
		wantErr   bool
	}{
		{"Nil Opts", nil, "/get", nil, nil, []string{defaultUA}, false},
		{"Empty Opts", &HTTPClientOptions{}, "/get", nil, nil, []string{defaultUA}, false},
		{"Custom UserAgent", &HTTPClientOptions{UserAgent: "amoy-test"}, "/get", nil, nil, []string{"amoy-test"}, false},
		{"Custom Timeout OK", &HTTPClientOptions{Timeout: Seconds(2)}, "/get", nil, nil, []string{defaultUA, "/get"}, false},
		{"Custom Timeout Error", &HTTPClientOptions{Timeout: Seconds(2)}, "/delay/5", nil, nil, []string{defaultUA, "/delay"}, true},
		{"Set Query Args", nil, "/get", map[string]string{"a": "1", "b": "2"}, nil, []string{defaultUA, "/get", "a=1", "b=2"}, false},
		{"Set Headers", nil, "/get", nil, map[string]string{"Type-A": "Apple", "Type-B": "Bravo"}, []string{defaultUA, "/get", "Apple", "Bravo"}, false},
		{"Invalid JSON", nil, "/base64/YWxvaGEK", nil, nil, []string{"aloha"}, false},
		{"Status Code OK", nil, "/status/200", nil, nil, nil, false},
		{"Status Code Error", nil, "/status/300", nil, nil, nil, true},
		{"Status Code Error 2", nil, "/status/404", nil, nil, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewHTTPClient(tt.opts)
			url := testHttpbinURL + tt.urlSuffix
			var (
				got []byte
				err error
			)
			_ = BackOffRetryIf(func() error {
				got, err = c.Get(url, tt.queryArgs, tt.headers)
				return err
			}, func(err error) bool {
				return !strings.Contains(err.Error(), `status code: 502`)
			}, testHTTPRetryTimes, Milliseconds(100))
			// check resp
			if (err != nil) != tt.wantErr {
				t.Errorf("Get(%s) error = %v, wantErr %v", url, err, tt.wantErr)
				return
			}
			if !tt.wantErr && len(tt.wantStr) > 0 {
				gs := string(got)
				for _, s := range tt.wantStr {
					if !strings.Contains(gs, s) {
						t.Errorf("Get(%s) = %s, should contain %q", url, got, s)
					}
				}
			}
		})
	}
}

func TestHTTPClient_GetJSON(t *testing.T) {
	if skipHTTPTest {
		t.Skip("Skip HTTP Test")
	}
	defaultUA := defaultHTTPClientOpts.UserAgent
	type respSchema struct {
		Args    map[string]string `json:"args"`
		Headers struct {
			AcceptEncoding string `json:"Accept-Encoding"`
			ContentType    string `json:"Content-Type"`
			Host           string `json:"Host"`
			UserAgent      string `json:"User-Agent"`
		} `json:"headers"`
		Origin string `json:"origin"`
		URL    string `json:"url"`
	}
	tests := []struct {
		name      string
		opts      *HTTPClientOptions
		urlSuffix string
		queryArgs map[string]string
		headers   map[string]string
		wantStr   []string
		wantErr   bool
	}{
		{"Nil Opts", nil, "/get", nil, nil, []string{defaultUA}, false},
		{"Empty Opts", &HTTPClientOptions{}, "/get", nil, nil, []string{defaultUA}, false},
		{"Custom UserAgent", &HTTPClientOptions{UserAgent: "amoy-test"}, "/get", nil, nil, []string{"amoy-test"}, false},
		{"Custom Timeout OK", &HTTPClientOptions{Timeout: Seconds(2)}, "/get", nil, nil, []string{defaultUA, "/get"}, false},
		{"Custom Timeout Error", &HTTPClientOptions{Timeout: Seconds(2)}, "/delay/5", nil, nil, []string{defaultUA, "/delay"}, true},
		{"Set Query Args", nil, "/get", map[string]string{"a": "1", "b": "2"}, nil, []string{defaultUA, "/get", "a=1", "b=2"}, false},
		{"Set Headers", nil, "/get", nil, map[string]string{"Type-A": "Apple", "Type-B": "Bravo"}, []string{defaultUA, "/get", "Apple", "Bravo"}, false},
		{"Set Query Headers", nil, "/get", map[string]string{"a": "1", "b": "2"}, map[string]string{"Type-A": "Apple", "Type-B": "Bravo"}, []string{defaultUA, "/get", "a=1", "b=2", "Apple", "Bravo"}, false},
		{"Invalid JSON", nil, "/base64/YWxvaGEK", nil, nil, nil, true},
		{"Status Code OK", nil, "/get", nil, nil, nil, false},
		{"Status Code Error", nil, "/status/300", nil, nil, nil, true},
		{"Status Code Error 2", nil, "/status/404", nil, nil, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewHTTPClient(tt.opts)
			url := testHttpbinURL + tt.urlSuffix
			var (
				got  []byte
				err  error
				resp respSchema
			)
			_ = BackOffRetryIf(func() error {
				got, err = c.GetJSON(url, tt.queryArgs, tt.headers, &resp)
				return err
			}, func(err error) bool {
				return !strings.Contains(err.Error(), `status code: 502`)
			}, testHTTPRetryTimes, Milliseconds(100))
			// check resp
			if (err != nil) != tt.wantErr {
				t.Errorf("GetJSON(%s) error = %v, wantErr %v", url, err, tt.wantErr)
				return
			}
			if !tt.wantErr && len(tt.wantStr) > 0 {
				gs := string(got)
				gotUA := tt.wantStr[0]
				if resp.Headers.UserAgent != gotUA {
					t.Errorf("GetJSON(%s)'s UA = %s, want %q", url, got, gotUA)
				}
				for _, s := range tt.wantStr {
					if !strings.Contains(gs, s) {
						t.Errorf("GetJSON(%s) = %s, should contain %q", url, got, s)
					}
				}
			}
		})
	}
}

func TestHTTPClient_Post(t *testing.T) {
	if skipHTTPTest {
		t.Skip("Skip HTTP Test")
	}
	defaultUA := defaultHTTPClientOpts.UserAgent
	tests := []struct {
		name      string
		opts      *HTTPClientOptions
		urlSuffix string
		queryArgs map[string]string
		headers   map[string]string
		body      []byte
		wantStr   []string
		wantErr   bool
	}{
		{"Nil Opts", nil, "/post", nil, nil, nil, []string{defaultUA, "/post"}, false},
		{"Empty Opts", &HTTPClientOptions{}, "/post", nil, nil, nil, []string{defaultUA, "/post"}, false},
		{"Custom UserAgent", &HTTPClientOptions{UserAgent: "amoy-test"}, "/post", nil, nil, nil, []string{"amoy-test", "/post"}, false},
		{"Custom Timeout OK", &HTTPClientOptions{Timeout: Seconds(2)}, "/post", nil, nil, nil, []string{defaultUA, "/post"}, false},
		{"Custom Timeout Error", &HTTPClientOptions{Timeout: Seconds(2)}, "/delay/5", nil, nil, nil, []string{defaultUA, "/delay"}, true},
		{"Set Query Args", nil, "/post", map[string]string{"a": "1", "b": "2"}, nil, nil, []string{defaultUA, "/post", "a=1", "b=2"}, false},
		{"Set Headers", nil, "/post", nil, map[string]string{"Type-A": "Apple", "Type-B": "Bravo"}, nil, []string{defaultUA, "/post", "Apple", "Bravo"}, false},
		{"Set Query Headers", nil, "/post", map[string]string{"a": "1", "b": "2"}, map[string]string{"Type-A": "Apple", "Type-B": "Bravo"}, nil, []string{defaultUA, "/post", "a=1", "b=2", "Apple", "Bravo"}, false},
		{"No Data", nil, "/post", nil, nil, nil, []string{defaultUA, "/post", `""`}, false},
		{"Empty Data", nil, "/post", nil, nil, []byte{}, []string{defaultUA, "/post", `""`}, false},
		{"Data Body", nil, "/post", nil, nil, []byte("hello"), []string{defaultUA, "/post", "hello"}, false},
		{"Status Code OK", nil, "/post", nil, nil, nil, nil, false},
		{"Status Code Error", nil, "/status/300", nil, nil, nil, nil, true},
		{"Status Code Error 2", nil, "/status/404", nil, nil, nil, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewHTTPClient(tt.opts)
			url := testHttpbinURL + tt.urlSuffix
			var (
				got []byte
				err error
			)
			_ = BackOffRetryIf(func() error {
				var rd io.Reader
				if tt.body != nil {
					rd = bytes.NewReader(tt.body)
				}
				got, err = c.Post(url, tt.queryArgs, tt.headers, rd)
				return err
			}, func(err error) bool {
				return !strings.Contains(err.Error(), `status code: 502`)
			}, testHTTPRetryTimes, Milliseconds(100))
			// check resp
			if (err != nil) != tt.wantErr {
				t.Errorf("PostData(%s) error = %v, wantErr %v", url, err, tt.wantErr)
				return
			}
			if !tt.wantErr && len(tt.wantStr) > 0 {
				gs := string(got)
				ds := string(tt.body)
				if !strings.Contains(gs, ds) {
					t.Errorf("PostData(%s)'s Payload = %s, should contain %q", url, got, ds)
				}
				for _, s := range tt.wantStr {
					if !strings.Contains(gs, s) {
						t.Errorf("PostData(%s) = %s, should contain %q", url, got, s)
					}
				}
			}
		})
	}
}

func TestHTTPClient_PostData(t *testing.T) {
	if skipHTTPTest {
		t.Skip("Skip HTTP Test")
	}
	defaultUA := defaultHTTPClientOpts.UserAgent
	tests := []struct {
		name      string
		opts      *HTTPClientOptions
		urlSuffix string
		queryArgs map[string]string
		headers   map[string]string
		body      []byte
		wantStr   []string
		wantErr   bool
	}{
		{"Nil Opts", nil, "/post", nil, nil, nil, []string{defaultUA, "/post"}, false},
		{"Empty Opts", &HTTPClientOptions{}, "/post", nil, nil, nil, []string{defaultUA, "/post"}, false},
		{"Custom UserAgent", &HTTPClientOptions{UserAgent: "amoy-test"}, "/post", nil, nil, nil, []string{"amoy-test", "/post"}, false},
		{"Custom Timeout OK", &HTTPClientOptions{Timeout: Seconds(2)}, "/post", nil, nil, nil, []string{defaultUA, "/post"}, false},
		{"Custom Timeout Error", &HTTPClientOptions{Timeout: Seconds(2)}, "/delay/5", nil, nil, nil, []string{defaultUA, "/delay"}, true},
		{"Set Query Args", nil, "/post", map[string]string{"a": "1", "b": "2"}, nil, nil, []string{defaultUA, "/post", "a=1", "b=2"}, false},
		{"Set Headers", nil, "/post", nil, map[string]string{"Type-A": "Apple", "Type-B": "Bravo"}, nil, []string{defaultUA, "/post", "Apple", "Bravo"}, false},
		{"Set Query Headers", nil, "/post", map[string]string{"a": "1", "b": "2"}, map[string]string{"Type-A": "Apple", "Type-B": "Bravo"}, nil, []string{defaultUA, "/post", "a=1", "b=2", "Apple", "Bravo"}, false},
		{"No Data", nil, "/post", nil, nil, nil, []string{defaultUA, "/post", `""`}, false},
		{"Data Body", nil, "/post", nil, nil, []byte("hello"), []string{defaultUA, "/post", "hello"}, false},
		{"Status Code OK", nil, "/post", nil, nil, nil, nil, false},
		{"Status Code Error", nil, "/status/300", nil, nil, nil, nil, true},
		{"Status Code Error 2", nil, "/status/404", nil, nil, nil, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewHTTPClient(tt.opts)
			url := testHttpbinURL + tt.urlSuffix
			var (
				got []byte
				err error
			)
			_ = BackOffRetryIf(func() error {
				got, err = c.PostData(url, tt.queryArgs, tt.headers, tt.body)
				return err
			}, func(err error) bool {
				return !strings.Contains(err.Error(), `status code: 502`)
			}, testHTTPRetryTimes, Milliseconds(100))
			// check resp
			if (err != nil) != tt.wantErr {
				t.Errorf("PostData(%s) error = %v, wantErr %v", url, err, tt.wantErr)
				return
			}
			if !tt.wantErr && len(tt.wantStr) > 0 {
				gs := string(got)
				ds := string(tt.body)
				if !strings.Contains(gs, ds) {
					t.Errorf("PostData(%s)'s Payload = %s, should contain %q", url, got, ds)
				}
				for _, s := range tt.wantStr {
					if !strings.Contains(gs, s) {
						t.Errorf("PostData(%s) = %s, should contain %q", url, got, s)
					}
				}
			}
		})
	}
}

func TestHTTPClient_PostJSON(t *testing.T) {
	if skipHTTPTest {
		t.Skip("Skip HTTP Test")
	}
	var nilMap map[string]interface{}
	defaultUA := defaultHTTPClientOpts.UserAgent
	type respSchema struct {
		Data    string                 `json:"data"`
		Files   map[string]interface{} `json:"files"`
		Form    map[string]interface{} `json:"form"`
		JSON    map[string]interface{} `json:"json"`
		Args    map[string]string      `json:"args"`
		Headers struct {
			AcceptEncoding string `json:"Accept-Encoding"`
			ContentType    string `json:"Content-Type"`
			Host           string `json:"Host"`
			UserAgent      string `json:"User-Agent"`
		} `json:"headers"`
		Origin string `json:"origin"`
		URL    string `json:"url"`
	}
	tests := []struct {
		name      string
		opts      *HTTPClientOptions
		urlSuffix string
		queryArgs map[string]string
		headers   map[string]string
		body      map[string]interface{}
		wantStr   []string
		wantErr   bool
	}{
		{"Nil Opts", nil, "/post", nil, nil, nil, []string{defaultUA, "/post"}, false},
		{"Empty Opts", &HTTPClientOptions{}, "/post", nil, nil, nil, []string{defaultUA, "/post"}, false},
		{"Custom UserAgent", &HTTPClientOptions{UserAgent: "amoy-test"}, "/post", nil, nil, nil, []string{"amoy-test", "/post"}, false},
		{"Custom Timeout OK", &HTTPClientOptions{Timeout: Seconds(2)}, "/post", nil, nil, nil, []string{defaultUA, "/post"}, false},
		{"Custom Timeout Error", &HTTPClientOptions{Timeout: Seconds(2)}, "/delay/5", nil, nil, nil, []string{defaultUA, "/delay"}, true},
		{"Set Query Args", nil, "/post", map[string]string{"a": "1", "b": "2"}, nil, nil, []string{defaultUA, "/post", "a=1", "b=2"}, false},
		{"Set Headers", nil, "/post", nil, map[string]string{"Type-A": "Apple", "Type-B": "Bravo"}, nil, []string{defaultUA, "/post", "Apple", "Bravo"}, false},
		{"Set Query Headers", nil, "/post", map[string]string{"a": "1", "b": "2"}, map[string]string{"Type-A": "Apple", "Type-B": "Bravo"}, nil, []string{defaultUA, "/post", "a=1", "b=2", "Apple", "Bravo"}, false},
		{"Nil Data", nil, "/post", nil, nil, nil, []string{defaultUA, "/post", `""`, `{}`}, false},
		{"Nil Data 2", nil, "/post", nil, nil, nilMap, []string{defaultUA, "/post", `""`, `{}`}, false},
		{"Empty JSON", nil, "/post", nil, nil, map[string]interface{}{}, []string{defaultUA, "/post", `{}`}, false},
		{"JSON Data", nil, "/post", nil, nil, map[string]interface{}{"task": "hello", "job": "world", "success": true}, []string{defaultUA, "/post", "hello", "world"}, false},
		{"JSON Marshal Error", nil, "/post", nil, nil, map[string]interface{}{"task": "error", "func": strings.Repeat}, nil, true},
		{"Status Code OK", nil, "/post", nil, nil, nil, nil, false},
		{"Status Code Error", nil, "/status/300", nil, nil, nil, nil, true},
		{"Status Code Error 2", nil, "/status/404", nil, nil, nil, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewHTTPClient(tt.opts)
			url := testHttpbinURL + tt.urlSuffix
			var (
				got  []byte
				err  error
				resp respSchema
			)
			_ = BackOffRetryIf(func() error {
				got, err = c.PostJSON(url, tt.queryArgs, tt.headers, tt.body, &resp)
				return err
			}, func(err error) bool {
				return !strings.Contains(err.Error(), `status code: 502`)
			}, testHTTPRetryTimes, Milliseconds(100))
			// check resp
			if (err != nil) != tt.wantErr {
				t.Errorf("PostJSON(%s) error = %v, wantErr %v", url, err, tt.wantErr)
				return
			}
			if !tt.wantErr && tt.body != nil {
				if !reflect.DeepEqual(resp.JSON, tt.body) {
					t.Errorf("PostJSON(%s)'s JSON = %v, want %v", url, resp.JSON, tt.body)
				}
			}
			if !tt.wantErr && len(tt.wantStr) > 0 {
				gs := string(got)
				gotUA := tt.wantStr[0]
				if resp.Headers.UserAgent != gotUA {
					t.Errorf("PostJSON(%s)'s UA = %s, want %q", url, got, gotUA)
				}
				for _, s := range tt.wantStr {
					if !strings.Contains(gs, s) {
						t.Errorf("PostJSON(%s) = %s, should contain %q", url, got, s)
					}
				}
			}
		})
	}
}

func TestHTTPClient_Custom_Head(t *testing.T) {
	if skipHTTPTest {
		t.Skip("Skip HTTP Test")
	}
	tests := []struct {
		name      string
		opts      *HTTPClientOptions
		urlSuffix string
		queryArgs map[string]string
		headers   map[string]string
		body      []byte
		wantErr   bool
	}{
		{"Nil Opts", nil, "/get", nil, nil, nil, false},
		{"Empty Opts", &HTTPClientOptions{}, "/get", nil, nil, nil, false},
		{"Custom Timeout OK", &HTTPClientOptions{Timeout: Seconds(2)}, "/get", nil, nil, nil, false},
		{"Custom Timeout Error", &HTTPClientOptions{Timeout: Seconds(2)}, "/delay/5", nil, nil, nil, true},
		{"Set Query Args", nil, "/get", map[string]string{"a": "1", "b": "2"}, nil, nil, false},
		{"Set Headers", nil, "/get", nil, map[string]string{"Type-A": "Apple", "Type-B": "Bravo"}, nil, false},
		{"Body Data", nil, "/base64/YWxvaGEK", nil, nil, []byte("aloha"), false},
		{"Status Code OK", nil, "/status/200", nil, nil, nil, false},
		{"Status Code Error", nil, "/status/300", nil, nil, nil, true},
		{"Status Code Error 2", nil, "/status/404", nil, nil, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewHTTPClient(tt.opts)
			url := testHttpbinURL + tt.urlSuffix
			var (
				got []byte
				err error
			)
			_ = BackOffRetryIf(func() error {
				var rd io.Reader
				if tt.body != nil {
					rd = bytes.NewReader(tt.body)
				}
				got, err = c.Custom(http.MethodHead, url, tt.queryArgs, tt.headers, rd)
				return err
			}, func(err error) bool {
				return !strings.Contains(err.Error(), `status code: 502`)
			}, testHTTPRetryTimes, Milliseconds(100))
			// check resp
			if (err != nil) != tt.wantErr {
				t.Errorf("Custom(Head, %s) error = %v, wantErr %v", url, err, tt.wantErr)
				return
			}
			if !tt.wantErr && len(got) > 0 {
				t.Errorf("Custom(Head, %s) = %s, want empty", url, got)
			}
		})
	}
}

func TestHTTPClient_Custom_Patch(t *testing.T) {
	if skipHTTPTest {
		t.Skip("Skip HTTP Test")
	}
	defaultUA := defaultHTTPClientOpts.UserAgent
	tests := []struct {
		name      string
		opts      *HTTPClientOptions
		urlSuffix string
		queryArgs map[string]string
		headers   map[string]string
		body      []byte
		wantStr   []string
		wantErr   bool
	}{
		{"Nil Opts", nil, "/patch", nil, nil, nil, []string{defaultUA}, false},
		{"Empty Opts", &HTTPClientOptions{}, "/patch", nil, nil, nil, []string{defaultUA}, false},
		{"Custom UserAgent", &HTTPClientOptions{UserAgent: "amoy-test"}, "/patch", nil, nil, nil, []string{"amoy-test"}, false},
		{"Custom Timeout OK", &HTTPClientOptions{Timeout: Seconds(2)}, "/patch", nil, nil, nil, []string{defaultUA, "/patch"}, false},
		{"Custom Timeout Error", &HTTPClientOptions{Timeout: Seconds(2)}, "/delay/5", nil, nil, nil, []string{defaultUA, "/delay"}, true},
		{"Set Query Args", nil, "/patch", map[string]string{"a": "1", "b": "2"}, nil, nil, []string{defaultUA, "/patch", "a=1", "b=2"}, false},
		{"Set Headers", nil, "/patch", nil, map[string]string{"Type-A": "Apple", "Type-B": "Bravo"}, nil, []string{defaultUA, "/patch", "Apple", "Bravo"}, false},
		{"Body Data", nil, "/patch", nil, nil, []byte("aloha"), []string{"aloha"}, false},
		{"Status Code OK", nil, "/status/200", nil, nil, nil, nil, false},
		{"Status Code Error", nil, "/status/300", nil, nil, nil, nil, true},
		{"Status Code Error 2", nil, "/status/404", nil, nil, nil, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewHTTPClient(tt.opts)
			url := testHttpbinURL + tt.urlSuffix
			var (
				got []byte
				err error
			)
			_ = BackOffRetryIf(func() error {
				var rd io.Reader
				if tt.body != nil {
					rd = bytes.NewReader(tt.body)
				}
				got, err = c.Custom(http.MethodPatch, url, tt.queryArgs, tt.headers, rd)
				return err
			}, func(err error) bool {
				return !strings.Contains(err.Error(), `status code: 502`)
			}, testHTTPRetryTimes, Milliseconds(100))
			// check resp
			if (err != nil) != tt.wantErr {
				t.Errorf("Custom(Patch, %s) error = %v, wantErr %v", url, err, tt.wantErr)
				return
			}
			if !tt.wantErr && len(tt.wantStr) > 0 {
				gs := string(got)
				for _, s := range tt.wantStr {
					if !strings.Contains(gs, s) {
						t.Errorf("Custom(Patch, %s) = %s, should contain %q", url, got, s)
					}
				}
			}
		})
	}
}

func TestHTTPClient_EmbeddingClient(t *testing.T) {
	if skipHTTPTest {
		t.Skip("Skip HTTP Test")
	}
	defaultUA := "Go-http-client/"
	tests := []struct {
		name      string
		opts      *HTTPClientOptions
		urlSuffix string
		queryArgs map[string]string
		headers   map[string]string
		wantStr   []string
		wantErr   bool
	}{
		{"Custom Timeout OK", &HTTPClientOptions{Timeout: Seconds(2)}, "/get", nil, nil, []string{defaultUA, "/get"}, false},
		{"Custom Timeout Error", &HTTPClientOptions{Timeout: Seconds(2)}, "/delay/5", nil, nil, nil, true},
		{"Missed Auth", &HTTPClientOptions{Username: "joe", Password: "secret"}, "/basic-auth/joe/secret", nil, nil, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewHTTPClient(tt.opts)
			url := testHttpbinURL + tt.urlSuffix
			var (
				got []byte
				err error
			)
			err = BackOffRetryIf(func() error {
				resp, e := c.Client.Get(url)
				if e != nil {
					return e
				}
				if resp.StatusCode != http.StatusOK {
					return fmt.Errorf("status code: %d", resp.StatusCode)
				}
				defer resp.Body.Close()
				got, err = ioutil.ReadAll(resp.Body)
				return err
			}, func(err error) bool {
				return !strings.Contains(err.Error(), `status code: 502`)
			}, testHTTPRetryTimes, Milliseconds(100))
			// check resp
			if (err != nil) != tt.wantErr {
				t.Errorf("EmbeddingClient Get(%s) error = %v, wantErr %v", url, err, tt.wantErr)
				return
			}
			if !tt.wantErr && len(tt.wantStr) > 0 {
				gs := string(got)
				for _, s := range tt.wantStr {
					if !strings.Contains(gs, s) {
						t.Errorf("EmbeddingClient Get(%s) = %s, should contain %q", url, got, s)
					}
				}
			}
		})
	}
}

func TestHTTPClient_Auth_Basic(t *testing.T) {
	if skipHTTPTest {
		t.Skip("Skip HTTP Test")
	}
	tests := []struct {
		name      string
		opts      *HTTPClientOptions
		urlSuffix string
		queryArgs map[string]string
		headers   map[string]string
		wantStr   []string
		wantErr   bool
	}{
		{"Nil Opts", nil, "/basic-auth/joe/secret", nil, nil, nil, true},
		{"Empty Opts", &HTTPClientOptions{}, "/basic-auth/joe/secret", nil, nil, nil, true},
		{"Incorrect User", &HTTPClientOptions{Username: "john", Password: "secret"}, "/basic-auth/joe/secret", nil, nil, nil, true},
		{"Incorrect Password", &HTTPClientOptions{Username: "joe", Password: "nopass"}, "/basic-auth/joe/secret", nil, nil, nil, true},
		{"Token Override", &HTTPClientOptions{Username: "joe", Password: "secret", BearerToken: "abc12345"}, "/basic-auth/joe/secret", nil, nil, nil, true},
		{"Correct", &HTTPClientOptions{Username: "joe", Password: "secret"}, "/basic-auth/joe/secret", nil, nil, []string{"true", "joe"}, false},
		{"Correct 2", &HTTPClientOptions{Username: "john", Password: "imagine"}, "/basic-auth/john/imagine", nil, nil, []string{"true", "john"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewHTTPClient(tt.opts)
			url := testHttpbinURL + tt.urlSuffix
			var (
				got []byte
				err error
			)
			_ = BackOffRetryIf(func() error {
				got, err = c.Get(url, tt.queryArgs, tt.headers)
				return err
			}, func(err error) bool {
				return !strings.Contains(err.Error(), `status code: 502`)
			}, testHTTPRetryTimes, Milliseconds(100))
			// check resp
			if (err != nil) != tt.wantErr {
				t.Errorf("Auth Basic(%s) error = %v, wantErr %v", url, err, tt.wantErr)
				return
			}
			if !tt.wantErr && len(tt.wantStr) > 0 {
				gs := string(got)
				for _, s := range tt.wantStr {
					if !strings.Contains(gs, s) {
						t.Errorf("Auth Basic(%s) = %s, should contain %q", url, got, s)
					}
				}
			}
		})
	}
}

func TestHTTPClient_Auth_Bearer(t *testing.T) {
	if skipHTTPTest {
		t.Skip("Skip HTTP Test")
	}
	tests := []struct {
		name      string
		opts      *HTTPClientOptions
		urlSuffix string
		queryArgs map[string]string
		headers   map[string]string
		wantStr   []string
		wantErr   bool
	}{
		{"Nil Opts", nil, "/bearer", nil, nil, nil, true},
		{"Empty Opts", &HTTPClientOptions{}, "/bearer", nil, nil, nil, true},
		{"No Token", &HTTPClientOptions{BearerToken: ""}, "/bearer", nil, nil, nil, true},
		{"Correct Token", &HTTPClientOptions{BearerToken: "abc12345"}, "/bearer", nil, nil, []string{"abc12345"}, false},
		{"Duplicate User", &HTTPClientOptions{Username: "joe", Password: "secret", BearerToken: "abc12345"}, "/bearer", nil, nil, []string{"abc12345"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewHTTPClient(tt.opts)
			url := testHttpbinURL + tt.urlSuffix
			var (
				got []byte
				err error
			)
			_ = BackOffRetryIf(func() error {
				got, err = c.Get(url, tt.queryArgs, tt.headers)
				return err
			}, func(err error) bool {
				return !strings.Contains(err.Error(), `status code: 502`)
			}, testHTTPRetryTimes, Milliseconds(100))
			// check resp
			if (err != nil) != tt.wantErr {
				t.Errorf("Auth Bearer(%s) error = %v, wantErr %v", url, err, tt.wantErr)
				return
			}
			if !tt.wantErr && len(tt.wantStr) > 0 {
				gs := string(got)
				for _, s := range tt.wantStr {
					if !strings.Contains(gs, s) {
						t.Errorf("Auth Bearer(%s) = %s, should contain %q", url, got, s)
					}
				}
			}
		})
	}
}

func TestHTTPClient_Redirect(t *testing.T) {
	if skipHTTPTest {
		t.Skip("Skip HTTP Test")
	}
	defaultUA := defaultHTTPClientOpts.UserAgent
	tests := []struct {
		name      string
		opts      *HTTPClientOptions
		urlSuffix string
		queryArgs map[string]string
		headers   map[string]string
		wantStr   []string
		wantErr   bool
	}{
		{"Nil Opts", nil, "/redirect/1", nil, nil, []string{defaultUA, "/get"}, false},
		{"Empty Opts", &HTTPClientOptions{}, "/redirect/1", nil, nil, []string{defaultUA, "/get"}, false},
		{"Enable Redirect", &HTTPClientOptions{DisableRedirect: false}, "/redirect/1", nil, nil, []string{defaultUA, "/get"}, false},
		{"Disable Redirect", &HTTPClientOptions{DisableRedirect: true}, "/redirect/1", nil, nil, nil, true},
		{"Redirect Too Many", &HTTPClientOptions{DisableRedirect: false}, "/redirect/10", nil, nil, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewHTTPClient(tt.opts)
			url := testHttpbinURL + tt.urlSuffix
			var (
				got []byte
				err error
			)
			_ = BackOffRetryIf(func() error {
				got, err = c.Get(url, tt.queryArgs, tt.headers)
				return err
			}, func(err error) bool {
				return !strings.Contains(err.Error(), `status code: 502`)
			}, testHTTPRetryTimes, Milliseconds(100))
			// check resp
			if (err != nil) != tt.wantErr {
				t.Errorf("Redirect(%s) error = %v, wantErr %v", url, err, tt.wantErr)
				return
			}
			if !tt.wantErr && len(tt.wantStr) > 0 {
				gs := string(got)
				for _, s := range tt.wantStr {
					if !strings.Contains(gs, s) {
						t.Errorf("Redirect(%s) = %s, should contain %q", url, got, s)
					}
				}
			}
		})
	}
}

func TestHTTPClient_Error(t *testing.T) {
	if skipHTTPTest {
		t.Skip("Skip HTTP Test")
	}
	tests := []struct {
		name      string
		opts      *HTTPClientOptions
		url       string
		queryArgs map[string]string
		headers   map[string]string
		wantStr   []string
		wantErr   bool
	}{
		{"No Host", nil, "https://example.invalid", nil, nil, nil, true},
		{"Expired Cert", nil, "https://expired.badssl.com", nil, nil, nil, true},
		{"Expired Cert Ignored", &HTTPClientOptions{Insecure: true}, "https://expired.badssl.com", nil, nil, []string{"expired"}, false},
		{"Revoked Cert", nil, "https://revoked.badssl.com", nil, nil, nil, true},
		{"Revoked Cert Ignored", &HTTPClientOptions{Insecure: true}, "https://revoked.badssl.com", nil, nil, []string{"revoked"}, false},
		{"Wrong Host Cert", nil, "https://wrong.host.badssl.com", nil, nil, nil, true},
		{"Wrong Host Cert Ignored", &HTTPClientOptions{Insecure: true}, "https://wrong.host.badssl.com", nil, nil, []string{"wrong"}, false},
		{"Self Signed Cert", nil, "https://self-signed.badssl.com", nil, nil, nil, true},
		{"Self Signed Cert Ignored", &HTTPClientOptions{Insecure: true}, "https://self-signed.badssl.com", nil, nil, []string{"self-signed"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewHTTPClient(tt.opts)
			var (
				got []byte
				err error
			)
			_ = BackOffRetryIf(func() error {
				got, err = c.Get(tt.url, tt.queryArgs, tt.headers)
				return err
			}, func(err error) bool {
				return !strings.Contains(err.Error(), `status code: 502`)
			}, testHTTPRetryTimes, Milliseconds(100))
			// check resp
			if (err != nil) != tt.wantErr {
				t.Errorf("Error(%s) error = %v, data = %d, wantErr %v", tt.url, err, len(got), tt.wantErr)
				return
			}
			if !tt.wantErr && len(tt.wantStr) > 0 {
				gs := string(got)
				for _, s := range tt.wantStr {
					if !strings.Contains(gs, s) {
						t.Errorf("Error(%s) = %s, should contain %q", tt.url, got, s)
					}
				}
			}
		})
	}
}
